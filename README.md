# Ark2

## What is it ?

Provide performance portable Kokkos implementation for
multidimensional compressible hydrodynamics (1D, 2D, 3D).

## Dependencies

* [Kokkos](https://github.com/kokkos/kokkos) with version >= 2.9.00
* [CMake](https://cmake.org) with version >= 3.9

Optional
* [Cuda](https://developer.nvidia.com/cuda-downloads) with version >= 8.0
* MPI, for example [OpenMPI](https://www.open-mpi.org)
* [HDF5](https://portal.hdfgroup.org)

## How to get ark v2 sources

Kokkos sources are included as a git submodule.

To download project "ark2" clone it with option "--recurse-submodules"
```
git clone --recurse-submodules https://gitlab.erc-atmo.eu/thomas.padioleau/ark_refactored
```

If you performed a regular "git clone", just type
```
git submodule init
git submodule update
```
to retrieve kokkos sources.

## How to build

A few example of builds. Default values make it compile without MPI,
with Kokkos-serial and in Release build type.

### Build Serial-Serial (Default)

Create a build directory, configure and make
```shell
mkdir build && cd build
cmake ..
cmake --build . -- -j 4
```

Add variable CXX on the cmake command line to change the compiler
(clang++, icpc, pgcc, ....)

### Build Serial-OpenMP

Create a build directory, configure and make
```shell
mkdir build && cd build
cmake -DKokkos_ENABLE_OPENMP=ON ..
cmake --build . -- -j 4
```

Add variable CXX on the cmake command line to change the compiler
(clang++, icpc, pgcc, ....)

### Build Serial-Cuda

Create a build directory, configure and make (for a Maxwell GPU, see [Table 4.2](https://github.com/kokkos/kokkos/wiki/Compiling#table-43-architecture-variables) for available architectures)
```shell
mkdir build && cd build
export CXX=$PWD/../lib/kokkos/bin/nvcc_wrapper
cmake -DKokkos_ENABLE_CUDA=ON -DKokkos_ARCH_MAXWELL50=ON ..
cmake --build . -- -j 4
```

### Build MPI-OpenMP

Create a build directory, configure and make
```shell
mkdir build && cd build
cmake -DSESSION=MPI_SESSION -DKokkos_ENABLE_OPENMP=ON ..
cmake --build . -- -j 4
```

### Build MPI-Cuda

Please make sure to use a CUDA-aware MPI implementation (OpenMPI or
MVAPICH2) built with the proper flags for activating CUDA support.

Create a build directory, configure and make
```shell
mkdir build && cd build
export CXX=$PWD/../lib/kokkos/bin/nvcc_wrapper
cmake -DSESSION=MPI_SESSION -DKokkos_ENABLE_CUDA=ON -DKokkos_ARCH_MAXWELL50=ON ..
cmake --build . -- -j 4
```

## How to run

In the build directory just type
``` shell
./main configuration_file.ini --kokkos-xxx=value
```
If you add kokkos options (see [Table 5.1](https://github.com/kokkos/kokkos/wiki/Initialization#table-51-command-line-options-for-kokkosinitialize-) for a list of options) from the command line, use the prefixed version (--kokkos-xxx).

### Run Serial-OpenMP

``` shell
./main path/to/configuration_file.ini --kokkos-threads=2
```

### Run MPI-Cuda

To run on 4 GPUs, 1 GPU per MPI task
```shell
mpirun -np 4 ./main path/to/configuration_file.ini --kokkos-ndevices=4
```

## Developping with vim and youcomplete plugin

Assuming you are using vim (or neovim) text editor and have installed
the youcomplete plugin, you can have semantic autocompletion in a C++
project.

Make sure to have CMake variable CMAKE_EXPORT_COMPILE_COMMANDS set to
ON, and symlink the generated file to the top level source directory.

## Coding rules

For cmake follow [Modern
CMake](https://cliutils.gitlab.io/modern-cmake/) which also cites
other sources like coding rules from [Effective Modern
CMake](https://gist.github.com/mbinna/c61dbb39bca0e4fb7d1f73b0d66a4fd1).

# Tips

## Verbose mode of nvcc_wrapper

You can set the variable
``` shell
NVCC_WRAPPER_SHOW_COMMANDS_BEING_RUN=1
```
when you compile. This prints to the standard output the nvcc command line.

## Usage of Kokkos-tools (see Kokkos [documentation](https://github.com/kokkos/kokkos-tools/wiki))

To use one of the tools you have to compile it, which will generate a
dynamic library. Before executing the Kokkos application you then have
to set the environment variable KOKKOS_PROFILE_LIBRARY to point to the
dynamic library e.g. in Bash, in the build directory type:

``` shell
export KOKKOS_PROFILE_LIBRARY=${PWD}/../lib/kokkos-tools/src/tools/memory-events/kp_memory_events.so
```

Many of the tools will produce an output file which uses the hostname
as well as the process id as part of the filename.
