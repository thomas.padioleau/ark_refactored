#pragma once

#include <Kokkos_Core.hpp>

#include "linalg/Vector.hpp"

#include "EulerSystem.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

// Should be used only inside a Kokkos::parallel_for
// This is temporary fix until Kokkos a Kokkos::TeamVectorRange with #pragma ivdep inside
template <class Team, class iType>
KOKKOS_INLINE_FUNCTION static auto TeamVectorRange(
        const Team& team,
        const iType start,
        const iType end) noexcept -> decltype(Kokkos::ThreadVectorRange(team, start, end))
{
    const iType chunk = (end - start) / team.team_size();
    const iType start_vector = start + team.team_rank() * chunk;
    const iType end_vector
            = (team.team_rank() == team.team_size() - 1) ? end : start_vector + chunk;

    return Kokkos::ThreadVectorRange(team, start_vector, end_vector);
}

template <class T, T... Is, class F>
KOKKOS_FORCEINLINE_FUNCTION constexpr void for_each_static(
        std::integer_sequence<T, Is...>,
        const F& f)
{
    (f(std::integral_constant<T, Is>()), ...);
}

template <class T, T N, class F>
KOKKOS_FORCEINLINE_FUNCTION constexpr void for_each_static(std::integral_constant<T, N>, const F& f)
{
    for_each_static(std::make_integer_sequence<T, N>(), f);
}

template <dim_t dim>
struct BaseKernel
{
    using Euler = EulerSystem<dim>;
    using EquationOfState = typename Euler::EquationOfState;
    using VC = typename Euler::VarCons;
    using VP = typename Euler::VarPrim;

    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;

    static Int computeLeagueSize(const IntVector<dim>& nbCells, int ghostdepth)
    {
        int league_size;
        if (dim == one_d)
        {
            league_size = 1;
        }
        else if (dim == two_d)
        {
            league_size = (nbCells[IY] + 2 * ghostdepth);
        }
        else if (dim == three_d)
        {
            league_size = (nbCells[IY] + 2 * ghostdepth) * (nbCells[IZ] + 2 * ghostdepth);
        }
        return league_size;
    }

    template <class kokkos_team>
    KOKKOS_FORCEINLINE_FUNCTION static IntVector<dim> teamToCoord(
            const kokkos_team& team,
            const UniformGrid<dim>& grid,
            int ghostdepth) noexcept
    {
        IntVector<dim> coord;
        if (dim == two_d)
        {
            coord[IY] = team.league_rank();
            coord[IY] += grid.m_ghostWidths[IY] - ghostdepth;
        }
        if (dim == three_d)
        {
            coord[IZ] = team.league_rank() / (grid.m_nbCells[IY] + 2 * ghostdepth);
            coord[IY] = team.league_rank() - (grid.m_nbCells[IY] + 2 * ghostdepth) * coord[IZ];
            coord[IZ] += grid.m_ghostWidths[IZ] - ghostdepth;
            coord[IY] += grid.m_ghostWidths[IY] - ghostdepth;
        }
        coord[IX] = 0;
        return coord;
    }

    template <class kokkos_team>
    KOKKOS_FORCEINLINE_FUNCTION static Int teamToLinearIndex(
            const kokkos_team& team,
            const UniformGrid<dim>& grid,
            int ghostdepth) noexcept
    {
        return grid.coordToIndex(teamToCoord(team, grid, ghostdepth));
    }

    KOKKOS_INLINE_FUNCTION
    void copy(const RealArray<dim>& array, Int j, Int j0) const
    {
        for (int ivar = 0; ivar < Euler::nbvar; ++ivar)
        {
            array(j, ivar) = array(j0, ivar);
        }
    }

    KOKKOS_INLINE_FUNCTION
    void copy(const RealArray<dim>& dst, Int j, const ConstRealArray<dim>& src, Int j0) const
    {
        for (int ivar = 0; ivar < Euler::nbvar; ++ivar)
        {
            dst(j, ivar) = src(j0, ivar);
        }
    }

    template <class T>
    KOKKOS_FORCEINLINE_FUNCTION static ConsState getCons(
            const Array<T, dim>& array,
            Int j) noexcept;

    template <class T>
    KOKKOS_FORCEINLINE_FUNCTION static PrimState getPrim(
            const Array<T, dim>& array,
            Int j) noexcept;

    template <class T>
    KOKKOS_FORCEINLINE_FUNCTION static Vector<dim, Real> getVelocity(
            const Array<T, dim>& array,
            Int j) noexcept;

    KOKKOS_FORCEINLINE_FUNCTION
    static void set(const RealArray<dim>& array, Int j, const PrimState& q_j) noexcept;

    KOKKOS_FORCEINLINE_FUNCTION
    static void set(const RealArray<dim>& array, Int j, const ConsState& q_j) noexcept;
};

template <>
template <class T>
KOKKOS_FORCEINLINE_FUNCTION BaseKernel<one_d>::ConsState BaseKernel<one_d>::getCons(
        const Array<T, one_d>& array,
        Int j) noexcept
{
    ConsState u_j;
    u_j.d = array(j, VC::ID);
    u_j.e = array(j, VC::IE);
    u_j.m(Tags::DirX) = array(j, VC::IMx);
    return u_j;
}

template <>
template <class T>
KOKKOS_FORCEINLINE_FUNCTION BaseKernel<two_d>::ConsState BaseKernel<two_d>::getCons(
        const Array<T, two_d>& array,
        Int j) noexcept
{
    ConsState u_j;
    u_j.d = array(j, VC::ID);
    u_j.e = array(j, VC::IE);
    u_j.m(Tags::DirX) = array(j, VC::IMx);
    u_j.m(Tags::DirY) = array(j, VC::IMy);
    return u_j;
}

template <>
template <class T>
KOKKOS_FORCEINLINE_FUNCTION BaseKernel<three_d>::ConsState BaseKernel<three_d>::getCons(
        const Array<T, three_d>& array,
        Int j) noexcept
{
    ConsState u_j;
    u_j.d = array(j, VC::ID);
    u_j.e = array(j, VC::IE);
    u_j.m(Tags::DirX) = array(j, VC::IMx);
    u_j.m(Tags::DirY) = array(j, VC::IMy);
    u_j.m(Tags::DirZ) = array(j, VC::IMz);
    return u_j;
}

template <>
template <class T>
KOKKOS_FORCEINLINE_FUNCTION BaseKernel<one_d>::PrimState BaseKernel<one_d>::getPrim(
        const Array<T, one_d>& array,
        Int j) noexcept
{
    PrimState q_j;
    q_j.d = array(j, VP::ID);
    q_j.p = array(j, VP::IP);
    q_j.v(Tags::DirX) = array(j, VP::IUx);
    return q_j;
}

template <>
template <class T>
KOKKOS_FORCEINLINE_FUNCTION BaseKernel<two_d>::PrimState BaseKernel<two_d>::getPrim(
        const Array<T, two_d>& array,
        Int j) noexcept
{
    PrimState q_j;
    q_j.d = array(j, VP::ID);
    q_j.p = array(j, VP::IP);
    q_j.v(Tags::DirX) = array(j, VP::IUx);
    q_j.v(Tags::DirY) = array(j, VP::IUy);
    return q_j;
}

template <>
template <class T>
KOKKOS_FORCEINLINE_FUNCTION BaseKernel<three_d>::PrimState BaseKernel<three_d>::getPrim(
        const Array<T, three_d>& array,
        Int j) noexcept
{
    PrimState q_j;
    q_j.d = array(j, VP::ID);
    q_j.p = array(j, VP::IP);
    q_j.v(Tags::DirX) = array(j, VP::IUx);
    q_j.v(Tags::DirY) = array(j, VP::IUy);
    q_j.v(Tags::DirZ) = array(j, VP::IUz);
    return q_j;
}

template <>
template <class T>
KOKKOS_FORCEINLINE_FUNCTION Vector<one_d, Real> BaseKernel<one_d>::getVelocity(
        const Array<T, one_d>& array,
        Int j) noexcept
{
    Vector<one_d, Real> v_j;
    v_j(Tags::DirX) = array(j, VP::IUx);
    return v_j;
}

template <>
template <class T>
KOKKOS_FORCEINLINE_FUNCTION Vector<two_d, Real> BaseKernel<two_d>::getVelocity(
        const Array<T, two_d>& array,
        Int j) noexcept
{
    Vector<two_d, Real> v_j;
    v_j(Tags::DirX) = array(j, VP::IUx);
    v_j(Tags::DirY) = array(j, VP::IUy);
    return v_j;
}

template <>
template <class T>
KOKKOS_FORCEINLINE_FUNCTION Vector<three_d, Real> BaseKernel<three_d>::getVelocity(
        const Array<T, three_d>& array,
        Int j) noexcept
{
    Vector<three_d, Real> v_j;
    v_j(Tags::DirX) = array(j, VP::IUx);
    v_j(Tags::DirY) = array(j, VP::IUy);
    v_j(Tags::DirZ) = array(j, VP::IUz);
    return v_j;
}

template <>
KOKKOS_FORCEINLINE_FUNCTION void BaseKernel<one_d>::set(
        const RealArray<one_d>& array,
        Int j,
        const PrimState& q_j) noexcept
{
    array(j, VP::ID) = q_j.d;
    array(j, VP::IP) = q_j.p;
    array(j, VP::IUx) = q_j.v(Tags::DirX);
}

template <>
KOKKOS_FORCEINLINE_FUNCTION void BaseKernel<two_d>::set(
        const RealArray<two_d>& array,
        Int j,
        const PrimState& q_j) noexcept
{
    array(j, VP::ID) = q_j.d;
    array(j, VP::IP) = q_j.p;
    array(j, VP::IUx) = q_j.v(Tags::DirX);
    array(j, VP::IUy) = q_j.v(Tags::DirY);
}

template <>
KOKKOS_FORCEINLINE_FUNCTION void BaseKernel<three_d>::set(
        const RealArray<three_d>& array,
        Int j,
        const PrimState& q_j) noexcept
{
    array(j, VP::ID) = q_j.d;
    array(j, VP::IP) = q_j.p;
    array(j, VP::IUx) = q_j.v(Tags::DirX);
    array(j, VP::IUy) = q_j.v(Tags::DirY);
    array(j, VP::IUz) = q_j.v(Tags::DirZ);
}

template <>
KOKKOS_FORCEINLINE_FUNCTION void BaseKernel<one_d>::set(
        const RealArray<one_d>& array,
        Int j,
        const ConsState& u_j) noexcept
{
    array(j, VC::ID) = u_j.d;
    array(j, VC::IE) = u_j.e;
    array(j, VC::IMx) = u_j.m(Tags::DirX);
}

template <>
KOKKOS_FORCEINLINE_FUNCTION void BaseKernel<two_d>::set(
        const RealArray<two_d>& array,
        Int j,
        const ConsState& u_j) noexcept
{
    array(j, VC::ID) = u_j.d;
    array(j, VC::IE) = u_j.e;
    array(j, VC::IMx) = u_j.m(Tags::DirX);
    array(j, VC::IMy) = u_j.m(Tags::DirY);
}

template <>
KOKKOS_FORCEINLINE_FUNCTION void BaseKernel<three_d>::set(
        const RealArray<three_d>& array,
        Int j,
        const ConsState& u_j) noexcept
{
    array(j, VC::ID) = u_j.d;
    array(j, VC::IE) = u_j.e;
    array(j, VC::IMx) = u_j.m(Tags::DirX);
    array(j, VC::IMy) = u_j.m(Tags::DirY);
    array(j, VC::IMz) = u_j.m(Tags::DirZ);
}

} // namespace ark
