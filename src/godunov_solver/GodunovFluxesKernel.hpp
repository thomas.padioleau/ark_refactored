#pragma once

#include <Kokkos_Core.hpp>

#include "BaseKernel.hpp"
#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim, class RiemannSolver>
class FluxesAndUpdateKernel : public BaseKernel<dim>
{
    using Super = BaseKernel<dim>;

    using Euler = EulerSystem<dim>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;

public:
    using TeamPolicy = Kokkos::TeamPolicy<Kokkos::IndexType<Int>>;
    using Team = typename TeamPolicy::member_type;

    static constexpr int ghostDepth = 0;

    FluxesAndUpdateKernel(
            const Params<dim>&,
            const UniformGrid<dim>& grid,
            const RealArray<dim>& u,
            const RealArray<dim>& q,
            const RiemannSolver& riemman_solver,
            Real dt)
        : m_u(u)
        , m_q(q)
        , m_grid(grid)
        , m_riemann_solver(riemman_solver)
        , m_dt(dt)
        , m_x_beg(grid.m_ghostWidths[IX] - ghostDepth)
        , m_x_end(grid.m_ghostWidths[IX] + grid.m_nbCells[IX] + ghostDepth)
    {
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const Team& team) const
    {
        // From given team compute linear index of coordinates of cell (0, jy, jz).
        const Int j0 = Super::teamToLinearIndex(team, m_grid, ghostDepth);

        Kokkos::parallel_for(ark::TeamVectorRange(team, m_x_beg, m_x_end), [&](const Int jx) {
            const Int j = j0 + jx;
            ConsState u_j = Super::getCons(m_u, j);

            updateAlongFace(Tags::DirX, Tags::SideL, j, u_j);
            updateAlongFace(Tags::DirX, Tags::SideR, j, u_j);
            updateAlongFace(Tags::DirY, Tags::SideL, j, u_j);
            updateAlongFace(Tags::DirY, Tags::SideR, j, u_j);
            updateAlongFace(Tags::DirZ, Tags::SideL, j, u_j);
            updateAlongFace(Tags::DirZ, Tags::SideR, j, u_j);

            Super::set(m_u, j, u_j);
        });
    }

    template <int idir, int iside, std::enable_if_t<(idir >= dim), int> = 0>
    KOKKOS_FORCEINLINE_FUNCTION void updateAlongFace(
            std::integral_constant<int, idir>,
            std::integral_constant<int, iside>,
            Int,
            ConsState&) const
    {
    }

    template <int idir, int iside, std::enable_if_t<(idir < dim), int> = 0>
    KOKKOS_FORCEINLINE_FUNCTION void updateAlongFace(
            std::integral_constant<int, idir> idir_tag,
            std::integral_constant<int, iside> iside_tag,
            Int j,
            ConsState& u_j) const
    {
        using namespace constants;

        const Int j_L = iside == 0 ? j - m_grid.m_strides[idir] : j;
        const Int j_R = iside == 0 ? j : j + m_grid.m_strides[idir];

        const PrimState q_L = Super::getPrim(m_q, j_L);
        const PrimState q_R = Super::getPrim(m_q, j_R);

        const ConsState flux = m_riemann_solver(q_L, q_R, idir_tag, iside_tag);

        const Real side = iside == 0 ? -one : +one;
        const Real sdtdSdV = side * m_dt * m_grid.getdSdV(j, idir_tag, iside_tag);
        u_j.d -= sdtdSdV * flux.d;
        u_j.m -= sdtdSdV * flux.m;
        u_j.e -= sdtdSdV * flux.e;
    }

    RealArray<dim> m_u;
    ConstRealArray<dim> m_q;
    UniformGrid<dim> m_grid;
    RiemannSolver m_riemann_solver;
    Real m_dt;
    Int m_x_beg;
    Int m_x_end;
};

} // namespace ark
