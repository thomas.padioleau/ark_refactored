#include <Kokkos_Core.hpp>

#include "ars/HydroRiemannAllRegime.hpp"
#include "ars/HydroRiemannAllRegimeDev.hpp"
#include "ars/HydroRiemannHllc.hpp"
#include "ars/HydroRiemannRusanov.hpp"

#include "GodunovFluxesExecution.hpp"
#include "GodunovFluxesKernel.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

namespace
{

template <dim_t dim, class RiemannSolver>
void ExecuteFluxesAndUpdate(
        const Params<dim>& params,
        const UniformGrid<dim>& grid,
        const RealArray<dim>& u,
        const RealArray<dim>& q,
        const RiemannSolver& riemann_solver,
        Real dt)
{
    using Kernel = FluxesAndUpdateKernel<dim, RiemannSolver>;
    using TeamPolicy = typename Kernel::TeamPolicy;
    const int league_size = Kernel::computeLeagueSize(grid.m_nbCells, Kernel::ghostDepth);
    const int vector_length = TeamPolicy::vector_length_max();
    const TeamPolicy policy(league_size, Kokkos::AUTO, vector_length);
    const Kernel kernel(params, grid, u, q, riemann_solver, dt);
    Kokkos::parallel_for("Godunov kernel - TeamPolicy", policy, kernel);
}

} // namespace

template <dim_t dim>
void GodunovOperator<dim>::execute(
        Params<dim> const& params,
        UniformGrid<dim> const& grid,
        RealArray<dim> const& u,
        RealArray<dim> const& q,
        Real dt) const
{
    typename EulerSystem<dim>::EquationOfState eos(params.thermo);
    if (params.run.riemann == "rusanov")
    {
        ExecuteFluxesAndUpdate(params, grid, u, q, riemann::Rusanov<dim>(eos), dt);
    }
    else if (params.run.riemann == "hllc")
    {
        ExecuteFluxesAndUpdate(params, grid, u, q, riemann::Hllc<dim>(eos), dt);
    }
    else if (params.run.riemann == "all_regime")
    {
        ExecuteFluxesAndUpdate(params, grid, u, q, riemann::AllRegime<dim>(params, eos), dt);
    }
    else if (params.run.riemann == "all_regime_dev")
    {
        ExecuteFluxesAndUpdate(params, grid, u, q, riemann::AllRegimeDev<dim>(params, eos), dt);
    }
    else
    {
        throw;
    }
}

template class GodunovOperator<one_d>;
template class GodunovOperator<two_d>;
template class GodunovOperator<three_d>;

} // namespace ark
