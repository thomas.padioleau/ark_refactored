#include <chrono>
#include <iomanip>
#include <limits>
#include <memory>
#include <utility>
#include <vector>

#include "io/Reader.hpp"
#include "io/Writer.hpp"

#include "ConservativeToPrimitiveExecution.hpp"
#include "Constants.hpp"
#include "DistributedMemorySession.hpp"
#include "Godunov.hpp"
#include "GodunovFluxesExecution.hpp"
#include "GravityStepExecution.hpp"
#include "Print.hpp"
#include "Problem.hpp"
#include "Solver.hpp"
#include "ThermalDiffusionStepExecution.hpp"
#include "TimeStep.hpp"
#include "TimeStepExecution.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"
#include "Utils.hpp"
#include "ViscousStepExecution.hpp"

namespace ark
{

template <dim_t dim>
GodunovSolver<dim>::GodunovSolver(const std::shared_ptr<Problem<dim>>& problem)
    : Solver()
    , m_problem(problem)
    , m_params(problem->m_params)
    , m_grid(std::make_shared<UniformGrid<dim>>(
              m_params->mesh.low,
              m_params->mesh.up,
              m_params->mesh.nbCells,
              m_params->mesh.dom,
              ghostWidth))
    , m_writer()
    , m_u("U", m_grid->nbCells())
    , m_q("Q", m_grid->nbCells())
    , m_u_host(Kokkos::create_mirror(m_u))
{
    if (m_params->hydro.hydro_enabled)
    {
        m_euler_operator = std::make_unique<GodunovOperator<dim>>();
    }
    else
    {
        m_euler_operator = std::make_unique<NullEulerOperator<dim>>();
    }

    m_conservative_to_primitive_operator = std::make_unique<ConservativeToPrimitiveOperator<dim>>();

    if (m_params->hydro.gravity_enabled)
    {
        m_gravity_operator = std::make_unique<GravityOperator<dim>>();
    }
    else
    {
        m_gravity_operator = std::make_unique<NullGravityOperator<dim>>();
    }

    if (m_params->hydro.viscosity_enabled)
    {
        m_viscosity_operator = std::make_unique<ViscousOperator<dim>>();
    }
    else
    {
        m_viscosity_operator = std::make_unique<NullViscousOperator<dim>>();
    }

    if (m_params->hydro.thermal_diffusion_enabled)
    {
        m_thermal_operator = std::make_unique<ThermalOperator<dim>>();
    }
    else
    {
        m_thermal_operator = std::make_unique<NullThermalOperator<dim>>();
    }

    std::vector<std::string> var_names = Euler::cons_names();
    std::vector<std::pair<int, std::string>> variables_to_save;
    variables_to_save.reserve(Euler::nbvar);
    for (int ivar = 0; ivar < Euler::nbvar; ++ivar)
    {
        variables_to_save.emplace_back(ivar, var_names[ivar]);
    }

    m_writer = io::WriterFactory<dim>::
            New(*m_grid,
                *m_params,
                m_params->output.type,
                m_params->output.prefix,
                variables_to_save);

    if (m_params->run.restart)
    {
        Int outputId = -1;
        Int restartId = -1;
        io::Reader<dim> reader(*m_grid, *m_params, variables_to_save);
        reader.read(m_u_host, *m_grid, Super::m_iteration, Super::m_t, outputId, restartId);
        Kokkos::deep_copy(m_u, m_u_host);
        m_writer->setOutputId(++outputId);
        m_writer->setRestartId(++restartId);
    }
    else
    {
        m_problem->initialize(m_u, *m_grid);
    }
    m_problem->make_boundaries(m_u, *m_grid);
    m_conservative_to_primitive_operator->execute(*m_params, *m_grid, m_u, m_q);
}

template <dim_t dim>
Real GodunovSolver<dim>::computeTimeStep()
{
    const TimeStep multi_dt = ExecuteTimeStep(*m_params, *m_grid, m_q);
    return multi_dt.min();
}

template <dim_t dim>
void GodunovSolver<dim>::nextIteration(Real dt)
{
    m_euler_operator->execute(*m_params, *m_grid, m_u, m_q, dt);

    m_gravity_operator->execute(*m_params, *m_grid, m_u, m_q, dt);

    m_viscosity_operator->execute(*m_params, *m_grid, m_q, m_u, dt);

    m_thermal_operator->execute(*m_params, *m_grid, m_q, m_u, dt);

    // fill ghost cell in data_in
    m_problem->make_boundaries(m_u, *m_grid);

    // convert conservative variable into primitives ones for the entire domain
    m_conservative_to_primitive_operator->execute(*m_params, *m_grid, m_u, m_q);

    Super::m_t += dt;
    ++Super::m_iteration;
}

template <dim_t dim>
void GodunovSolver<dim>::saveOutput()
{
    Kokkos::deep_copy(m_u_host, m_u);
    m_writer
            ->write(m_u_host,
                    *m_grid,
                    Super::m_iteration,
                    Super::m_t,
                    m_params->thermo.gamma,
                    m_params->thermo.mmw);
}

template <dim_t dim>
std::size_t GodunovSolver<dim>::memoryUsage() const
{
    return (m_u.span() + m_q.span()) * sizeof(Real);
}

template class GodunovSolver<one_d>;
template class GodunovSolver<two_d>;
template class GodunovSolver<three_d>;

} // namespace ark
