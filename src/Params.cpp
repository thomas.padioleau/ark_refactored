#include <iomanip>
#include <iostream>
#include <limits>
#include <string>
#include <vector>

#include "inih/INIReader.hpp"
#include "utils/Stringify.hpp"

#include "Params.hpp"
#include "Print.hpp"
#include "Types.hpp"

namespace ark
{

const std::vector<std::string> X {"x", "y", "z"};

RunParams::RunParams(const INIReader& reader)
{
    nStepmax = reader.GetInteger(section, "nStepmax", nStepmax);
    info = reader.GetInteger(section, "info", info);
    tEnd = reader.GetReal(section, "tEnd", tEnd);
    cfl = reader.GetReal(section, "cfl", cfl);
    solver = reader.Get(section, "solver", solver);
    riemann = reader.Get(section, "riemann", riemann);
    restart = reader.GetBoolean(section, "restart", restart);
    restart_filename = reader.Get(section, "restart_filename", restart_filename);
}

template <dim_t dim>
HydroParams<dim>::HydroParams(const INIReader& reader)
{
    hydro_enabled = reader.GetBoolean(section, "hydro_enabled", hydro_enabled);
    gravity_enabled = reader.GetBoolean(section, "gravity_enabled", gravity_enabled);
    thermal_diffusion_enabled
            = reader.GetBoolean(section, "thermal_diffusion_enabled", thermal_diffusion_enabled);
    viscosity_enabled = reader.GetBoolean(section, "viscosity_enabled", viscosity_enabled);
    for (int idim = 0; idim < dim; ++idim)
    {
        g[idim] = reader.GetReal(section, "g_" + X[idim], g[idim]);
    }
    kappa = reader.GetReal(section, "kappa", kappa);
    mu = reader.GetReal(section, "mu", mu);
    K = reader.GetReal(section, "K", K);
}

template <dim_t dim>
MeshParams<dim>::MeshParams(const INIReader& reader)
{
    for (int idim = 0; idim < dim; ++idim)
    {
        nbCells[idim] = reader.GetInteger(section, 'n' + X[idim], nbCells[idim]);
        dom[idim] = reader.GetInteger(section, 'm' + X[idim], dom[idim]);
        low[idim] = reader.GetReal(section, X[idim] + "min", low[idim]);
        up[idim] = reader.GetReal(section, X[idim] + "max", up[idim]);
        boundaryTypes[IL + 2 * idim] = reader
                                               .Get(section,
                                                    "boundary_type_" + X[idim] + "min",
                                                    boundaryTypes[IL + 2 * idim]);
        boundaryTypes[IR + 2 * idim] = reader
                                               .Get(section,
                                                    "boundary_type_" + X[idim] + "max",
                                                    boundaryTypes[IR + 2 * idim]);
    }
}

OutputParams::OutputParams(const INIReader& reader)
{
    type = reader.Get(section, "type", type);
    directory = reader.Get(section, "directory", directory);
    format = reader.Get(section, "format", format);
    prefix = reader.Get(section, "prefix", prefix);
    dt_io = reader.GetReal(section, "dt_io", dt_io);
    nOutput = reader.GetInteger(section, "nOutput", nOutput);
}

template <dim_t dim>
Params<dim>::Params(const std::string& filename)
    : reader {INIReader {filename}}
    , run {reader}
    , hydro {reader}
    , mesh {reader}
    , output {reader}
    , thermo {reader}
{
    problem = reader.Get("problem", "name", problem);
}

template <dim_t dim>
void Params<dim>::print(std::ostream& os) const
{
    const std::vector<std::string> v2 {"i", "j", "k"};
    const int len1 = 40;
    const int len2 = 40;
    const int len3 = 10;

    os << std::scientific;
    os << std::setprecision(std::numeric_limits<Real>::digits10);

    os << std::string(len1 + len2, '#') << std::endl;
    os << std::string(len1 - len3, '#');
    os << std::left << std::setw(len2 + len3) << std::setfill('#') << " Run parameters " << '\n';
    os << utils::stringify("solver", run.solver) << "\n";
    os << utils::stringify("riemann", run.riemann) << "\n";
    os << utils::stringify("cfl", run.cfl) << "\n";
    os << utils::stringify("tEnd", run.tEnd) << "\n";
    os << utils::stringify("nStepmax", run.nStepmax) << "\n";
    os << utils::stringify("info", run.info) << "\n";
    os << utils::stringify("restart", run.restart) << "\n";
    os << utils::stringify("restart_filename", run.restart_filename) << "\n";

    os << std::string(len1 - len3, '#');
    os << std::left << std::setw(len2 + len3) << std::setfill('#') << " Hydro parameters " << '\n';
    os << utils::stringify("kappa", hydro.kappa) << "\n";
    os << utils::stringify("mu", hydro.mu) << "\n";
    os << utils::stringify("K", hydro.K) << "\n";
    for (int idim = 0; idim < dim; ++idim)
    {
        os << utils::stringify('g' + X[idim], hydro.g[idim]) << "\n";
    }

    os << std::string(len1 - len3, '#');
    os << std::left << std::setw(len2 + len3) << std::setfill('#') << " Thermo parameters " << '\n';
    os << utils::stringify("Eos type", thermo.type) << "\n";
    os << utils::stringify("mmw", thermo.mmw) << "\n";
    os << utils::stringify("gamma", thermo.gamma) << "\n";
    os << utils::stringify("p0", thermo.p0) << "\n";

    os << std::string(len1 - len3, '#');
    os << std::left << std::setw(len2 + len3) << std::setfill('#') << " Mesh parameters " << '\n';
    for (int idim = 0; idim < dim; ++idim)
    {
        os << utils::stringify('n' + X[idim], mesh.nbCells[idim]) << "\n";
    }
    for (int idim = 0; idim < dim; ++idim)
    {
        os << utils::stringify('m' + X[idim], mesh.dom[idim]) << "\n";
    }
    for (int idim = 0; idim < dim; ++idim)
    {
        os << utils::stringify(X[idim] + "min", mesh.low[idim]) << "\n";
        os << utils::stringify(X[idim] + "max", mesh.up[idim]) << "\n";
    }
    for (int idim = 0; idim < dim; ++idim)
    {
        os << utils::stringify("boundary type " + X[idim] + "min", mesh.boundaryTypes[0 + 2 * idim])
           << "\n";
        os << utils::stringify("boundary type " + X[idim] + "max", mesh.boundaryTypes[1 + 2 * idim])
           << "\n";
    }

    os << std::string(len1 - len3, '#');
    os << std::left << std::setw(len2 + len3) << std::setfill('#') << " Output parameters " << '\n';
    os << utils::stringify("type", output.type) << "\n";
    os << utils::stringify("directory", output.directory) << "\n";
    os << utils::stringify("format", output.format) << "\n";
    os << utils::stringify("prefix", output.prefix) << "\n";
    os << utils::stringify("nOutput", output.nOutput) << "\n";
    os << utils::stringify("dt_io", output.dt_io) << "\n";
    os << std::string(len1 + len2, '#') << std::endl;
}

template struct HydroParams<one_d>;
template struct HydroParams<two_d>;
template struct HydroParams<three_d>;

template struct MeshParams<one_d>;
template struct MeshParams<two_d>;
template struct MeshParams<three_d>;

template struct Params<one_d>;
template struct Params<two_d>;
template struct Params<three_d>;

} // namespace ark
