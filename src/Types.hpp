#pragma once

#include <Kokkos_Core.hpp>
#include <Kokkos_DualView.hpp>

namespace ark
{

#if defined(FLOAT64_EXTENDED)
using Real = long double;
#elif defined(FLOAT64)
using Real = double;
#elif defined(FLOAT32)
using Real = float;
#else
static_assert(false, "You must define a floating point type")
#endif

#if defined(INT64)
using Int = long int;
#elif defined(INT32)
using Int = int;
#else
static_assert(false, "You must define an integer type")
#endif

enum dim_t : Int
{
    one_d = 1,
    two_d = 2,
    three_d = 3
};

enum dir_t : Int
{
    IX = 0,
    IY = 1,
    IZ = 2
};

enum side_t : int
{
    IL = 0,
    IR = 1
};

struct Tags
{
    template <int i>
    using Tag_t = std::integral_constant<int, i>;

    using Dim1_t = Tag_t<one_d>;
    using Dim2_t = Tag_t<two_d>;
    using Dim3_t = Tag_t<three_d>;
    static constexpr Dim1_t Dim1 = Dim1_t();
    static constexpr Dim2_t Dim2 = Dim2_t();
    static constexpr Dim3_t Dim3 = Dim3_t();

    using DirX_t = Tag_t<IX>;
    using DirY_t = Tag_t<IY>;
    using DirZ_t = Tag_t<IZ>;
    static constexpr DirX_t DirX = DirX_t();
    static constexpr DirY_t DirY = DirY_t();
    static constexpr DirZ_t DirZ = DirZ_t();

    using SideL_t = Tag_t<IL>;
    using SideR_t = Tag_t<IR>;
    static constexpr SideL_t SideL = SideL_t();
    static constexpr SideR_t SideR = SideR_t();
};

using Layout = Kokkos::LayoutLeft;

template <Int size>
using IntVector = Kokkos::Array<Int, size>;

template <dim_t dim>
using RealVector = Kokkos::Array<Real, dim>;

// Arrays with compile-time dimension
template <class T, dim_t dim>
using Array = Kokkos::View<T * [dim + 2], Layout>;

template <dim_t dim>
using RealArray = Array<Real, dim>;

template <dim_t dim>
using RealDualArray = Kokkos::DualView<Real * [dim + 2], Layout>;

template <dim_t dim>
using ConstRealArray = Kokkos::View<const Real * [dim + 2], Layout>;

template <dim_t dim>
using HostRealArray = typename RealArray<dim>::HostMirror;

template <dim_t dim>
using HostConstRealArray = typename ConstRealArray<dim>::HostMirror;

// Arrays with run-time dimension
using ArrayDyn = Kokkos::View<Real**, Layout>;
using ConstArrayDyn = Kokkos::View<const Real**, Layout>;
using HostArrayDyn = ArrayDyn::HostMirror;
using HostConstArrayDyn = ConstArrayDyn::HostMirror;

} // namespace ark
