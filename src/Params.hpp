#pragma once

#include <iosfwd>
#include <string>

#include "inih/INIReader.hpp"
#include "thermodynamics/ThermoParams.hpp"

#include "Constants.hpp"
#include "Types.hpp"
#include "Utils.hpp"

namespace ark
{

struct RunParams
{
    RunParams() = default;

    explicit RunParams(const INIReader& reader);

    Int nStepmax = 0;
    Int info = 100;
    Real tEnd = constants::infinity;
    Real cfl = constants::half;
    std::string solver = "unknown";
    std::string riemann = "unknown";
    bool restart = false;
    std::string restart_filename = "";
    std::string section = "run";
};

template <dim_t dim>
struct HydroParams
{
    HydroParams() = default;

    explicit HydroParams(const INIReader& reader);

    bool hydro_enabled = true;
    bool gravity_enabled = false;
    bool thermal_diffusion_enabled = false;
    bool viscosity_enabled = false;
    RealVector<dim> g = {utils::make_array<Real, dim>(constants::zero)};
    Real kappa = constants::zero;
    Real mu = constants::zero;
    Real K = constants::one + constants::tenth;
    std::string section = "hydro";
};

template <dim_t dim>
struct MeshParams
{
    MeshParams() = default;

    explicit MeshParams(const INIReader& reader);

    IntVector<dim> nbCells = {utils::make_array<Int, dim>(2)};
    IntVector<dim> dom = {utils::make_array<Int, dim>(1)};
    RealVector<dim> low = {utils::make_array<Real, dim>(constants::zero)};
    RealVector<dim> up = {utils::make_array<Real, dim>(constants::one)};
    Kokkos::Array<std::string, 2 * dim> boundaryTypes
            = {utils::make_array<std::string, 2 * dim>("periodic")};
    std::string section = "mesh";
};

struct OutputParams
{
    OutputParams() = default;

    explicit OutputParams(const INIReader& reader);

    std::string type = "vtk";
    std::string directory = ".";
    std::string format = "appended";
    std::string prefix = "output";
    Real dt_io = -constants::one;
    Int nOutput = 10;
    std::string section = "output";
};

template <dim_t dim>
struct Params
{
    Params() = default;

    explicit Params(const std::string& filename);

    void print(std::ostream& os) const;

    INIReader reader = INIReader("");
    RunParams run = RunParams();
    HydroParams<dim> hydro = HydroParams<dim>();
    MeshParams<dim> mesh = MeshParams<dim>();
    OutputParams output = OutputParams();
    thermodynamics::ThermoParams thermo = thermodynamics::ThermoParams();
    std::string problem = "unknown";
};

extern template struct HydroParams<one_d>;
extern template struct HydroParams<two_d>;
extern template struct HydroParams<three_d>;

extern template struct MeshParams<one_d>;
extern template struct MeshParams<two_d>;
extern template struct MeshParams<three_d>;

extern template struct Params<one_d>;
extern template struct Params<two_d>;
extern template struct Params<three_d>;

} // namespace ark
