#pragma once

#include <cmath>

#include <Kokkos_Core.hpp>

#include "BaseKernel.hpp"
#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "TimeStep.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
class TimeStepKernel : public BaseKernel<dim>
{
    using Super = BaseKernel<dim>;

    using Euler = EulerSystem<dim>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;
    using VC = typename Euler::VarCons;
    using VP = typename Euler::VarPrim;

public:
    TimeStepKernel(const Params<dim>& params, const UniformGrid<dim>& grid, RealArray<dim> q)
        : m_grid(grid)
        , m_eos(params.thermo)
        , m_gamma(params.thermo.gamma)
        , m_kappa(params.hydro.kappa)
        , m_cp(m_gamma / (m_gamma - constants::one) * code_units::constants::Rstar_h)
        , m_mu(params.hydro.mu)
        , m_q(q)
    {
    }

    KOKKOS_INLINE_FUNCTION
    void init(TimeStep& dst) const
    {
        using constants::zero;
        dst.hydro = zero;
        dst.thermal = zero;
        dst.viscous = zero;
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(Int j, TimeStep& invDt) const
    {
        using namespace constants;

        const PrimState q_j = Super::getPrim(m_q, j);
        // get primitive variables in current cell
        const Real c_j = m_eos.computeSpeedOfSound(q_j.d, q_j.p);

        Real invDtLoc = zero;
        for (int iface = 0; iface < m_grid.faces(j); ++iface)
        {
            const int idim = iface / 2;

            const Real un_j = q_j.v(IX + idim);
            invDtLoc += m_grid.m_invdl[idim] * (c_j + std::fabs(un_j));
        }
        if (invDt.hydro < invDtLoc)
        {
            invDt.hydro = invDtLoc;
        }

        // Heat flux
        invDtLoc = zero;
        for (int iface = 0; iface < m_grid.faces(j); ++iface)
        {
            const int idim = iface / 2;

            invDtLoc += m_kappa / (q_j.d * m_cp) * m_grid.m_invdl[idim] * m_grid.m_invdl[idim];
        }
        if (invDt.thermal < invDtLoc)
        {
            invDt.thermal = invDtLoc;
        }

        // Viscous flux
        // (1.0*mu+abs(-2.0/3.0*mu)=8.0/3.0*mu
        //  formula still needs some justification)
        invDtLoc = zero;
        for (int iface = 0; iface < m_grid.faces(j); ++iface)
        {
            const int idim = iface / 2;

            invDtLoc += eight / three * m_mu / q_j.d * m_grid.m_invdl[idim] * m_grid.m_invdl[idim];
        }
        if (invDt.viscous < invDtLoc)
        {
            invDt.viscous = invDtLoc;
        }
    }

    KOKKOS_INLINE_FUNCTION
    void join(volatile TimeStep& dst, const volatile TimeStep& src) const
    {
        // max reduce
        if (dst.hydro < src.hydro)
        {
            dst.hydro = src.hydro;
        }
        if (dst.thermal < src.thermal)
        {
            dst.thermal = src.thermal;
        }
        if (dst.viscous < src.viscous)
        {
            dst.viscous = src.viscous;
        }
    }

    KOKKOS_INLINE_FUNCTION
    void join(TimeStep& dst, const TimeStep& src) const
    {
        // max reduce
        if (dst.hydro < src.hydro)
        {
            dst.hydro = src.hydro;
        }
        if (dst.thermal < src.thermal)
        {
            dst.thermal = src.thermal;
        }
        if (dst.viscous < src.viscous)
        {
            dst.viscous = src.viscous;
        }
    }

    UniformGrid<dim> m_grid;
    EquationOfState m_eos;
    Real m_gamma;
    Real m_kappa;
    Real m_cp;
    Real m_mu;
    ConstRealArray<dim> m_q;
};

} // namespace ark
