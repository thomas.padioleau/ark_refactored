#pragma once

#include "Params.hpp"
#include "TimeStep.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
TimeStep ExecuteTimeStep(
        const Params<dim>& params,
        const UniformGrid<dim>& grid,
        const RealArray<dim>& q);

#define ETI_ExecuteTimeStep(dim)                                                                   \
    TimeStep ExecuteTimeStep(                                                                      \
            const Params<dim>& params,                                                             \
            const UniformGrid<dim>& grid,                                                          \
            const RealArray<dim>& q)

extern template ETI_ExecuteTimeStep(one_d);
extern template ETI_ExecuteTimeStep(two_d);
extern template ETI_ExecuteTimeStep(three_d);

} // namespace ark
