#include "ConservativeToPrimitiveExecution.hpp"
#include "ConservativeToPrimitiveKernel.hpp"

namespace ark
{

template <dim_t dim>
void ConservativeToPrimitiveOperator<dim>::execute(
        Params<dim> const& params,
        UniformGrid<dim> const& grid,
        RealArray<dim> const& u,
        RealArray<dim> const& q) const
{
    ConservativeToPrimitiveKernel<dim> kernel(params, grid, u, q);
    Kokkos::RangePolicy<Int> range(0, grid.nbCells());
    Kokkos::parallel_for("C2P kernel", range, kernel);
}

template class ConservativeToPrimitiveOperator<one_d>;
template class ConservativeToPrimitiveOperator<two_d>;
template class ConservativeToPrimitiveOperator<three_d>;

} // namespace ark
