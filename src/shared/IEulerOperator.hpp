#pragma once

#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
class IEulerOperator
{
public:
    virtual ~IEulerOperator() = default;

    virtual void execute(
            const Params<dim>& params,
            const UniformGrid<dim>& grid,
            const RealArray<dim>& u,
            const RealArray<dim>& q,
            Real dt) const = 0;
};

template <dim_t dim>
class NullEulerOperator : public IEulerOperator<dim>
{
public:
    void execute(
            Params<dim> const&,
            UniformGrid<dim> const&,
            RealArray<dim> const&,
            RealArray<dim> const&,
            Real) const override
    {
    }
};

} // namespace ark
