#pragma once

#include "FillingCurve.hpp"
#include "Params.hpp"
#include "Types.hpp"

namespace ark
{

template <dim_t dim>
void ExecuteCopyFromBuffer(
        const RealArray<dim>& u,
        const RealArray<dim>& buffer,
        const StridedFillingCurve<dim>& curve,
        int idim,
        int side);

#define ETI_ExecuteCopyFromBuffer(dim)                                                             \
    void ExecuteCopyFromBuffer(                                                                    \
            const RealArray<dim>& u,                                                               \
            const RealArray<dim>& buffer,                                                          \
            const StridedFillingCurve<dim>& curve,                                                 \
            int idim,                                                                              \
            int side)

extern template ETI_ExecuteCopyFromBuffer(one_d);
extern template ETI_ExecuteCopyFromBuffer(two_d);
extern template ETI_ExecuteCopyFromBuffer(three_d);

} // namespace ark
