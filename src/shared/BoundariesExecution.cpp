#include <string>

#include "BoundariesExecution.hpp"
#include "BoundariesKernel.hpp"

namespace ark
{

template <dim_t dim>
void ExecuteBoundaries(
        const RealArray<dim>& u,
        const UniformGrid<dim>& grid,
        int idim,
        int side,
        std::string const& boundaryType)
{
    Int nbiter = 1;
    for (int idim2 = 0; idim2 < dim; ++idim2)
    {
        nbiter *= idim2 == idim ? grid.m_ghostWidths[idim2]
                                : grid.m_nbCells[idim2] + 2 * grid.m_ghostWidths[idim2];
    }

    if (boundaryType == "reflexive")
    {
        Kokkos::RangePolicy<int, ReflexiveTag> range(0, nbiter);
        BoundariesKernel<dim> kernel(u, grid, idim, side);
        Kokkos::parallel_for("Reflexive boundary kernel - RangePolicy", range, kernel);
    }
    else if (boundaryType == "neumann")
    {
        Kokkos::RangePolicy<int, NeumannTag> range(0, nbiter);
        BoundariesKernel<dim> kernel(u, grid, idim, side);
        Kokkos::parallel_for("Neumann boundary kernel - RangePolicy", range, kernel);
    }
    else if (boundaryType == "periodic")
    {
        Kokkos::RangePolicy<int, PeriodicTag> range(0, nbiter);
        BoundariesKernel<dim> kernel(u, grid, idim, side);
        Kokkos::parallel_for("Periodic boundary kernel - RangePolicy", range, kernel);
    }
    else if (boundaryType == "adhesive")
    {
        Kokkos::RangePolicy<int, AdhesiveTag> range(0, nbiter);
        BoundariesKernel<dim> kernel(u, grid, idim, side);
        Kokkos::parallel_for("Periodic boundary kernel - RangePolicy", range, kernel);
    }
    else
    {
        throw std::runtime_error("Invalid boundary condition.");
    }
}

template ETI_ExecuteBoundaries(one_d);
template ETI_ExecuteBoundaries(two_d);
template ETI_ExecuteBoundaries(three_d);

} // namespace ark
