#include <cmath>

#include <Kokkos_Core.hpp>

#include "linalg/Tensor.hpp"
#include "linalg/Vector.hpp"

#include "BaseKernel.hpp"
#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"
#include "ViscousStepExecution.hpp"
#include "ViscousStepKernel.hpp"

namespace ark
{

template <dim_t dim>
void ViscousOperator<dim>::execute(
        Params<dim> const& params,
        UniformGrid<dim> const& grid,
        RealArray<dim> const& q,
        RealArray<dim> const& u,
        Real const dt) const
{
    using Kernel = ViscousStepKernel<dim>;
    using TeamPolicy = typename Kernel::TeamPolicy;
    Kernel kernel(params, grid, q, u, dt);
    const int league_size = Kernel::computeLeagueSize(grid.m_nbCells, Kernel::ghostDepth);
    const int vector_length = TeamPolicy::vector_length_max();
    TeamPolicy policy(league_size, Kokkos::AUTO, vector_length);
    Kokkos::parallel_for("Viscosity kernel", policy, kernel);
}

template class ViscousOperator<one_d>;
template class ViscousOperator<two_d>;
template class ViscousOperator<three_d>;

} // namespace ark
