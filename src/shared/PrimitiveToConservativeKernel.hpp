#pragma once

#include <cmath>

#include <Kokkos_Core.hpp>

#include "BaseKernel.hpp"
#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
class PrimitiveToConservativeKernel : public BaseKernel<dim>
{
    using Super = BaseKernel<dim>;

    using Euler = EulerSystem<dim>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;

public:
    PrimitiveToConservativeKernel(
            const Params<dim>& params,
            const UniformGrid<dim>& grid,
            const RealArray<dim>& q,
            const RealArray<dim>& u)
        : m_grid(grid)
        , m_u(u)
        , m_q(q)
        , m_eos(params.thermo)
    {
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(Int j) const
    {
        const PrimState q_j = Super::getPrim(m_q, j);
        const ConsState u_j = Euler::primitiveToConservative(q_j, m_eos);
        Super::set(m_u, j, u_j);
    }

    UniformGrid<dim> m_grid;
    RealArray<dim> m_u;
    RealArray<dim> m_q;
    EquationOfState m_eos;
};

} // namespace ark
