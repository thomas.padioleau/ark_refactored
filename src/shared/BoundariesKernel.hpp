#pragma once

#include <exception>

#include "BaseKernel.hpp"
#include "FillingCurve.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

struct ReflexiveTag
{
};
struct AdhesiveTag
{
};
struct NeumannTag
{
};
struct PeriodicTag
{
};

template <dim_t dim>
class BoundariesKernel : public BaseKernel<dim>
{
    using Super = BaseKernel<dim>;

    using Euler = EulerSystem<dim>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;

public:
    BoundariesKernel(const RealArray<dim>& u, const UniformGrid<dim>& grid, int idim, int side)
        : m_u(u)
        , m_grid(grid)
        , m_idim(idim)
        , m_side(side)
        , m_boundary_curve()
    {
        IntVector<dim> nbCells;
        for (int idim2 = 0; idim2 < dim; ++idim2)
        {
            nbCells[idim2] = idim2 == m_idim
                                     ? m_grid.m_ghostWidths[idim2]
                                     : m_grid.m_nbCells[idim2] + 2 * m_grid.m_ghostWidths[idim2];
        }
        m_boundary_curve = StridedFillingCurve<dim>(nbCells, 0);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const ReflexiveTag&, Int j) const
    {
        IntVector<dim> coords = m_boundary_curve.indexToCoord(j);
        coords[m_idim] += m_side == 0 ? 0 : m_grid.m_ghostWidths[m_idim] + m_grid.m_nbCells[m_idim];
        IntVector<dim> coords0 = coords;
        coords0[m_idim] = ((m_side == 1) ? 2 * m_grid.m_nbCells[m_idim] : 0)
                          + 2 * m_grid.m_ghostWidths[m_idim] - 1 - coords[m_idim];
        const Int j0 = m_grid.coordToIndex(coords0);
        ConsState u_j0 = Super::getCons(m_u, j0);
        u_j0.m(m_idim) = -u_j0.m(m_idim);

        Super::set(m_u, m_grid.coordToIndex(coords), u_j0);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const AdhesiveTag&, Int j) const
    {
        IntVector<dim> coords = m_boundary_curve.indexToCoord(j);
        coords[m_idim] += m_side == 0 ? 0 : m_grid.m_ghostWidths[m_idim] + m_grid.m_nbCells[m_idim];
        IntVector<dim> coords0 = coords;
        coords0[m_idim] = ((m_side == 1) ? 2 * m_grid.m_nbCells[m_idim] : 0)
                          + 2 * m_grid.m_ghostWidths[m_idim] - 1 - coords[m_idim];
        const Int j0 = m_grid.coordToIndex(coords0);
        ConsState u_j0 = Super::getCons(m_u, j0);
        for (int idim = 0; idim < dim; ++idim)
        {
            u_j0.m(idim) = -u_j0.m(idim);
        }

        Super::set(m_u, m_grid.coordToIndex(coords), u_j0);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const NeumannTag&, Int j) const
    {
        IntVector<dim> coords = m_boundary_curve.indexToCoord(j);
        coords[m_idim] += m_side == 0 ? 0 : m_grid.m_ghostWidths[m_idim] + m_grid.m_nbCells[m_idim];

        IntVector<dim> coords0 = coords;
        coords0[m_idim]
                = m_grid.m_ghostWidths[m_idim] + ((m_side == 1) ? m_grid.m_nbCells[m_idim] - 1 : 0);
        const Int j0 = m_grid.coordToIndex(coords0);
        Super::copy(m_u, m_grid.coordToIndex(coords), j0);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const PeriodicTag&, Int j) const
    {
        IntVector<dim> coords = m_boundary_curve.indexToCoord(j);
        coords[m_idim] += m_side == 0 ? 0 : m_grid.m_ghostWidths[m_idim] + m_grid.m_nbCells[m_idim];

        IntVector<dim> coords0 = coords;
        coords0[m_idim] -= (m_side == 1 ? 1 : -1) * m_grid.m_nbCells[m_idim];
        const Int j0 = m_grid.coordToIndex(coords0);
        Super::copy(m_u, m_grid.coordToIndex(coords), j0);
    }

    RealArray<dim> m_u;
    UniformGrid<dim> m_grid;
    int m_idim;
    int m_side;
    StridedFillingCurve<dim> m_boundary_curve;
};

} // namespace ark
