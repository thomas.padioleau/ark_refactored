#pragma once

#include "FillingCurve.hpp"
#include "Params.hpp"
#include "Types.hpp"

namespace ark
{

template <dim_t dim>
void ExecuteCopyToBuffer(
        const RealArray<dim>& u,
        const RealArray<dim>& buffer,
        const StridedFillingCurve<dim>& curve,
        int idim,
        int side);

#define ETI_ExecuteCopyToBuffer(dim)                                                               \
    void ExecuteCopyToBuffer(                                                                      \
            const RealArray<dim>& u,                                                               \
            const RealArray<dim>& buffer,                                                          \
            const StridedFillingCurve<dim>& curve,                                                 \
            int idim,                                                                              \
            int side)

extern template ETI_ExecuteCopyToBuffer(one_d);
extern template ETI_ExecuteCopyToBuffer(two_d);
extern template ETI_ExecuteCopyToBuffer(three_d);

} // namespace ark
