#include "CopyFromBufferExecution.hpp"
#include "CopyFromBufferKernel.hpp"
#include "FillingCurve.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
void ExecuteCopyFromBuffer(
        const RealArray<dim>& u,
        const RealArray<dim>& buffer,
        const StridedFillingCurve<dim>& curve,
        int idim,
        int side)
{
    CopyFromBufferKernel<dim> kernel(u, buffer, curve, idim, side);
    Kokkos::RangePolicy<Int> range(0, kernel.m_boundary_curve.nbCells());
    Kokkos::parallel_for("Copy from buffer kernel - RangePolicy", range, kernel);
}

template ETI_ExecuteCopyFromBuffer(one_d);
template ETI_ExecuteCopyFromBuffer(two_d);
template ETI_ExecuteCopyFromBuffer(three_d);

} // namespace ark
