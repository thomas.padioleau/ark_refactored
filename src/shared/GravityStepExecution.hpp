#pragma once

#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
class IStaticGravityOperator
{
public:
    virtual ~IStaticGravityOperator() = default;

    virtual void execute(
            Params<dim> const& params,
            UniformGrid<dim> const& grid,
            RealArray<dim> const& u,
            RealArray<dim> const& q,
            Real dt) const = 0;
};

template <dim_t dim>
class NullGravityOperator : public IStaticGravityOperator<dim>
{
public:
    void execute(
            Params<dim> const&,
            UniformGrid<dim> const&,
            RealArray<dim> const&,
            RealArray<dim> const&,
            Real) const override
    {
    }
};

template <dim_t dim>
class GravityOperator : public IStaticGravityOperator<dim>
{
public:
    void execute(
            Params<dim> const& params,
            UniformGrid<dim> const& grid,
            RealArray<dim> const& u,
            RealArray<dim> const& q,
            Real dt) const override;
};

extern template class GravityOperator<one_d>;
extern template class GravityOperator<two_d>;
extern template class GravityOperator<three_d>;

} // namespace ark
