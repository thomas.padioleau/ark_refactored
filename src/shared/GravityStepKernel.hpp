#pragma once

#include <Kokkos_Core.hpp>

#include "BaseKernel.hpp"
#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "Problem.hpp"
#include "Solver.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
class GravityStepKernel : public BaseKernel<dim>
{
    using Super = BaseKernel<dim>;

    using Euler = EulerSystem<dim>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;

public:
    using TeamPolicy = Kokkos::TeamPolicy<Kokkos::IndexType<Int>>;
    using Team = typename TeamPolicy::member_type;

    static constexpr int ghostDepth = 0;

    GravityStepKernel(
            const Params<dim>& params,
            const UniformGrid<dim>& grid,
            const RealArray<dim>& u,
            const RealArray<dim>& q,
            Real dt)
        : m_u(u)
        , m_q(q)
        , m_g(params.hydro.g)
        , m_grid(grid)
        , m_dt(dt)
        , m_x_beg(grid.m_ghostWidths[IX] - ghostDepth)
        , m_x_end(grid.m_ghostWidths[IX] + grid.m_nbCells[IX] + ghostDepth)
    {
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const Team& team) const
    {
        // From given team compute linear index of coordinates of cell (0, jy, jz).
        const Int j0 = Super::teamToLinearIndex(team, m_grid, ghostDepth);

        Kokkos::parallel_for(ark::TeamVectorRange(team, m_x_beg, m_x_end), [&](const Int jx) {
            const Int j = j0 + jx;
            ConsState u_j = Super::getCons(m_u, j);
            const PrimState q_j = Super::getPrim(m_q, j);
            for (int idim = 0; idim < dim; ++idim)
            {
                const Real dt_rho_g = m_dt * q_j.d * m_g[idim];
                u_j.m(idim) += dt_rho_g;
                u_j.e += dt_rho_g * q_j.v(idim);
            }
            Super::set(m_u, j, u_j);
        });
    }

    RealArray<dim> m_u;
    ConstRealArray<dim> m_q;
    RealVector<dim> m_g;
    UniformGrid<dim> m_grid;
    Real m_dt;
    Int m_x_beg;
    Int m_x_end;
};

} // namespace ark
