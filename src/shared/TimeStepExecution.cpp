#include <cmath>

#include <Kokkos_Core.hpp>

#include "BaseKernel.hpp"
#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "TimeStep.hpp"
#include "TimeStepExecution.hpp"
#include "TimeStepKernel.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
TimeStep ExecuteTimeStep(
        const Params<dim>& params,
        const UniformGrid<dim>& grid,
        const RealArray<dim>& q)
{
    TimeStepKernel<dim> kernel(params, grid, q);
    TimeStep invDt;
    Kokkos::RangePolicy<Int> range(0, grid.nbCells());
    Kokkos::parallel_reduce("Time step kernel - RangePolicy", range, kernel, invDt);
    TimeStep dt = TimeStep(std::numeric_limits<Real>::infinity());
    if (params.hydro.hydro_enabled)
    {
        dt.hydro = params.run.cfl / invDt.hydro;
    }
    if (params.hydro.thermal_diffusion_enabled)
    {
        dt.thermal = params.run.cfl / invDt.thermal;
    }
    if (params.hydro.viscosity_enabled)
    {
        dt.viscous = params.run.cfl / invDt.viscous;
    }
#if defined(MPI_SESSION)
    // AllReduce on dt using local copy "dt_loc"
    TimeStep dt_loc = dt;
    const int type = grid.comm.template dataType<Real>();
    if (params.hydro.hydro_enabled)
    {
        grid.comm.allReduce(&dt_loc.hydro, &dt.hydro, 1, type, grid.comm.MIN);
    }
    if (params.hydro.thermal_diffusion_enabled)
    {
        grid.comm.allReduce(&dt_loc.thermal, &dt.thermal, 1, type, grid.comm.MIN);
    }
    if (params.hydro.viscosity_enabled)
    {
        grid.comm.allReduce(&dt_loc.viscous, &dt.viscous, 1, type, grid.comm.MIN);
    }
#endif
    return dt;
}

template ETI_ExecuteTimeStep(one_d);
template ETI_ExecuteTimeStep(two_d);
template ETI_ExecuteTimeStep(three_d);

} // namespace ark
