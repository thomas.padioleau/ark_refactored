#include "BaseKernel.hpp"
#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "GravityStepExecution.hpp"
#include "GravityStepKernel.hpp"
#include "Problem.hpp"
#include "Solver.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
void GravityOperator<dim>::execute(
        Params<dim> const& params,
        UniformGrid<dim> const& grid,
        RealArray<dim> const& u,
        RealArray<dim> const& q,
        Real dt) const
{
    using Kernel = GravityStepKernel<dim>;
    using TeamPolicy = typename Kernel::TeamPolicy;
    GravityStepKernel<dim> kernel(params, grid, u, q, dt);
    const int league_size = Kernel::computeLeagueSize(grid.m_nbCells, Kernel::ghostDepth);
    const int vector_length = TeamPolicy::vector_length_max();
    TeamPolicy policy(league_size, Kokkos::AUTO, vector_length);
    Kokkos::parallel_for("Gravity kernel - TeamPolicy", policy, kernel);
}

template class GravityOperator<one_d>;
template class GravityOperator<two_d>;
template class GravityOperator<three_d>;

} // namespace ark
