#pragma once

#include "FillingCurve.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
class CopyToBufferKernel
{
public:
    CopyToBufferKernel(
            const RealArray<dim>& u,
            const RealArray<dim>& buffer,
            const StridedFillingCurve<dim>& curve,
            int idim,
            int side)
        : m_u(u)
        , m_buffer(buffer)
        , m_nvar(0)
        , m_curve(curve)
        , m_idim(idim)
        , m_side(side)
        , m_boundary_curve()
    {
        if (m_buffer.extent(1) != m_u.extent(1))
        {
            throw std::runtime_error("Arrays don't have same size.\n");
        }
        else
        {
            m_nvar = static_cast<int>(m_buffer.extent(1));
        }

        IntVector<dim> nbCells;
        for (int idim2 = 0; idim2 < dim; ++idim2)
        {
            nbCells[idim2] = idim2 == m_idim
                                     ? m_curve.m_ghostWidths[idim2]
                                     : m_curve.m_nbCells[idim2] + 2 * m_curve.m_ghostWidths[idim2];
        }
        m_boundary_curve = StridedFillingCurve<dim>(nbCells, 0);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(Int j) const
    {
        IntVector<dim> coords = m_boundary_curve.indexToCoord(j);
        coords[m_idim] += (m_side == 1 ? m_curve.m_nbCells[m_idim] : m_curve.m_ghostWidths[m_idim]);
        const Int j0 = m_curve.coordToIndex(coords);
        for (int ivar = 0; ivar < m_nvar; ++ivar)
        {
            m_buffer(j, ivar) = m_u(j0, ivar);
        }
    }

    ConstRealArray<dim> m_u;
    RealArray<dim> m_buffer;
    int m_nvar;
    StridedFillingCurve<dim> m_curve;
    int m_idim;
    int m_side;
    StridedFillingCurve<dim> m_boundary_curve;
};

} // namespace ark
