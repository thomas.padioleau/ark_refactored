#include <cmath>

#include <Kokkos_Core.hpp>

#include "BaseKernel.hpp"
#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "PrimitiveToConservativeExecution.hpp"
#include "PrimitiveToConservativeKernel.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
void PrimitiveToConservativeOperator<dim>::execute(
        const Params<dim>& params,
        const UniformGrid<dim>& grid,
        const RealArray<dim>& q,
        const RealArray<dim>& u) const
{
    PrimitiveToConservativeKernel<dim> kernel(params, grid, q, u);
    Kokkos::RangePolicy<Int> range(0, grid.nbCells());
    Kokkos::parallel_for("P2C kernel", range, kernel);
}

template class PrimitiveToConservativeOperator<one_d>;
template class PrimitiveToConservativeOperator<two_d>;
template class PrimitiveToConservativeOperator<three_d>;

} // namespace ark
