#pragma once

#include <cmath>
#include <string>

#include "Types.hpp"

namespace ark
{

class TimeStep
{
public:
    TimeStep() noexcept = default;

    explicit TimeStep(Real value) noexcept : hydro(value), thermal(value), viscous(value) {}

    Real hydro;
    Real thermal;
    Real viscous;

    Real min() const
    {
        Real dt = hydro;
        dt = std::fmin(dt, thermal);
        dt = std::fmin(dt, viscous);
        return dt;
    }

    std::string cfl() const
    {
        std::string name {"hydro"};
        Real dt = hydro;
        if (thermal < dt)
        {
            name = std::string("thermal");
            dt = thermal;
        }
        if (viscous < dt)
        {
            name = std::string("viscous");
            dt = viscous;
        }
        return name;
    }
};

} // namespace ark
