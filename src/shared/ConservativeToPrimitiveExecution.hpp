#pragma once

#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
class IConservativeToPrimitiveOperator
{
public:
    virtual ~IConservativeToPrimitiveOperator() = default;

    virtual void execute(
            Params<dim> const& params,
            UniformGrid<dim> const& grid,
            RealArray<dim> const& u,
            RealArray<dim> const& q) const = 0;
};

template <dim_t dim>
class ConservativeToPrimitiveOperator : public IConservativeToPrimitiveOperator<dim>
{
public:
    void execute(
            Params<dim> const& params,
            UniformGrid<dim> const& grid,
            RealArray<dim> const& u,
            RealArray<dim> const& q) const override;
};

extern template class ConservativeToPrimitiveOperator<one_d>;
extern template class ConservativeToPrimitiveOperator<two_d>;
extern template class ConservativeToPrimitiveOperator<three_d>;

} // namespace ark
