#pragma once

#include <cmath>

#include <Kokkos_Core.hpp>

#include "BaseKernel.hpp"
#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
class ConservativeToPrimitiveKernel : public BaseKernel<dim>
{
    using Super = BaseKernel<dim>;

    using Euler = EulerSystem<dim>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;

public:
    ConservativeToPrimitiveKernel(
            const Params<dim>& params,
            const UniformGrid<dim>& grid,
            const RealArray<dim>& u,
            const RealArray<dim>& q)
        : m_grid(grid)
        , m_q(q)
        , m_u(u)
        , m_eos(params.thermo)
    {
    }

    ConservativeToPrimitiveKernel(ConservativeToPrimitiveKernel const& x) = default;

    ConservativeToPrimitiveKernel(ConservativeToPrimitiveKernel&& x) = default;

    ~ConservativeToPrimitiveKernel() = default;

    ConservativeToPrimitiveKernel& operator=(ConservativeToPrimitiveKernel const& x) = default;

    ConservativeToPrimitiveKernel& operator=(ConservativeToPrimitiveKernel&& x) = default;

    KOKKOS_INLINE_FUNCTION
    void operator()(Int j) const
    {
        const ConsState u_j = Super::getCons(m_u, j);
        const PrimState q_j = Euler::conservativeToPrimitive(u_j, m_eos);
        Super::set(m_q, j, q_j);
    }

    UniformGrid<dim> m_grid;
    RealArray<dim> m_q;
    ConstRealArray<dim> m_u;
    EquationOfState m_eos;
};

} // namespace ark
