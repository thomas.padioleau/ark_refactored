#pragma once

#include <cmath>

#include <Kokkos_Core.hpp>

#include "linalg/Vector.hpp"

#include "BaseKernel.hpp"
#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
class ThermalDiffusionStepKernel : public BaseKernel<dim>
{
    using Super = BaseKernel<dim>;

    using Euler = EulerSystem<dim>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;
    using VC = typename Euler::VarCons;

public:
    using TeamPolicy = Kokkos::TeamPolicy<Kokkos::IndexType<Int>>;
    using Team = typename TeamPolicy::member_type;

    static constexpr int ghostDepth = 0;

    ThermalDiffusionStepKernel(
            const Params<dim>& params,
            const UniformGrid<dim>& grid,
            const RealArray<dim>& q,
            const RealArray<dim>& u,
            Real dt)
        : m_grid(grid)
        , m_q(q)
        , m_u(u)
        , m_eos(params.thermo)
        , m_kappa(params.hydro.kappa)
        , m_dt(dt)
        , m_x_beg(grid.m_ghostWidths[IX] - ghostDepth)
        , m_x_end(grid.m_ghostWidths[IX] + grid.m_nbCells[IX] + ghostDepth)
    {
    }

    template <int idim, int iside, std::enable_if_t<(idim >= dim), int> = idim>
    KOKKOS_INLINE_FUNCTION void computeThermalFlux(
            std::integral_constant<int, idim>,
            std::integral_constant<int, iside>,
            Int,
            Real&) const noexcept
    {
    }

    template <int idim, int iside, std::enable_if_t<(idim < dim), int> = idim>
    KOKKOS_INLINE_FUNCTION void computeThermalFlux(
            std::integral_constant<int, idim> idir_tag,
            std::integral_constant<int, iside> iside_tag,
            Int j,
            Real& flux) const noexcept
    {
        using namespace constants;

        const Int j_L = iside == 0 ? j - m_grid.m_strides[idim] : j;
        const Int j_R = iside == 0 ? j : j + m_grid.m_strides[idim];

        const PrimState q_L = Super::getPrim(m_q, j_L);
        const PrimState q_R = Super::getPrim(m_q, j_R);
        const Real T_L = Euler::computeTemperature(q_L, m_eos);
        const Real T_R = Euler::computeTemperature(q_R, m_eos);
        const Real gradT = (T_R - T_L) * m_grid.m_invdl[idim];

        const Real side = iside == 0 ? -one : +one;
        const Real dtdSdV = m_dt * m_grid.getdSdV(j, idir_tag, iside_tag);
        flux += -side * dtdSdV * m_kappa * gradT;
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const Team& team) const
    {
        using namespace constants;

        // From given team compute linear index of coordinates of cell (0, jy, jz).
        const Int j0 = Super::teamToLinearIndex(team, m_grid, ghostDepth);

        Kokkos::parallel_for(ark::TeamVectorRange(team, m_x_beg, m_x_end), [&](const Int jx) {
            const Int j = j0 + jx;

            Real flux_j = zero;

            computeThermalFlux(Tags::DirX, Tags::SideL, j, flux_j);
            computeThermalFlux(Tags::DirX, Tags::SideR, j, flux_j);
            computeThermalFlux(Tags::DirY, Tags::SideL, j, flux_j);
            computeThermalFlux(Tags::DirY, Tags::SideR, j, flux_j);
            computeThermalFlux(Tags::DirZ, Tags::SideL, j, flux_j);
            computeThermalFlux(Tags::DirZ, Tags::SideR, j, flux_j);

            m_u(j, VC::IE) -= flux_j;
        });
    }

private:
    UniformGrid<dim> m_grid;
    ConstRealArray<dim> m_q;
    RealArray<dim> m_u;
    EquationOfState m_eos;
    Real m_kappa;
    Real m_dt;
    Int m_x_beg;
    Int m_x_end;
};

} // namespace ark
