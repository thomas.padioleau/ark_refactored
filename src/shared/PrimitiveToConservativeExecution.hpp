#pragma once

#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
class IPrimitiveToConservativeOperator
{
public:
    virtual ~IPrimitiveToConservativeOperator() = default;

    virtual void execute(
            Params<dim> const& params,
            UniformGrid<dim> const& grid,
            RealArray<dim> const& q,
            RealArray<dim> const& u) const = 0;
};

template <dim_t dim>
class PrimitiveToConservativeOperator : public IPrimitiveToConservativeOperator<dim>
{
public:
    void execute(
            Params<dim> const& params,
            UniformGrid<dim> const& grid,
            RealArray<dim> const& q,
            RealArray<dim> const& u) const override;
};

extern template class PrimitiveToConservativeOperator<one_d>;
extern template class PrimitiveToConservativeOperator<two_d>;
extern template class PrimitiveToConservativeOperator<three_d>;

} // namespace ark
