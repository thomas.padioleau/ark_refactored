#pragma once

#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
class IThermalOperator
{
public:
    virtual ~IThermalOperator() = default;

    virtual void execute(
            Params<dim> const& params,
            UniformGrid<dim> const& grid,
            RealArray<dim> const& q,
            RealArray<dim> const& u,
            Real dt) const = 0;
};

template <dim_t dim>
class NullThermalOperator : public IThermalOperator<dim>
{
public:
    void execute(
            Params<dim> const&,
            UniformGrid<dim> const&,
            RealArray<dim> const&,
            RealArray<dim> const&,
            Real) const override
    {
    }
};

template <dim_t dim>
class ThermalOperator : public IThermalOperator<dim>
{
public:
    void execute(
            Params<dim> const& params,
            UniformGrid<dim> const& grid,
            RealArray<dim> const& q,
            RealArray<dim> const& u,
            Real dt) const override;
};

extern template class ThermalOperator<one_d>;
extern template class ThermalOperator<two_d>;
extern template class ThermalOperator<three_d>;

} // namespace ark
