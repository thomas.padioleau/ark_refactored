#pragma once

#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
class IViscousOperator
{
public:
    virtual ~IViscousOperator() = default;

    virtual void execute(
            Params<dim> const& params,
            UniformGrid<dim> const& grid,
            RealArray<dim> const& q,
            RealArray<dim> const& u,
            Real dt) const = 0;
};

template <dim_t dim>
class NullViscousOperator : public IViscousOperator<dim>
{
public:
    void execute(
            Params<dim> const&,
            UniformGrid<dim> const&,
            RealArray<dim> const&,
            RealArray<dim> const&,
            Real) const override
    {
    }
};

template <dim_t dim>
class ViscousOperator : public IViscousOperator<dim>
{
public:
    void execute(
            Params<dim> const& params,
            UniformGrid<dim> const& grid,
            RealArray<dim> const& q,
            RealArray<dim> const& u,
            Real dt) const override;
};

extern template class ViscousOperator<one_d>;
extern template class ViscousOperator<two_d>;
extern template class ViscousOperator<three_d>;

} // namespace ark
