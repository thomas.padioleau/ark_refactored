#pragma once

#include <cmath>

#include <Kokkos_Core.hpp>

#include "linalg/Tensor.hpp"
#include "linalg/Vector.hpp"

#include "BaseKernel.hpp"
#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <int dim, int IN>
struct getTransverse
{
    static constexpr int IT1 = (IN + 1) % dim;
    static constexpr int IT2 = (IN + 2) % dim;
};

template <dim_t dim>
class ViscousStepKernel : public BaseKernel<dim>
{
    using Super = BaseKernel<dim>;

    using Euler = EulerSystem<dim>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;

public:
    using TeamPolicy = Kokkos::TeamPolicy<Kokkos::IndexType<Int>>;
    using Team = typename TeamPolicy::member_type;

    static constexpr int ghostDepth = 0;

    using RealVector = Vector<dim, Real>;
    using RealTensor = Tensor<dim, Real>;

    ViscousStepKernel(
            const Params<dim>& params,
            const UniformGrid<dim>& grid,
            const RealArray<dim>& q,
            const RealArray<dim>& u,
            Real dt)
        : m_grid(grid)
        , m_q(q)
        , m_u(u)
        , m_mu(params.hydro.mu)
        , m_eta(-constants::two * constants::third * m_mu)
        , m_dt(dt)
        , m_x_beg(grid.m_ghostWidths[IX] - ghostDepth)
        , m_x_end(grid.m_ghostWidths[IX] + grid.m_nbCells[IX] + ghostDepth)
    {
    }

    template <int idir>
    KOKKOS_FORCEINLINE_FUNCTION RealTensor computeGradVel(
            Tags::Dim1_t,
            std::integral_constant<int, idir> idir_tag,
            Int,
            Int,
            const RealVector& velL,
            const RealVector& velR) const noexcept
    {
        using namespace constants;

        RealTensor gradVel;

        // Normal gradient
        gradVel(idir_tag, Tags::DirX)
                = (velR(Tags::DirX) - velL(Tags::DirX)) * m_grid.m_invdl[idir];

        return gradVel;
    }

    template <int idir>
    KOKKOS_FORCEINLINE_FUNCTION RealTensor computeGradVel(
            Tags::Dim2_t,
            std::integral_constant<int, idir> idir_tag,
            Int j_L,
            Int j_R,
            const RealVector& velL,
            const RealVector& velR) const noexcept
    {
        using namespace constants;

        RealTensor gradVel;

        // Normal gradient
        gradVel(idir_tag, Tags::DirX)
                = (velR(Tags::DirX) - velL(Tags::DirX)) * m_grid.m_invdl[idir];
        gradVel(idir_tag, Tags::DirY)
                = (velR(Tags::DirY) - velL(Tags::DirY)) * m_grid.m_invdl[idir];

        {
            constexpr int it = getTransverse<dim, idir>::IT1;
            std::integral_constant<int, it> it_tag;
            const RealVector velLL = Super::getVelocity(m_q, j_L - m_grid.m_strides[it]);
            const RealVector velRL = Super::getVelocity(m_q, j_R - m_grid.m_strides[it]);
            const RealVector velLR = Super::getVelocity(m_q, j_L + m_grid.m_strides[it]);
            const RealVector velRR = Super::getVelocity(m_q, j_R + m_grid.m_strides[it]);

            const RealVector meanVelL = mean(velLL, velRL);
            const RealVector meanVelR = mean(velLR, velRR);

            gradVel(it_tag, Tags::DirX)
                    = half * (meanVelR(Tags::DirX) - meanVelL(Tags::DirX)) * m_grid.m_invdl[it];
            gradVel(it_tag, Tags::DirY)
                    = half * (meanVelR(Tags::DirY) - meanVelL(Tags::DirY)) * m_grid.m_invdl[it];
        }

        return gradVel;
    }

    template <int idir>
    KOKKOS_FORCEINLINE_FUNCTION RealTensor computeGradVel(
            Tags::Dim3_t,
            std::integral_constant<int, idir> idir_tag,
            const Int j_L,
            const Int j_R,
            const RealVector& velL,
            const RealVector& velR) const noexcept
    {
        using namespace constants;

        RealTensor gradVel;

        // Normal gradient
        gradVel(idir_tag, Tags::DirX)
                = (velR(Tags::DirX) - velL(Tags::DirX)) * m_grid.m_invdl[idir];
        gradVel(idir_tag, Tags::DirY)
                = (velR(Tags::DirY) - velL(Tags::DirY)) * m_grid.m_invdl[idir];
        gradVel(idir_tag, Tags::DirZ)
                = (velR(Tags::DirZ) - velL(Tags::DirZ)) * m_grid.m_invdl[idir];

        {
            constexpr int it = getTransverse<dim, idir>::IT1;
            std::integral_constant<int, it> it_tag;
            const RealVector velLL = Super::getVelocity(m_q, j_L - m_grid.m_strides[it]);
            const RealVector velRL = Super::getVelocity(m_q, j_R - m_grid.m_strides[it]);
            const RealVector velLR = Super::getVelocity(m_q, j_L + m_grid.m_strides[it]);
            const RealVector velRR = Super::getVelocity(m_q, j_R + m_grid.m_strides[it]);

            const RealVector meanVelL = mean(velLL, velRL);
            const RealVector meanVelR = mean(velLR, velRR);

            gradVel(it_tag, Tags::DirX)
                    = half * (meanVelR(Tags::DirX) - meanVelL(Tags::DirX)) * m_grid.m_invdl[it];
            gradVel(it_tag, Tags::DirY)
                    = half * (meanVelR(Tags::DirY) - meanVelL(Tags::DirY)) * m_grid.m_invdl[it];
            gradVel(it_tag, Tags::DirZ)
                    = half * (meanVelR(Tags::DirZ) - meanVelL(Tags::DirZ)) * m_grid.m_invdl[it];
        }

        {
            constexpr int it = getTransverse<dim, idir>::IT2;
            std::integral_constant<int, it> it_tag;
            const RealVector velLL = Super::getVelocity(m_q, j_L - m_grid.m_strides[it]);
            const RealVector velRL = Super::getVelocity(m_q, j_R - m_grid.m_strides[it]);
            const RealVector velLR = Super::getVelocity(m_q, j_L + m_grid.m_strides[it]);
            const RealVector velRR = Super::getVelocity(m_q, j_R + m_grid.m_strides[it]);

            const RealVector meanVelL = mean(velLL, velRL);
            const RealVector meanVelR = mean(velLR, velRR);

            gradVel(it_tag, Tags::DirX)
                    = half * (meanVelR(Tags::DirX) - meanVelL(Tags::DirX)) * m_grid.m_invdl[it];
            gradVel(it_tag, Tags::DirY)
                    = half * (meanVelR(Tags::DirY) - meanVelL(Tags::DirY)) * m_grid.m_invdl[it];
            gradVel(it_tag, Tags::DirZ)
                    = half * (meanVelR(Tags::DirZ) - meanVelL(Tags::DirZ)) * m_grid.m_invdl[it];
        }

        return gradVel;
    }

    template <int idir, int iside, std::enable_if_t<(idir >= dim), int> = idir>
    KOKKOS_FORCEINLINE_FUNCTION void updateAlongFace(
            std::integral_constant<int, idir>,
            std::integral_constant<int, iside>,
            const Int,
            ConsState&) const noexcept
    {
    }

    template <int idir, int iside, std::enable_if_t<(idir < dim), int> = idir>
    KOKKOS_FORCEINLINE_FUNCTION void updateAlongFace(
            std::integral_constant<int, idir> idir_tag,
            std::integral_constant<int, iside> iside_tag,
            const Int j,
            ConsState& u_j) const noexcept
    {
        using namespace constants;

        constexpr Tags::Tag_t<dim> dim_tag;

        const Int j_L = iside == 0 ? j - m_grid.m_strides[idir] : j;
        const Int j_R = iside == 0 ? j : j + m_grid.m_strides[idir];

        const RealVector velL = Super::getVelocity(m_q, j_L);
        const RealVector velR = Super::getVelocity(m_q, j_R);

        // Compute velocity gradient at face (idir, iside)
        // gradVel_{i, j} = d(Vel_i) / dx_j
        const RealTensor gradVel = computeGradVel(dim_tag, idir_tag, j_L, j_R, velL, velR);

        // Compute divergence by summing diagonal elements
        // div(Vel) = Sum_j d(Vel_j) / dx_j
        const Real divu = trace(gradVel);

        // Compute normal to face (idir, iside) projection of symetric part of gradVel
        // tau_i = 2*mu*Sym(gradVel)_{i, idir} + kronecker_{i, idir}*eta*div(Vel)
        RealVector tau = sym(idir_tag, gradVel);
        tau *= two * m_mu;
        tau(idir_tag) += m_eta * divu;

        const RealVector meanVel = mean(velL, velR);
        const Real tau_dot_u = dot(meanVel, tau);

        const Real side = iside == 0 ? -one : +one;
        const Real dtdSdV = side * m_dt * m_grid.getdSdV(j, idir_tag, iside_tag);
        u_j.m += dtdSdV * tau;
        u_j.e += dtdSdV * tau_dot_u;
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const Team& team) const
    {
        // From given team compute linear index of coordinates of cell (0, jy, jz).
        const Int j0 = Super::teamToLinearIndex(team, m_grid, ghostDepth);

        Kokkos::parallel_for(ark::TeamVectorRange(team, m_x_beg, m_x_end), [&](const Int jx) {
            const Int j = j0 + jx;

            ConsState u_j = Super::getCons(m_u, j);

            updateAlongFace(Tags::DirX, Tags::SideL, j, u_j);
            updateAlongFace(Tags::DirX, Tags::SideR, j, u_j);
            updateAlongFace(Tags::DirY, Tags::SideL, j, u_j);
            updateAlongFace(Tags::DirY, Tags::SideR, j, u_j);
            updateAlongFace(Tags::DirZ, Tags::SideL, j, u_j);
            updateAlongFace(Tags::DirZ, Tags::SideR, j, u_j);

            Super::set(m_u, j, u_j);
        });
    }

private:
    UniformGrid<dim> m_grid;
    ConstRealArray<dim> m_q;
    RealArray<dim> m_u;
    Real m_mu;
    Real m_eta;
    Real m_dt;
    Int m_x_beg;
    Int m_x_end;
};

} // namespace ark
