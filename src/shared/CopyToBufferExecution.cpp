#include "CopyToBufferExecution.hpp"
#include "CopyToBufferKernel.hpp"
#include "FillingCurve.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
void ExecuteCopyToBuffer(
        const RealArray<dim>& u,
        const RealArray<dim>& buffer,
        const StridedFillingCurve<dim>& curve,
        int idim,
        int side)
{
    CopyToBufferKernel<dim> kernel(u, buffer, curve, idim, side);
    Kokkos::RangePolicy<Int> range(0, kernel.m_boundary_curve.nbCells());
    Kokkos::parallel_for("Copy to buffer kernel - RangePolicy", range, kernel);
}

template ETI_ExecuteCopyToBuffer(one_d);
template ETI_ExecuteCopyToBuffer(two_d);
template ETI_ExecuteCopyToBuffer(three_d);

} // namespace ark
