#pragma once

#include <string>

#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
void ExecuteBoundaries(
        const RealArray<dim>& u,
        const UniformGrid<dim>& grid,
        int idim,
        int side,
        std::string const& boundaryType);

#define ETI_ExecuteBoundaries(dim)                                                                 \
    void ExecuteBoundaries(                                                                        \
            const RealArray<dim>& u,                                                               \
            const UniformGrid<dim>& grid,                                                          \
            int idim,                                                                              \
            int side,                                                                              \
            std::string const& boundaryType)

extern template ETI_ExecuteBoundaries(one_d);
extern template ETI_ExecuteBoundaries(two_d);
extern template ETI_ExecuteBoundaries(three_d);

} // namespace ark
