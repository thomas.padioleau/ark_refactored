#pragma once

#include <iosfwd>
#include <memory>
#include <string>

#include "Params.hpp"
#include "Problem.hpp"
#include "Solver.hpp"
#include "Types.hpp"

namespace ark
{

class Engine
{
public:
    Engine() = default;

    Engine(const Engine& x) = default;

    Engine(Engine&& x) = default;

    virtual ~Engine() = default;

    Engine& operator=(const Engine& x) = default;

    Engine& operator=(Engine&& x) = default;

    virtual void print_configuration(std::ostream& os) const = 0;

    virtual void run() const = 0;
};

template <dim_t dim>
struct EngineNd : Engine
{
    explicit EngineNd(const std::string& filename);

    void print_configuration(std::ostream& os) const final;

    void run() const final;

    std::shared_ptr<Params<dim>> params;
    std::shared_ptr<Problem<dim>> problem;
    std::shared_ptr<Solver> solver;
};

struct EngineFactory
{
    static std::shared_ptr<Engine> New(const std::string& filename);
};

} // namespace ark
