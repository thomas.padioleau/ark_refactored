#include "Constants.hpp"
#include "PerfectGas.hpp"
#include "ThermoParams.hpp"
#include "Types.hpp"
#include "Units.hpp"

namespace ark::thermodynamics
{

PerfectGas::PerfectGas(Real const gamma, Real const mmw, Real const e0) noexcept
    : m_gamma(gamma)
    , m_gamma_m1(gamma - constants::one)
    , m_inv_gamma_m1(constants::one / (gamma - constants::one))
    , m_mmw(mmw)
    , m_Rstar(code_units::constants::Rstar_h / mmw)
    , m_e0(e0)
{
}

PerfectGas::PerfectGas(const ThermoParams& thermoParams)
    : PerfectGas(thermoParams.gamma, thermoParams.mmw, thermoParams.e0)
{
}

} // namespace ark::thermodynamics
