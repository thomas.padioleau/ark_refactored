#pragma once

#include <string>

#include "inih/INIReader.hpp"

#include "Constants.hpp"
#include "Types.hpp"

namespace ark::thermodynamics
{

class ThermoParams
{
public:
    ThermoParams() = default;

    explicit ThermoParams(const INIReader& reader);

    ThermoParams(const ThermoParams& x) = default;

    ThermoParams(ThermoParams&& x) = default;

    ~ThermoParams() = default;

    ThermoParams& operator=(const ThermoParams& x) = default;

    ThermoParams& operator=(ThermoParams&& x) = default;

    std::string type = "perfect gas";
    Real gamma = constants::five_third;
    Real mmw = constants::one;
    Real p0 = constants::zero;
    Real e0 = constants::zero;
    std::string section = "thermo";
};

} // namespace ark::thermodynamics
