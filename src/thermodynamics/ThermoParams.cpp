#include <string>

#include "inih/INIReader.hpp"

#include "Constants.hpp"
#include "ThermoParams.hpp"
#include "Types.hpp"

namespace ark::thermodynamics
{

ThermoParams::ThermoParams(const INIReader& reader)
{
    type = reader.Get(section, "type", type);
    gamma = reader.GetReal(section, "gamma", gamma);
    mmw = reader.GetReal(section, "mmw", mmw);
    e0 = reader.GetReal(section, "e0", e0);
    p0 = reader.GetReal(section, "p0", p0);
}

} // namespace ark::thermodynamics
