#include "Constants.hpp"
#include "StiffenedGas.hpp"
#include "ThermoParams.hpp"
#include "Types.hpp"
#include "Units.hpp"

namespace ark::thermodynamics
{

StiffenedGas::StiffenedGas(Real const gamma, Real const mmw, Real const p0, Real const e0) noexcept
    : m_gamma(gamma)
    , m_gamma_m1(gamma - constants::one)
    , m_inv_gamma_m1(constants::one / (gamma - constants::one))
    , m_mmw(mmw)
    , m_Rstar(code_units::constants::Rstar_h / mmw)
    , m_p0(p0)
    , m_e0(e0)
{
}

StiffenedGas::StiffenedGas(const ThermoParams& thermoParams)
    : StiffenedGas(thermoParams.gamma, thermoParams.mmw, thermoParams.p0, thermoParams.e0)
{
}

} // namespace ark::thermodynamics
