#include <chrono>
#include <cstring>
#include <exception>
#include <iomanip>
#include <iostream>
#include <limits>
#include <memory>
#include <string>

#include "DistributedMemorySession.hpp"
#include "Engine.hpp"
#include "Params.hpp"
#include "Print.hpp"
#include "ProblemFactory.hpp"
#include "Solver.hpp"
#include "SolverFactory.hpp"
#include "Types.hpp"

namespace ark
{

template <dim_t dim>
EngineNd<dim>::EngineNd(const std::string& filename)
    : Engine()
    , params(std::make_shared<Params<dim>>(filename))
    , problem(problems::ProblemFactory<dim>::New(params->problem, params))
    , solver(SolverFactory<dim>::New(params->run.solver, problem))
{
}

template <dim_t dim>
void EngineNd<dim>::print_configuration(std::ostream& os) const
{
    const double memory_gb = solver->memoryUsage() * 1e-9;
    params->print(os);
    os << std::left << std::setw(40) << std::setfill('.') << "memory requested [in GB]"
       << std::right << std::setw(40) << std::setfill('.') << memory_gb << '\n';
    os << "Starting simulation from ";
    os << std::setprecision(std::numeric_limits<Real>::digits10);
    os << std::scientific;
    os << "step n=";
    os << std::setw(std::numeric_limits<int>::digits10) << std::setfill('.') << solver->iteration();
    os << "; time t=" << solver->time() << "\n";
}

template <dim_t dim>
void EngineNd<dim>::run() const
{
    const auto finished = [&]() -> bool {
        return (solver->time() >= params->run.tEnd || solver->iteration() >= params->run.nStepmax);
    };

    const auto next_checkpoint = [&](Real& dt) -> bool {
        bool should_save = false;
        if (params->output.nOutput <= 0 && params->output.dt_io <= 0.)
        {
            return should_save;
        }

        if (const Real dt_io = params->output.dt_io; dt_io > constants::zero)
        {
            const Real dt_old = dt;
            // Next physical time to do output
            const Real t_io = (std::floor(solver->time() / dt_io) + constants::one) * dt_io;
            if (solver->time() + dt >= t_io)
            {
                // Make sure dt is big enough such that next t_io is different than m_t+dt
                dt = utils::adjust(solver->time(), t_io, [dt_io](Real v1, Real v2, Real delta) {
                    return (delta + v1 < v2)
                           || ((std::floor((v1 + delta) / dt_io) + constants::one) * dt_io) <= v2;
                });
                if (dt_old < dt)
                {
                    throw std::runtime_error(
                            "Time step is increasing whereas it should decrease.\n");
                }
                should_save = true;
            }
        }

        if (solver->time() + dt >= params->run.tEnd)
        {
            should_save = true;
        }

        if ((params->output.nOutput > 0) && (solver->iteration() % params->output.nOutput == 0))
        {
            should_save = true;
        }

        return should_save;
    };

    const auto shouldPrintInformation = [&](Int iteration) -> bool {
        if (params->run.info == 0)
        {
            return false;
        }

        if (finished())
        {
            return true;
        }

        return iteration % params->run.info == 0;
    };

    const auto printInformation = [&](double dt) {
        Print oss;
        oss << std::setprecision(std::numeric_limits<Real>::digits10);
        oss << std::scientific;
        oss << "Step n=";
        oss << std::setw(std::numeric_limits<int>::digits10) << std::setfill('.')
            << solver->iteration();
        oss << "; time t=" << solver->time();
        oss << " [" << std::setw(5) << std::setfill(' ') << std::setprecision(1) << std::fixed
            << 100.0 * static_cast<double>(solver->time() / params->run.tEnd) << "%]\n";
        oss << std::setprecision(std::numeric_limits<Real>::digits10);
        oss << std::scientific;
        oss << " * Time step dt=" << dt << std::endl;
    };

    if ((params->output.nOutput > 0 || params->output.dt_io > 0.) && !params->run.restart)
    {
        Kokkos::Profiling::pushRegion("I/O");
        solver->saveOutput();
        Kokkos::Profiling::popRegion();
    }

    const std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
    while (!finished())
    {
        Kokkos::Profiling::pushRegion("Time step");
        Real dt = solver->computeTimeStep();
        if (solver->time() + dt > params->run.tEnd)
        {
            dt = utils::adjust(solver->time(), params->run.tEnd);
        }
        Kokkos::Profiling::popRegion();

        bool const should_save = next_checkpoint(dt);

        Kokkos::Profiling::pushRegion("Solver");
        solver->nextIteration(dt);
        Kokkos::Profiling::popRegion();

        if (shouldPrintInformation(solver->iteration()))
        {
            printInformation(dt);
        }

        if (should_save)
        {
            Kokkos::Profiling::pushRegion("I/O");
            solver->saveOutput();
            Kokkos::Profiling::popRegion();
        }
    }
    const std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

    double nbcells_treated = 1;
    nbcells_treated *= solver->iteration();
    for (std::size_t i = 0; i < params->mesh.nbCells.size(); ++i)
    {
        nbcells_treated *= params->mesh.nbCells[i];
    }
    nbcells_treated *= Session::getNProc();
    const double elapsed_time = std::chrono::duration<double>(end - start).count();
    const double w_perf = nbcells_treated / elapsed_time * 1.0E-6;
    Print() << "Perf (Wall clock): " << w_perf << " Mcell-updates/s\n";
}

std::shared_ptr<Engine> EngineFactory::New(const std::string& filename)
{
    const INIReader reader(filename);
    if (reader.ParseError() < 0)
    {
        throw std::runtime_error(
                "Error opening file \"" + filename + "\": " + std::strerror(errno));
    }

    std::shared_ptr<Engine> engine;
    const int dim = reader.GetInteger("problem", "dimension", 0);
    if (dim == one_d)
    {
        engine = std::make_shared<EngineNd<one_d>>(filename);
    }
    else if (dim == two_d)
    {
        engine = std::make_shared<EngineNd<two_d>>(filename);
    }
    else if (dim == three_d)
    {
        engine = std::make_shared<EngineNd<three_d>>(filename);
    }
    else
    {
        throw std::runtime_error("Invalid dimension. Dimensions allowed: 1, 2, 3.");
    }

    return engine;
}

} // namespace ark
