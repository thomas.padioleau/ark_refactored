#pragma once

#include "IEulerOperator.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
class SplitAllRegimeOperator : public IEulerOperator<dim>
{
    RealArray<dim> m_uAc;

public:
    SplitAllRegimeOperator(UniformGrid<dim> const& grid) : m_uAc("Uac", grid.nbCells()) {}

    void execute(
            Params<dim> const& params,
            UniformGrid<dim> const& grid,
            RealArray<dim> const& u,
            RealArray<dim> const& q,
            Real dt) const override;
};

extern template class SplitAllRegimeOperator<one_d>;
extern template class SplitAllRegimeOperator<two_d>;
extern template class SplitAllRegimeOperator<three_d>;

} // namespace ark
