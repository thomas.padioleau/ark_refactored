#pragma once

#include <cmath>

#include <Kokkos_Core.hpp>

#include "BaseKernel.hpp"
#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
class TransportStepKernel : public BaseKernel<dim>
{
    using Super = BaseKernel<dim>;

    using Euler = EulerSystem<dim>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;

public:
    using TeamPolicy = Kokkos::TeamPolicy<Kokkos::IndexType<Int>>;
    using Team = typename TeamPolicy::member_type;

    static constexpr int ghostDepth = 0;

    TransportStepKernel(
            const Params<dim>& params,
            const UniformGrid<dim>& grid,
            const RealArray<dim>& u,
            const RealArray<dim>& uAc,
            const RealArray<dim>& q,
            Real dt)
        : m_K(params.hydro.K)
        , m_eos(params.thermo)
        , m_grid(grid)
        , m_u(u)
        , m_uAc(uAc)
        , m_q(q)
        , m_dt(dt)
        , m_dphi {}
        , m_x_beg(grid.m_ghostWidths[IX] - ghostDepth)
        , m_x_end(grid.m_ghostWidths[IX] + grid.m_nbCells[IX] + ghostDepth)
    {
        for (int idir = 0; idir < dim; ++idir)
        {
            m_dphi(idir) = -params.hydro.g[idir] * grid.dl(idir);
        }
    }

    template <int idir, int iside, std::enable_if_t<(idir >= dim), int> = idir>
    KOKKOS_INLINE_FUNCTION void updateAlongFace(
            std::integral_constant<int, idir>,
            std::integral_constant<int, iside>,
            const Int,
            const ConsState&,
            ConsState&) const noexcept
    {
    }

    template <int idir, int iside, std::enable_if_t<(idir < dim), int> = idir>
    KOKKOS_INLINE_FUNCTION void updateAlongFace(
            std::integral_constant<int, idir> idir_tag,
            std::integral_constant<int, iside> iside_tag,
            const Int j,
            const ConsState& uAc_j,
            ConsState& u_j) const noexcept
    {
        using namespace constants;

        const Int j_L = iside == 0 ? j - m_grid.m_strides[idir] : j;
        const Int j_R = iside == 0 ? j : j + m_grid.m_strides[idir];

        const PrimState q_L = Super::getPrim(m_q, j_L);
        const PrimState q_R = Super::getPrim(m_q, j_R);

        const Real c_L = Euler::computeSpeedOfSound(q_L, m_eos);
        const Real c_R = Euler::computeSpeedOfSound(q_R, m_eos);

        const Real vn_L = q_L.v(idir_tag);
        const Real vn_R = q_R.v(idir_tag);

        const Real grav_corr = -half * (q_L.d + q_R.d) * m_dphi(idir_tag);

        const Real a = m_K * std::fmax(q_L.d * c_L, q_R.d * c_R);
        const Real uStar = +half * (vn_L + vn_R) - half * (q_R.p - q_L.p - grav_corr) / a;

        const Int j_upwind = uStar > zero ? j_L : j_R;
        const ConsState uAc_upwind = Super::getCons(m_uAc, j_upwind);
        const Real side = iside == 0 ? -one : +one;
        const Real dtdSdV = m_dt * m_grid.getdSdV(j, idir_tag, iside_tag);
        const Real factor = side * dtdSdV * uStar;
        u_j.d += factor * (uAc_j.d - uAc_upwind.d);
        u_j.e += factor * (uAc_j.e - uAc_upwind.e);
        u_j.m += factor * (uAc_j.m - uAc_upwind.m);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const Team& team) const
    {
        // From given team compute linear index of coordinates of cell (0, jy, jz).
        const Int j0 = Super::teamToLinearIndex(team, m_grid, ghostDepth);

        Kokkos::parallel_for(ark::TeamVectorRange(team, m_x_beg, m_x_end), [&](const Int jx) {
            const int j = j0 + jx;
            const ConsState uAc_j = Super::getCons(m_uAc, j);
            ConsState u_j = uAc_j;

            updateAlongFace(Tags::DirX, Tags::SideL, j, uAc_j, u_j);
            updateAlongFace(Tags::DirX, Tags::SideR, j, uAc_j, u_j);
            updateAlongFace(Tags::DirY, Tags::SideL, j, uAc_j, u_j);
            updateAlongFace(Tags::DirY, Tags::SideR, j, uAc_j, u_j);
            updateAlongFace(Tags::DirZ, Tags::SideL, j, uAc_j, u_j);
            updateAlongFace(Tags::DirZ, Tags::SideR, j, uAc_j, u_j);

            Super::set(m_u, j, u_j);
        });
    }

private:
    Real m_K;
    EquationOfState m_eos;
    UniformGrid<dim> m_grid;
    RealArray<dim> m_u;
    ConstRealArray<dim> m_uAc;
    ConstRealArray<dim> m_q;
    Real m_dt;
    Vector<dim, Real> m_dphi;
    Int m_x_beg;
    Int m_x_end;
};

} // namespace ark
