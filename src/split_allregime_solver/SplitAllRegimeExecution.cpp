#include <Kokkos_Core.hpp>

#include "Params.hpp"
#include "SplitAllRegimeAcousticKernel.hpp"
#include "SplitAllRegimeExecution.hpp"
#include "SplitAllRegimeTransportKernel.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

namespace
{

template <dim_t dim>
void ExecuteAcousticStep(
        const Params<dim>& params,
        const UniformGrid<dim>& grid,
        const RealArray<dim>& u,
        const RealArray<dim>& uAc,
        const RealArray<dim>& q,
        Real dt)
{
    using Kernel = AcousticStepKernel<dim>;
    using TeamPolicy = typename Kernel::TeamPolicy;
    Kernel kernel(params, grid, u, uAc, q, dt);
    const int league_size = Kernel::computeLeagueSize(grid.m_nbCells, Kernel::ghostDepth);
    const int vector_length = TeamPolicy::vector_length_max();
    TeamPolicy policy {league_size, Kokkos::AUTO, vector_length};
    Kokkos::parallel_for("Acoustic kernel - TeamPolicy", policy, kernel);
}

template <dim_t dim>
void ExecuteTransportStep(
        const Params<dim>& params,
        const UniformGrid<dim>& grid,
        const RealArray<dim>& u,
        const RealArray<dim>& uAc,
        const RealArray<dim>& q,
        Real dt)
{
    using Kernel = TransportStepKernel<dim>;
    using TeamPolicy = typename Kernel::TeamPolicy;
    Kernel kernel {params, grid, u, uAc, q, dt};
    const int league_size = Kernel::computeLeagueSize(grid.m_nbCells, Kernel::ghostDepth);
    const int vector_length = TeamPolicy::vector_length_max();
    TeamPolicy policy {league_size, Kokkos::AUTO, vector_length};
    Kokkos::parallel_for("Transport kernel - TeamPolicy", policy, kernel);
}

} // namespace

template <dim_t dim>
void SplitAllRegimeOperator<dim>::execute(
        Params<dim> const& params,
        UniformGrid<dim> const& grid,
        RealArray<dim> const& u,
        RealArray<dim> const& q,
        Real dt) const
{
    ExecuteAcousticStep(params, grid, u, m_uAc, q, dt);

    ExecuteTransportStep(params, grid, u, m_uAc, q, dt);
}

template class SplitAllRegimeOperator<one_d>;
template class SplitAllRegimeOperator<two_d>;
template class SplitAllRegimeOperator<three_d>;

} // namespace ark
