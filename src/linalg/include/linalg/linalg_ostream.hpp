#pragma once

#include "Tensor.hpp"
#include "Vector.hpp"

namespace ark
{

template <int dim, class T>
std::ostream& operator<<(std::ostream& os, const Vector<dim, T>& rhs)
{
    os << "[";
    for (int i = 0; i < dim - 1; ++i)
    {
        os << rhs(i);
        if (i != dim - 1)
        {
            os << ",";
        }
    }
    os << "]";
    return os;
}

template <int dim, class T>
std::ostream& operator<<(std::ostream& os, const Tensor<dim, T>& rhs)
{
    os << "[";
    for (int i = 0; i < dim; ++i)
    {
        os << "[";
        for (int j = 0; j < dim; ++j)
        {
            os << rhs(i, j);
            if (j != dim - 1)
            {
                os << ",";
            }
        }
        os << "]";
        if (i != dim - 1)
        {
            os << ",";
        }
    }
    os << "]";
    return os;
}

} // namespace ark
