#pragma once

#include <memory>
#include <string>

#include "Problem.hpp"
#include "Solver.hpp"

namespace ark
{

template <dim_t dim>
class SolverFactory
{
public:
    static std::shared_ptr<Solver> New(
            const std::string& solver_name,
            std::shared_ptr<Problem<dim>> problem);
};

extern template class SolverFactory<one_d>;
extern template class SolverFactory<two_d>;
extern template class SolverFactory<three_d>;

} // namespace ark
