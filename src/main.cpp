#include <cstdlib>
#include <exception>
#include <memory>
#include <string>

#include "Ark.hpp"
#include "Engine.hpp"
#include "Print.hpp"

#if !defined(NDEBUG)
#include <cfenv>
#endif // !defined(NDEBUG)

int main(int argc, char** argv)
{
#if !defined(NDEBUG)
#if defined(__GNUG__)
    feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW | FE_UNDERFLOW);
#endif // defined(__GNUG__)
#endif // !defined(NDEBUG)

    ark::initialize(argc, argv);

    try
    {
        if (argc < 2)
        {
            throw std::runtime_error("You need to pass the configuration file");
        }

        if (std::string(argv[1]) == "--config")
        {
            std::ostringstream oss;
            ark::print_configuration(oss);
            Print() << oss.str();
        }
        else
        {
            auto engine = ark::EngineFactory::New(argv[1]);

            std::ostringstream oss;
            ark::print_configuration(oss);
            engine->print_configuration(oss);
            Print() << oss.str();

            engine->run();
        }
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

    ark::finalize();

    return EXIT_SUCCESS;
}
