#pragma once

#include <memory>
#include <vector>

#include "io/Writer.hpp"

#include "ConservativeToPrimitiveExecution.hpp"
#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "GodunovARWBFluxesExecution.hpp"
#include "Problem.hpp"
#include "Solver.hpp"
#include "ThermalDiffusionStepExecution.hpp"
#include "Types.hpp"
#include "Utils.hpp"
#include "ViscousStepExecution.hpp"

namespace ark
{

template <dim_t dim>
class GodunovARWBSolver : public Solver
{
    using Super = Solver;

    using Euler = EulerSystem<dim>;
    using EquationOfState = typename Euler::EquationOfState;

    static constexpr Int ghostWidth = 2;

public:
    explicit GodunovARWBSolver(const std::shared_ptr<Problem<dim>>& problem);

    GodunovARWBSolver(const GodunovARWBSolver& x) = default;

    GodunovARWBSolver(GodunovARWBSolver&& x) = default;

    ~GodunovARWBSolver() override = default;

    GodunovARWBSolver& operator=(const GodunovARWBSolver& x) = default;

    GodunovARWBSolver& operator=(GodunovARWBSolver&& x) = default;

    Real computeTimeStep() final;

    void nextIteration(Real dt) final;

    void saveOutput() final;

    std::size_t memoryUsage() const final;

private:
    std::shared_ptr<Problem<dim>> m_problem;
    std::shared_ptr<Params<dim>> m_params;
    std::shared_ptr<UniformGrid<dim>> m_grid;
    std::shared_ptr<io::IWriter<dim>> m_writer;

    std::unique_ptr<IEulerOperator<dim>> m_euler_operator;
    std::unique_ptr<IViscousOperator<dim>> m_viscosity_operator;
    std::unique_ptr<IThermalOperator<dim>> m_thermal_operator;
    std::unique_ptr<IConservativeToPrimitiveOperator<dim>> m_conservative_to_primitive_operator;

    RealArray<dim> m_u;
    RealArray<dim> m_q;
    HostRealArray<dim> m_u_host;
};

extern template class GodunovARWBSolver<one_d>;
extern template class GodunovARWBSolver<two_d>;
extern template class GodunovARWBSolver<three_d>;

} // namespace ark
