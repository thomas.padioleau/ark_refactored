#pragma once

#include "IEulerOperator.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

template <dim_t dim>
class GodunovARWBOperator : public IEulerOperator<dim>
{
public:
    void execute(
            Params<dim> const& params,
            UniformGrid<dim> const& grid,
            RealArray<dim> const& u,
            RealArray<dim> const& q,
            Real dt) const override;
};

extern template class GodunovARWBOperator<one_d>;
extern template class GodunovARWBOperator<two_d>;
extern template class GodunovARWBOperator<three_d>;

} // namespace ark
