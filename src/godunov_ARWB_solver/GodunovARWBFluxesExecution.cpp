#include <Kokkos_Core.hpp>

#include "ars/HydroRiemannAllRegimeWB.hpp"

#include "GodunovARWBFluxesExecution.hpp"
#include "GodunovARWBFluxesKernel.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

namespace
{

template <dim_t dim, class RiemannSolver>
void ExecuteFluxesAndUpdate(
        const Params<dim>& params,
        const UniformGrid<dim>& grid,
        const RealArray<dim>& u,
        const RealArray<dim>& q,
        const RiemannSolver& riemann_solver,
        Real dt)
{
    using Kernel = FluxesAndUpdateKernel_ARWB<dim, RiemannSolver>;
    using TeamPolicy = typename Kernel::TeamPolicy;
    const int league_size = Kernel::computeLeagueSize(grid.m_nbCells, Kernel::ghostDepth);
    const int vector_length = TeamPolicy::vector_length_max();
    const TeamPolicy policy(league_size, Kokkos::AUTO, vector_length);
    const Kernel kernel(params, grid, u, q, riemann_solver, dt);
    Kokkos::parallel_for("Godunov kernel - TeamPolicy", policy, kernel);
}

} // namespace

template <dim_t dim>
void GodunovARWBOperator<dim>::execute(
        Params<dim> const& params,
        UniformGrid<dim> const& grid,
        RealArray<dim> const& u,
        RealArray<dim> const& q,
        Real dt) const
{
    typename EulerSystem<dim>::EquationOfState eos(params.thermo);
    if (params.run.riemann == "ARWB")
    {
        ExecuteFluxesAndUpdate(params, grid, u, q, riemann::ARWB<dim>(params, eos, grid), dt);
    }
    else
    {
        throw;
    }
}

template class GodunovARWBOperator<one_d>;
template class GodunovARWBOperator<two_d>;
template class GodunovARWBOperator<three_d>;

} // namespace ark
