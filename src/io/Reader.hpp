#pragma once

#include <list>
#include <memory>
#include <utility>
#include <vector>

#include "IReader.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark::io
{

template <dim_t dim>
class Reader
{
public:
    Reader() = default;
    Reader(const UniformGrid<dim>& grid,
           const Params<dim>& params,
           const std::vector<std::pair<int, std::string>>& variables);
    Reader(const Reader& x) = default;
    Reader(Reader&& x) = default;
    ~Reader() = default;
    Reader& operator=(const Reader& x) = default;
    Reader& operator=(Reader&& x) = default;

    void read(
            HostArrayDyn u,
            const UniformGrid<dim>& grid,
            Int& iStep,
            Real& time,
            Int& outputId,
            Int& restartId) const;

private:
    std::shared_ptr<IReader<dim>> io_reader;
};

extern template class Reader<one_d>;
extern template class Reader<two_d>;
extern template class Reader<three_d>;

} // namespace ark::io
