#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <list>
#include <memory>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include <hdf5.h>

#include "DistributedMemorySession.hpp"
#include "IReader.hpp"
#include "Params.hpp"
#include "ReaderHDF5_C.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"
#include "Units.hpp"
#include "UtilsHDF5_C.hpp"

namespace ark::io
{

using utils::errCheck;
using utils::getHdf5DataType;

template <dim_t dim>
ReaderHDF5_C<dim>::ReaderHDF5_C(
        const UniformGrid<dim>&,
        const Params<dim>& params,
        const std::vector<std::pair<int, std::string>>& variables)
    : filename {params.run.restart_filename}
    , m_variables {variables}
    , mdatatype {getHdf5DataType<Real>()}
    , precision {sizeof(Real)}
{
}

template <dim_t dim>
void ReaderHDF5_C<dim>::copy_buffer(
        std::vector<Real>& data,
        HostArrayDyn u,
        const UniformGrid<dim>& grid,
        int ivar) const
{
    IntVector<dim> strides {};
    for (int idim = 0; idim < dim; ++idim)
    {
        strides[idim] = (idim == 0) ? 1 : strides[idim - 1] * grid.m_nbCells[idim - 1];
    }
    // transpose array to make data contiguous in memory
    for (Int index = 0; index < grid.nbCells(); ++index)
    {
        IntVector<dim> coord {grid.indexToCoord(index)};
        if (grid.belongsToInnerDomain(coord))
        {
            Int index2 {0};
            for (int idim = 0; idim < dim; ++idim)
            {
                index2 += (coord[idim] - grid.m_ghostWidths[idim]) * strides[idim];
            }
            u(index, ivar) = data[index2];
        }
    }
}

template <dim_t dim>
void ReaderHDF5_C<dim>::read(
        HostArrayDyn u,
        const UniformGrid<dim>& grid,
        Int& iStep,
        Real& time,
        Int& outputId,
        Int& restartId)
{
    // Create a new file using default properties.
    hid_t access_plist = ::H5Pcreate(H5P_FILE_ACCESS);
    errCheck(access_plist, "H5Pcreate");
    std::array<int, dim> coords = {};
#if defined(MPI_SESSION)
    errCheck(::H5Pset_fapl_mpio(access_plist, MPI_COMM_WORLD, MPI_INFO_NULL), "H5Pset_fapl_mpio");
    coords = grid.comm.getCoords(grid.comm.rank());
    const auto dom = grid.m_dom;
#else
    std::array<int, dim> dom = {};
    for (int idim = 0; idim < dim; ++idim)
    {
        dom[idim] = 1;
    }
#endif
    hid_t file = ::H5Fopen(filename.c_str(), H5F_ACC_RDONLY, access_plist);
    errCheck(file, "H5Fopen");
    errCheck(::H5Pclose(access_plist), "H5Pclose");

    // Create the data space for the dataset in memory and in file.
    std::array<hsize_t, 3> mdim;
    for (int idim = 0; idim < 3; ++idim)
    {
        int ridim = 2 - idim; // reverse idim
        mdim[ridim] = (idim < dim ? grid.m_nbCells[idim] : 1);
    }
    hid_t mspace = ::H5Screate_simple(3, mdim.data(), NULL);
    errCheck(mspace, "H5Screate_simple");

    std::array<hsize_t, 3> fdim;
    for (int idim = 0; idim < 3; ++idim)
    {
        int ridim = 2 - idim; // reverse idim
        fdim[ridim] = (idim < dim ? dom[idim] * grid.m_nbCells[idim] : 1);
    }
    hid_t fspace = ::H5Screate_simple(3, fdim.data(), NULL);
    errCheck(fspace, "H5Screate_simple");

    std::array<hsize_t, 3> start;
    std::array<hsize_t, 3> stride = {1, 1, 1};
    std::array<hsize_t, 3> count = {1, 1, 1};
    std::array<hsize_t, 3> block;
    for (int idim = 0; idim < 3; ++idim)
    {
        int ridim = 2 - idim; // reverse idim
        start[ridim] = (idim < dim ? coords[idim] * grid.m_nbCells[idim] : 0);
        block[ridim] = (idim < dim ? grid.m_nbCells[idim] : 1);
    }
    errCheck(
            ::H5Sselect_hyperslab(
                    fspace,
                    H5S_SELECT_SET,
                    start.data(),
                    stride.data(),
                    count.data(),
                    block.data()),
            "H5Sselect_hyperslab");

    std::vector<Real> data(u.extent(0));

    for (const auto& var : m_variables)
    {
        const int ivar = var.first;
        const std::string var_name = var.second;

        std::string name {'/' + var_name};

        hid_t dataset = ::H5Dopen(file, name.c_str(), H5P_DEFAULT);
        errCheck(dataset, "H5Dopen");

        access_plist = ::H5Pcreate(H5P_DATASET_XFER);
        errCheck(access_plist, "H5Pcreate");
#if defined(MPI_SESSION)
        errCheck(::H5Pset_dxpl_mpio(access_plist, H5FD_MPIO_COLLECTIVE), "H5Pset_dxpl_mpio");
#endif

        // Some tests
        {
            hid_t fspace2 = ::H5Dget_space(dataset);
            errCheck(fspace2, "H5Dget_space");
            int ndims = ::H5Sget_simple_extent_ndims(fspace2);
            errCheck(ndims, "H5Sget_simple_extent_ndims");
            // Check rank
            if (ndims != 3)
            {
                throw std::runtime_error("Ranks do not match");
            }

            std::vector<hsize_t> dims(ndims, 0);
            ndims = ::H5Sget_simple_extent_dims(fspace2, dims.data(), NULL);
            errCheck(ndims, "H5Sget_simple_extent_dims");
            // Check dimension extents
            for (int idim = 0; idim < 3; ++idim)
            {
                if (dims[idim] != fdim[idim])
                {
                    throw std::runtime_error("Dimensions do not match");
                }
            }

            // Check if right data type
            hid_t fdatatype = ::H5Dget_type(dataset);
            errCheck(fdatatype, "H5Dget_type");
            hid_t fnativedatatype = ::H5Tget_native_type(fdatatype, H5T_DIR_DEFAULT);
            errCheck(fnativedatatype, "H5Dget_native_type");
            htri_t equal = ::H5Tequal(fnativedatatype, mdatatype);
            errCheck(equal, "H5Tequal");
            if (equal == 0)
            {
                throw std::runtime_error("Datatypes do not match");
            }
            errCheck(::H5Tclose(fnativedatatype), "H5Tclose");
            errCheck(::H5Tclose(fdatatype), "H5Tclose");
        }

        errCheck(
                ::H5Dread(dataset, mdatatype, mspace, fspace, access_plist, data.data()),
                "H5Dread");

        copy_buffer(data, u, grid, ivar);

        errCheck(::H5Pclose(access_plist), "H5Pclose");
        errCheck(::H5Dclose(dataset), "H5Dclose");
    }

    readScalar(time, "time", file);
    readScalar(iStep, "iStep", file);
    readScalar(outputId, "output_id", file);
    readScalar(restartId, "restart_id", file);

    errCheck(::H5Sclose(fspace), "H5Sclose");
    errCheck(::H5Sclose(mspace), "H5Sclose");
    errCheck(::H5Fclose(file), "H5Fclose");
} // read

template <dim_t dim>
void ReaderHDF5_C<dim>::readScalar(Real& scalar, const std::string& name, hid_t file) const
{
    hid_t dataset = ::H5Dopen(file, name.c_str(), H5P_DEFAULT);
    errCheck(dataset, "H5Dopen");

    hid_t access_plist = ::H5Pcreate(H5P_DATASET_XFER);
    errCheck(access_plist, "H5Pcreate");
#if defined(MPI_SESSION)
    errCheck(::H5Pset_dxpl_mpio(access_plist, H5FD_MPIO_COLLECTIVE), "H5Pset_dxpl_mpio");
#endif
    errCheck(::H5Dread(dataset, mdatatype, H5S_ALL, H5S_ALL, access_plist, &scalar), "H5Dread");

    errCheck(::H5Pclose(access_plist), "H5Pclose");
    errCheck(::H5Dclose(dataset), "H5Dclose");
}

template <dim_t dim>
void ReaderHDF5_C<dim>::readScalar(Int& scalar, const std::string& name, hid_t file) const
{
    hid_t dataset = ::H5Dopen(file, name.c_str(), H5P_DEFAULT);
    errCheck(dataset, "H5Dopen");

    hid_t access_plist = ::H5Pcreate(H5P_DATASET_XFER);
    errCheck(access_plist, "H5Pcreate");
#if defined(MPI_SESSION)
    errCheck(::H5Pset_dxpl_mpio(access_plist, H5FD_MPIO_COLLECTIVE), "H5Pset_dxpl_mpio");
#endif
    hid_t scalar_datatype = getHdf5DataType<Int>();
    errCheck(
            ::H5Dread(dataset, scalar_datatype, H5S_ALL, H5S_ALL, access_plist, &scalar),
            "H5Dread");

    errCheck(::H5Pclose(access_plist), "H5Pclose");
    errCheck(::H5Dclose(dataset), "H5Dclose");
}

template class ReaderHDF5_C<one_d>;
template class ReaderHDF5_C<two_d>;
template class ReaderHDF5_C<three_d>;

} // namespace ark::io
