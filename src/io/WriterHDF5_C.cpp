#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <list>
#include <memory>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include <hdf5.h>

#include "DistributedMemorySession.hpp"
#include "IWriter.hpp"
#include "Params.hpp"
#include "Print.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"
#include "Units.hpp"
#include "UtilsHDF5_C.hpp"
#include "WriterHDF5_C.hpp"

namespace ark::io
{

using utils::errCheck;
using utils::getHdf5DataType;

template <dim_t dim>
WriterHDF5_C<dim>::WriterHDF5_C(
        const UniformGrid<dim>&,
        const Params<dim>& params,
        const std::string& prefix,
        const std::vector<std::pair<int, std::string>>& variables)
    : directory {params.output.directory}
    , m_prefix {prefix}
    , m_variables {variables}
    , dataType {getHdf5DataType<Real>()}
    , precision {sizeof(Real)}
{
}

template <dim_t dim>
std::string WriterHDF5_C<dim>::getFilename(Int outputId) const
{
    // write outputId in string outputNum
    std::ostringstream outputNum;
    outputNum << std::setw(std::numeric_limits<Int>::digits10);
    outputNum << std::setfill('0');
    outputNum << outputId;

    // concatenate file prefix + file number + suffix
    std::string filename {m_prefix};
    filename += "_time_" + outputNum.str();
    filename += ".h5";
    return filename;
}

template <dim_t dim>
void WriterHDF5_C<dim>::copy_to_buffer(
        std::vector<Real>& data,
        HostConstArrayDyn u,
        const UniformGrid<dim>& grid,
        int ivar) const
{
    // transpose array to make data contiguous in memory
    for (Int index = 0; index < grid.nbCells(); ++index)
    {
        data[index] = u(index, ivar);
    }
}

template <dim_t dim>
void WriterHDF5_C<dim>::write(
        HostConstArrayDyn u,
        const UniformGrid<dim>& grid,
        Int iStep,
        Real time,
        Real gamma,
        Real mmw)
{
    auto& outputId = IWriter<dim>::m_outputId;

    IWriter<dim>::m_previous_outputs.push_back(std::make_pair(outputId, time));

    // Create a new file using default properties.
    hid_t access_plist = ::H5Pcreate(H5P_FILE_ACCESS);
    errCheck(access_plist, "H5Pcreate");
    std::array<int, dim> coords = {};
#if defined(MPI_SESSION)
    errCheck(::H5Pset_fapl_mpio(access_plist, MPI_COMM_WORLD, MPI_INFO_NULL), "H5Pset_fapl_mpio");
    coords = grid.comm.getCoords(grid.comm.rank());
    const auto dom = grid.m_dom;
#else
    std::array<int, dim> dom = {};
    for (int idim = 0; idim < dim; ++idim)
    {
        dom[idim] = 1;
    }
#endif
    std::string filename_full = directory + '/' + getFilename(outputId);
    hid_t file = ::H5Fcreate(filename_full.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, access_plist);
    errCheck(file, "H5Fcreate");
    errCheck(::H5Pclose(access_plist), "H5Pclose");

    // Create the data space for the dataset in memory and in file.
    std::array<hsize_t, 3> mdim;
    for (int idim = 0; idim < 3; ++idim)
    {
        int ridim = 2 - idim; // reverse idim
        mdim[ridim] = (idim < dim ? grid.m_nbCells[idim] + 2 * grid.m_ghostWidths[idim] : 1);
    }
    hid_t mspace = ::H5Screate_simple(3, mdim.data(), NULL);
    errCheck(mspace, "H5Screate_simple");
    {
        std::array<hsize_t, 3> start;
        std::array<hsize_t, 3> stride = {1, 1, 1};
        std::array<hsize_t, 3> count = {1, 1, 1};
        std::array<hsize_t, 3> block;
        for (int idim = 0; idim < 3; ++idim)
        {
            int ridim = 2 - idim; // reverse idim
            start[ridim] = (idim < dim ? grid.m_ghostWidths[idim] : 0);
            block[ridim] = (idim < dim ? grid.m_nbCells[idim] : 1);
        }
        errCheck(
                ::H5Sselect_hyperslab(
                        mspace,
                        H5S_SELECT_SET,
                        start.data(),
                        stride.data(),
                        count.data(),
                        block.data()),
                "H5Sselect_hyperslab");
    }

    std::array<hsize_t, 3> fdim;
    for (int idim = 0; idim < 3; ++idim)
    {
        int ridim = 2 - idim; // reverse idim
        fdim[ridim] = (idim < dim ? dom[idim] * grid.m_nbCells[idim] : 1);
    }
    hid_t fspace = ::H5Screate_simple(3, fdim.data(), NULL);
    errCheck(fspace, "H5Screate_simple");

    {
        std::array<hsize_t, 3> start;
        std::array<hsize_t, 3> stride = {1, 1, 1};
        std::array<hsize_t, 3> count = {1, 1, 1};
        std::array<hsize_t, 3> block;
        for (int idim = 0; idim < 3; ++idim)
        {
            int ridim = 2 - idim; // reverse idim
            start[ridim] = (idim < dim ? coords[idim] * grid.m_nbCells[idim] : 0);
            block[ridim] = (idim < dim ? grid.m_nbCells[idim] : 1);
        }
        errCheck(
                ::H5Sselect_hyperslab(
                        fspace,
                        H5S_SELECT_SET,
                        start.data(),
                        stride.data(),
                        count.data(),
                        block.data()),
                "H5Sselect_hyperslab");
    }

    constexpr bool isLayoutLeft
            = std::is_same<HostConstArrayDyn::array_layout, Kokkos::LayoutLeft>::value;
    std::vector<Real> buffer;
    if (!isLayoutLeft)
    {
        buffer.resize(u.extent(0));
    }

    for (const auto& var : m_variables)
    {
        const int ivar = var.first;
        const std::string var_name = var.second;

        std::string name {'/' + var_name};

        hid_t dataset = ::H5Dcreate(
                file,
                name.c_str(),
                dataType,
                fspace,
                H5P_DEFAULT,
                H5P_DEFAULT,
                H5P_DEFAULT);
        errCheck(dataset, "H5Dcreate");

        access_plist = ::H5Pcreate(H5P_DATASET_XFER);
        errCheck(access_plist, "H5Pcreate");
#if defined(MPI_SESSION)
        errCheck(::H5Pset_dxpl_mpio(access_plist, H5FD_MPIO_COLLECTIVE), "H5Pset_dxpl_mpio");
#endif

        Real* buffer_ptr;
        if (!isLayoutLeft)
        {
            copy_to_buffer(buffer, u, grid, ivar);
            buffer_ptr = buffer.data();
        }
        else
        {
            buffer_ptr = &u(0, ivar);
        }

        errCheck(
                ::H5Dwrite(dataset, dataType, mspace, fspace, access_plist, buffer_ptr),
                "H5Dwrite");

        errCheck(::H5Pclose(access_plist), "H5Pclose");
        errCheck(::H5Dclose(dataset), "H5Dclose");
    }

    writeScalar(gamma, "gamma", file);
    writeScalar(mmw, "mmw", file);
    writeScalar(code_units::constants::Rstar_h, "Rstar_h", file);
    writeScalar(time, "time", file);
    writeScalar(iStep, "iStep", file);
    writeScalar(outputId, "output_id", file);
    writeScalar(IWriter<dim>::m_restartId, "restart_id", file);

    errCheck(::H5Sclose(fspace), "H5Sclose");
    errCheck(::H5Sclose(mspace), "H5Sclose");
    errCheck(::H5Fclose(file), "H5Fclose");

    if (Session::isIOProc())
    {
        writeXML(grid);
    }

    ++outputId;
} // save

template <dim_t dim>
void WriterHDF5_C<dim>::writeScalar(Real scalar, const std::string& name, hid_t file) const
{
    hid_t fspace = ::H5Screate(H5S_SCALAR);
    errCheck(fspace, "H5Screate");
    hid_t dataset = ::
            H5Dcreate(file, name.c_str(), dataType, fspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    errCheck(dataset, "H5Dcreate");
    hid_t access_plist = ::H5Pcreate(H5P_DATASET_XFER);
    errCheck(access_plist, "H5Pcreate");
#if defined(MPI_SESSION)
    errCheck(::H5Pset_dxpl_mpio(access_plist, H5FD_MPIO_COLLECTIVE), "H5Pset_dxpl_mpio");
#endif
    errCheck(::H5Dwrite(dataset, dataType, H5S_ALL, H5S_ALL, access_plist, &scalar), "H5Dwrite");

    errCheck(::H5Pclose(access_plist), "H5Pclose");
    errCheck(::H5Sclose(fspace), "H5Sclose");
    errCheck(::H5Dclose(dataset), "H5Dclose");
}

template <dim_t dim>
void WriterHDF5_C<dim>::writeScalar(Int scalar, const std::string& name, hid_t file) const
{
    hid_t fspace = ::H5Screate(H5S_SCALAR);
    errCheck(fspace, "H5Screate");
    hid_t dataset = ::
            H5Dcreate(file, name.c_str(), dataType, fspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    errCheck(dataset, "H5Dcreate");
    hid_t access_plist = ::H5Pcreate(H5P_DATASET_XFER);
    errCheck(access_plist, "H5Pcreate");
#if defined(MPI_SESSION)
    errCheck(::H5Pset_dxpl_mpio(access_plist, H5FD_MPIO_COLLECTIVE), "H5Pset_dxpl_mpio");
#endif
    hid_t scalar_datatype = getHdf5DataType<Int>();
    errCheck(
            ::H5Dwrite(dataset, scalar_datatype, H5S_ALL, H5S_ALL, access_plist, &scalar),
            "H5Dwrite");

    errCheck(::H5Pclose(access_plist), "H5Pclose");
    errCheck(::H5Sclose(fspace), "H5Sclose");
    errCheck(::H5Dclose(dataset), "H5Dclose");
}

template <dim_t dim>
void WriterHDF5_C<dim>::writeXML(const UniformGrid<dim>& grid) const
{
    const auto& restartId = IWriter<dim>::m_restartId;
    std::ostringstream restartNum;
    restartNum << std::setw(std::numeric_limits<Int>::digits10);
    restartNum << std::setfill('0');
    restartNum << restartId;

    const std::string xdmfFilenameFull {
            directory + '/' + m_prefix + '_' + restartNum.str() + ".xmf"};
    std::ofstream xdmfFile(xdmfFilenameFull, std::ofstream::trunc);

    xdmfFile << "<?xml version=\"1.0\"?>\n";
    xdmfFile << "<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>\n";
    xdmfFile << "<Xdmf Version=\"2.0\">\n";
    xdmfFile << std::string(2, ' ') << "<Domain>\n";
    xdmfFile << std::string(4, ' ');
    xdmfFile << "<Grid";
    xdmfFile << " Name=" << '"' << "TimeSeries" << '"';
    xdmfFile << " GridType=" << '"' << "Collection" << '"';
    xdmfFile << " CollectionType=" << '"' << "Temporal" << '"';
    xdmfFile << ">\n";

    Int nbCells {1};
    for (int idim = 0; idim < dim; ++idim)
    {
        nbCells *= grid.m_nbCells[idim];
    }

    for (const auto& previous_output : IWriter<dim>::m_previous_outputs)
    {
        xdmfFile << std::string(6, ' ');
        xdmfFile << "<Grid Name=" << '"' << "output" << '"';
        xdmfFile << " GridType=" << '"' << "Uniform" << '"';
        xdmfFile << ">\n";
        xdmfFile << std::string(8, ' ');
        xdmfFile << "<Time Value=" << '"' << previous_output.second << '"' << "/>\n";

        // topology CoRectMesh
        xdmfFile << std::string(8, ' ');
        xdmfFile << "<Topology";
        xdmfFile << " TopologyType=" << '"' << "3DCoRectMesh" << '"';
        xdmfFile << " Dimensions=" << '"';
        for (int idim = 2; idim >= 0; --idim)
        {
            xdmfFile << (idim < dim ? grid.m_dom[idim] * grid.m_nbCells[idim] + 1 : 2);
            xdmfFile << (idim == 0 ? "\"" : " ");
        }
        xdmfFile << "/>\n";

        // geometry
        xdmfFile << std::string(8, ' ');
        xdmfFile << "<Geometry";
        xdmfFile << " GeometryType=" << '"' << "ORIGIN_DXDYDZ" << '"';
        xdmfFile << ">\n";

        xdmfFile << std::string(10, ' ');
        xdmfFile << "<DataItem";
        xdmfFile << " Name=" << '"' << "Origin" << '"';
        xdmfFile << " NumberType=" << '"' << "Float" << '"';
        xdmfFile << " Precision=" << '"' << precision << '"';
        xdmfFile << " Dimensions=" << '"' << 3 << '"';
        xdmfFile << " Format=" << '"' << "XML" << '"';
        xdmfFile << ">\n";
        xdmfFile << std::string(12, ' ');
        for (int idim = 2; idim >= 0; --idim)
        {
            xdmfFile << (idim < dim ? grid.m_lowGlobal[idim] : constants::zero);
            xdmfFile << (idim == 0 ? "\n" : " ");
        }
        xdmfFile << std::string(10, ' ') << "</DataItem>\n";

        xdmfFile << std::string(10, ' ');
        xdmfFile << "<DataItem";
        xdmfFile << " Name=" << '"' << "Spacing" << '"';
        xdmfFile << " NumberType=" << '"' << "Float" << '"';
        xdmfFile << " Precision=" << '"' << precision << '"';
        xdmfFile << " Dimensions=" << '"' << 3 << '"';
        xdmfFile << " Format=" << '"' << "XML" << '"';
        xdmfFile << ">\n";
        xdmfFile << std::string(12, ' ');
        for (int idim = 2; idim >= 0; --idim)
        {
            xdmfFile << (idim < dim ? grid.m_dl[idim] : constants::one);
            xdmfFile << (idim == 0 ? "\n" : " ");
        }
        xdmfFile << std::string(10, ' ') << "</DataItem>\n";

        xdmfFile << std::string(8, ' ') << "</Geometry>\n";

        // Write gamma
        xdmfFile << std::string(8, ' ');
        xdmfFile << "<Attribute";
        xdmfFile << " Center=" << '"' << "Grid" << '"';
        xdmfFile << " Name=" << '"' << "gamma" << '"';
        xdmfFile << " AttributeType=" << '"' << "Scalar" << '"';
        xdmfFile << ">\n";
        xdmfFile << std::string(10, ' ');
        xdmfFile << "<DataItem";
        xdmfFile << " NumberType=" << '"' << "Float" << '"';
        xdmfFile << " Precision=" << '"' << precision << '"';
        xdmfFile << " Dimensions=" << '"' << 1 << '"';
        xdmfFile << " Format=" << '"' << "HDF" << '"';
        xdmfFile << ">\n";
        xdmfFile << std::string(12, ' ') << getFilename(previous_output.first) << ":/"
                 << "gamma"
                 << "\n";
        xdmfFile << std::string(10, ' ') << "</DataItem>\n";
        xdmfFile << std::string(8, ' ') << "</Attribute>\n";
        // Write mmw
        xdmfFile << std::string(8, ' ');
        xdmfFile << "<Attribute";
        xdmfFile << " Center=" << '"' << "Grid" << '"';
        xdmfFile << " Name=" << '"' << "mmw" << '"';
        xdmfFile << " AttributeType=" << '"' << "Scalar" << '"';
        xdmfFile << ">\n";
        xdmfFile << std::string(10, ' ');
        xdmfFile << "<DataItem";
        xdmfFile << " NumberType=" << '"' << "Float" << '"';
        xdmfFile << " Precision=" << '"' << precision << '"';
        xdmfFile << " Dimensions=" << '"' << 1 << '"';
        xdmfFile << " Format=" << '"' << "HDF" << '"';
        xdmfFile << ">\n";
        xdmfFile << std::string(12, ' ') << getFilename(previous_output.first) << ":/"
                 << "mmw"
                 << "\n";
        xdmfFile << std::string(10, ' ') << "</DataItem>\n";
        xdmfFile << std::string(8, ' ') << "</Attribute>\n";
        // Write Rstar_h
        xdmfFile << std::string(8, ' ');
        xdmfFile << "<Attribute";
        xdmfFile << " Center=" << '"' << "Grid" << '"';
        xdmfFile << " Name=" << '"' << "Rstar_h" << '"';
        xdmfFile << " AttributeType=" << '"' << "Scalar" << '"';
        xdmfFile << ">\n";
        xdmfFile << std::string(10, ' ');
        xdmfFile << "<DataItem";
        xdmfFile << " NumberType=" << '"' << "Float" << '"';
        xdmfFile << " Precision=" << '"' << precision << '"';
        xdmfFile << " Dimensions=" << '"' << 1 << '"';
        xdmfFile << " Format=" << '"' << "HDF" << '"';
        xdmfFile << ">\n";
        xdmfFile << std::string(12, ' ') << getFilename(previous_output.first) << ":/"
                 << "Rstar_h"
                 << "\n";
        xdmfFile << std::string(10, ' ') << "</DataItem>\n";
        xdmfFile << std::string(8, ' ') << "</Attribute>\n";

        for (const auto& var : m_variables)
        {
            const std::string var_name = var.second;

            xdmfFile << std::string(8, ' ');
            xdmfFile << "<Attribute";
            xdmfFile << " Center=" << '"' << "Cell" << '"';
            xdmfFile << " Name=" << '"' << var_name << '"';
            xdmfFile << " AttributeType=" << '"' << "Scalar" << '"';
            xdmfFile << ">\n";
            xdmfFile << std::string(10, ' ');
            xdmfFile << "<DataItem";
            xdmfFile << " NumberType=" << '"' << "Float" << '"';
            xdmfFile << " Precision=" << '"' << precision << '"';

            xdmfFile << " Dimensions=\"";
            for (int idim = 2; idim >= 0; --idim)
            {
                xdmfFile << (idim < dim ? grid.m_dom[idim] * grid.m_nbCells[idim] : 1);
                xdmfFile << (idim == 0 ? "\"" : " ");
            }

            xdmfFile << " Format=" << '"' << "HDF" << '"';
            xdmfFile << ">\n";
            xdmfFile << std::string(12, ' ') << getFilename(previous_output.first) << ":/"
                     << var_name << "\n";
            xdmfFile << std::string(10, ' ') << "</DataItem>\n";
            xdmfFile << std::string(8, ' ') << "</Attribute>\n";
        }
        // finalize grid file for the current time step
        xdmfFile << std::string(6, ' ') << "</Grid>\n";
    }

    // finalize Xdmf wrapper file
    xdmfFile << std::string(4, ' ') << "</Grid>\n";
    xdmfFile << std::string(2, ' ') << "</Domain>\n";
    xdmfFile << std::string(0, ' ') << "</Xdmf>\n";
}

template class WriterHDF5_C<one_d>;
template class WriterHDF5_C<two_d>;
template class WriterHDF5_C<three_d>;

} // namespace ark::io
