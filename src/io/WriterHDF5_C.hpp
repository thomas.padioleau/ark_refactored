#pragma once

#include <string>
#include <utility>
#include <vector>

#include <hdf5.h>

#include "IWriter.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"
#include "Units.hpp"

namespace ark::io
{

template <dim_t dim>
class WriterHDF5_C : public IWriter<dim>
{
public:
    WriterHDF5_C() = default;

    WriterHDF5_C(
            const UniformGrid<dim>& grid,
            const Params<dim>& params,
            const std::string& prefix,
            const std::vector<std::pair<int, std::string>>& variables);

    WriterHDF5_C(const WriterHDF5_C& x) = default;

    WriterHDF5_C(WriterHDF5_C&& x) = default;

    ~WriterHDF5_C() override = default;

    WriterHDF5_C& operator=(const WriterHDF5_C& x) = default;

    WriterHDF5_C& operator=(WriterHDF5_C&& x) = default;

    void write(
            HostConstArrayDyn u,
            const UniformGrid<dim>& grid,
            Int iStep,
            Real time,
            Real gamma,
            Real mmw) final;

    void writeScalar(Real scalar, const std::string& name, hid_t file) const;

    void writeScalar(Int scalar, const std::string& name, hid_t file) const;

    void writeXML(const UniformGrid<dim>& grid) const;

    void copy_to_buffer(
            std::vector<Real>& data,
            HostConstArrayDyn u,
            const UniformGrid<dim>& grid,
            int var) const;

    std::string getFilename(Int iStep) const;

private:
    std::string directory;
    std::string m_prefix;
    std::vector<std::pair<int, std::string>> m_variables;
    hid_t dataType;
    int precision;
}; // class WriterHDF5_C

extern template class WriterHDF5_C<one_d>;
extern template class WriterHDF5_C<two_d>;
extern template class WriterHDF5_C<three_d>;

} // namespace ark::io
