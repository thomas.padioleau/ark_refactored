#include "DistributedMemorySession.hpp"
#include "IWriter.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"
#include "Writer.hpp"
#if defined(Ark_ENABLE_HDF5_C)
#include "WriterHDF5_C.hpp"
#endif
#include <chrono>
#include <list>
#include <memory>
#include <stdexcept>
#include <vector>

#include "WriterVTK.hpp"

namespace ark::io
{

template <dim_t dim>
std::shared_ptr<IWriter<dim>> WriterFactory<dim>::New(
        const UniformGrid<dim>& grid,
        const Params<dim>& params,
        const std::string& type,
        const std::string& prefix,
        const std::vector<std::pair<int, std::string>>& variables)
{
    std::shared_ptr<IWriter<dim>> ptr(nullptr);
    if (type == "hdf5")
    {
#if defined(Ark_ENABLE_HDF5_C)
        ptr = std::make_shared<WriterHDF5_C<dim>>(grid, params, prefix, variables);
#else
        throw std::runtime_error("ARK has not been compiled with HDF5 support\n");
#endif
    }
    else if (type == "vtk")
    {
        ptr = std::make_shared<WriterVTK<dim>>(grid, params, prefix, variables);
    }
    else
    {
        throw std::runtime_error("Invalid writer.");
    }
    return ptr;
}

// template <dim_t dim>
// void Writer<dim>::write(HostConstArrayDyn u, const UniformGrid<dim>& grid,
//                         Int iStep, Real time, Real gamma, Real mmw) const
// {
//     Print() << " * I/O\n";
//     for (const auto& io_writer : io_writers)
//     {
//         io_writer->write(u, grid, iStep, time, gamma, mmw);
//         Session::synchronize();
//         double bandwidth = 1.0e-6 * (static_cast<double>(Session::getNProc()) *
//                                      static_cast<double>(grid.nbCells()) *
//                                      static_cast<double>(u.extent(1)) * // nbvar
//                                      static_cast<double>(sizeof(Real)) /
//                                      timer.template count<std::chrono::duration<double>>());
//         Print() << " ** Duration : " << timer.template count<std::chrono::duration<double>>() << " s\n";
//         Print() << " ** Bandwidth: " << bandwidth << " MB/s\n";
//     }
// }

template struct WriterFactory<one_d>;
template struct WriterFactory<two_d>;
template struct WriterFactory<three_d>;

} // namespace ark::io
