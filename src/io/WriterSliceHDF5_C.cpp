#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <memory>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include <hdf5.h>

#include "DistributedMemorySession.hpp"
#include "IWriter.hpp"
#include "Params.hpp"
#include "Print.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"
#include "Units.hpp"
#include "UtilsHDF5_C.hpp"
#include "WriterSliceHDF5_C.hpp"

namespace ark::io
{

using utils::errCheck;
using utils::getHdf5DataType;

template <dim_t dim>
WriterSliceHDF5_C<dim>::WriterSliceHDF5_C(
        const UniformGrid<dim>& grid,
        int idir_slice,
        const Params<dim>& params)
    : directory {params.output.directory}
    , prefix {params.output.prefix}
    , varNames {"rho", "energy", "mx", "my", "mz"}
    , dataType {getHdf5DataType<Real>()}
    , precision {4}
    , m_idir_slice {idir_slice}
    , m_local_slice_index
{
    0
}
#if defined(MPI_SESSION)
, m_comm_slice(MPI_COMM_NULL)
#endif // MPI_SESSION
{
    Real slice = constants::half;
    int mpi_slice_index;
    if (slice <= params.mesh.low[m_idir_slice])
    {
        slice = params.mesh.low[m_idir_slice];
        m_local_slice_index = grid.m_ghostWidths[m_idir_slice];
        mpi_slice_index = 0;
    }
    else if (slice >= params.mesh.up[m_idir_slice])
    {
        slice = params.mesh.up[m_idir_slice];
        m_local_slice_index = grid.m_ghostWidths[m_idir_slice] + grid.m_nbCells[m_idir_slice];
        mpi_slice_index = grid.m_dom[m_idir_slice] - 1;
    }
    else
    {
        int global_index = static_cast<int>(std::lround(
                (slice - params.mesh.low[m_idir_slice]) / grid.dl(m_idir_slice) - constants::half));
        mpi_slice_index = global_index / grid.m_nbCells[m_idir_slice];
        m_local_slice_index = global_index - mpi_slice_index * grid.m_nbCells[m_idir_slice]
                              + grid.m_ghostWidths[m_idir_slice];
    }
#if defined(MPI_SESSION)
    std::array<int, dim> mpi_coords = grid.comm.getCoords(grid.comm.rank());
    int color = (mpi_coords[m_idir_slice] == mpi_slice_index) ? 0 : MPI_UNDEFINED;
    int key = 0;
    ::MPI_Comm_split(MPI_COMM_WORLD, color, key, &m_comm_slice);
#endif // MPI_SESSION
}

template <dim_t dim>
WriterSliceHDF5_C<dim>::~WriterSliceHDF5_C()
{
#if defined(MPI_SESSION)
    if (m_comm_slice != MPI_COMM_NULL)
    {
        ::MPI_Comm_free(&m_comm_slice);
    }
#endif // MPI_SESSION
}

template <dim_t dim>
std::string WriterSliceHDF5_C<dim>::getFilename(Int outputId) const
{
    // write outputId in string outputNum
    std::ostringstream outputNum;
    outputNum << std::setw(std::numeric_limits<Int>::digits10);
    outputNum << std::setfill('0');
    outputNum << outputId;

    // concatenate file prefix + file number + suffix
    std::string filename {prefix};
    filename += "_slice_time_" + outputNum.str();
    filename += ".h5";
    return filename;
}

template <dim_t dim>
void WriterSliceHDF5_C<dim>::copy_to_buffer(
        std::vector<Real>& data,
        HostConstArrayDyn u,
        const UniformGrid<dim>& grid,
        int ivar) const
{
    // transpose array to make data contiguous in memory
    for (Int index = 0; index < grid.nbCells(); ++index)
    {
        data[index] = u(index, ivar);
    }
}

template <dim_t dim>
void WriterSliceHDF5_C<dim>::write(
        HostConstArrayDyn u,
        const UniformGrid<dim>& grid,
        Int iStep,
        Real time,
        Real gamma,
        Real mmw)
{
    auto& outputId = IWriter<dim>::m_outputId;
    IWriter<dim>::m_previous_outputs.push_back(std::make_pair(outputId, time));

#if defined(MPI_SESSION)
    if (m_comm_slice != MPI_COMM_NULL)
#endif
    {
        // Create a new file using default properties.
        hid_t access_plist = ::H5Pcreate(H5P_FILE_ACCESS);
        errCheck(access_plist, "H5Pcreate");
        std::array<int, dim> coords = {};
#if defined(MPI_SESSION)
        errCheck(::H5Pset_fapl_mpio(access_plist, m_comm_slice, MPI_INFO_NULL), "H5Pset_fapl_mpio");
        coords = grid.comm.getCoords(grid.comm.rank());
        const auto dom = grid.m_dom;
#else
        std::array<int, dim> dom = {};
        for (int idim = 0; idim < dim; ++idim)
        {
            dom[idim] = 1;
        }
#endif
        std::string filename_full = directory + '/' + getFilename(outputId);
        hid_t file = ::H5Fcreate(filename_full.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, access_plist);
        errCheck(file, "H5Fcreate");
        errCheck(::H5Pclose(access_plist), "H5Pclose");

        // Create the data space for the dataset in memory and in file.
        std::array<hsize_t, 3> mdim;
        for (int idim = 0; idim < 3; ++idim)
        {
            int ridim = 2 - idim; // reverse idim
            mdim[ridim] = (idim < dim ? grid.m_nbCells[idim] + 2 * grid.m_ghostWidths[idim] : 1);
        }
        hid_t mspace = ::H5Screate_simple(3, mdim.data(), NULL);
        errCheck(mspace, "H5Screate_simple");
        {
            std::array<hsize_t, 3> start;
            std::array<hsize_t, 3> count = {1, 1, 1};
            for (int idim = 0; idim < 3; ++idim)
            {
                int ridim = 2 - idim; // reverse idim
                start[ridim]
                        = (idim < dim
                                   ? (idim == m_idir_slice ? m_local_slice_index
                                                           : grid.m_ghostWidths[idim])
                                   : 0);
                count[ridim] = ((idim < dim && idim != m_idir_slice) ? grid.m_nbCells[idim] : 1);
            }
            errCheck(
                    ::H5Sselect_hyperslab(
                            mspace,
                            H5S_SELECT_SET,
                            start.data(),
                            NULL,
                            count.data(),
                            NULL),
                    "H5Sselect_hyperslab");
        }

        std::array<hsize_t, 3> fdim;
        for (int idim = 0; idim < 3; ++idim)
        {
            int ridim = 2 - idim; // reverse idim
            fdim[ridim]
                    = ((idim < dim && idim != m_idir_slice) ? dom[idim] * grid.m_nbCells[idim] : 1);
        }
        hid_t fspace = ::H5Screate_simple(3, fdim.data(), NULL);
        errCheck(fspace, "H5Screate_simple");

        {
            std::array<hsize_t, 3> start;
            std::array<hsize_t, 3> count;
            for (int idim = 0; idim < 3; ++idim)
            {
                int ridim = 2 - idim; // reverse idim
                start[ridim]
                        = ((idim < dim && idim != m_idir_slice)
                                   ? coords[idim] * grid.m_nbCells[idim]
                                   : 0);
                count[ridim] = ((idim < dim && idim != m_idir_slice) ? grid.m_nbCells[idim] : 1);
            }
            errCheck(
                    ::H5Sselect_hyperslab(
                            fspace,
                            H5S_SELECT_SET,
                            start.data(),
                            NULL,
                            count.data(),
                            NULL),
                    "H5Sselect_hyperslab");
        }

        constexpr bool isLayoutLeft
                = std::is_same<HostConstArrayDyn::array_layout, Kokkos::LayoutLeft>::value;
        std::vector<Real> buffer;
        if (!isLayoutLeft)
        {
            buffer.resize(u.extent(0));
        }

        for (int ivar = 0; ivar < dim + 2; ++ivar)
        {
            std::string name {'/' + varNames[ivar]};

            hid_t fdatatype = H5T_NATIVE_FLOAT;
            hid_t dataset = ::H5Dcreate(
                    file,
                    name.c_str(),
                    fdatatype,
                    fspace,
                    H5P_DEFAULT,
                    H5P_DEFAULT,
                    H5P_DEFAULT);
            errCheck(dataset, "H5Dcreate");

            access_plist = ::H5Pcreate(H5P_DATASET_XFER);
            errCheck(access_plist, "H5Pcreate");
#if defined(MPI_SESSION)
            errCheck(::H5Pset_dxpl_mpio(access_plist, H5FD_MPIO_COLLECTIVE), "H5Pset_dxpl_mpio");
#endif

            Real* buffer_ptr;
            if (!isLayoutLeft)
            {
                copy_to_buffer(buffer, u, grid, ivar);
                buffer_ptr = buffer.data();
            }
            else
            {
                buffer_ptr = &u(0, ivar);
            }

            hid_t mdatatype = dataType;
            errCheck(
                    ::H5Dwrite(dataset, mdatatype, mspace, fspace, access_plist, buffer_ptr),
                    "H5Dwrite");

            errCheck(::H5Pclose(access_plist), "H5Pclose");
            errCheck(::H5Dclose(dataset), "H5Dclose");
        }

        writeScalar(gamma, "gamma", file);
        writeScalar(mmw, "mmw", file);
        writeScalar(code_units::constants::Rstar_h, "Rstar_h", file);
        writeScalar(time, "time", file);
        writeScalar(iStep, "iStep", file);
        writeScalar(outputId, "output_id", file);
        writeScalar(IWriter<dim>::m_restartId, "restart_id", file);

        errCheck(::H5Sclose(fspace), "H5Sclose");
        errCheck(::H5Sclose(mspace), "H5Sclose");
        errCheck(::H5Fclose(file), "H5Fclose");
    }

    if (Session::isIOProc())
    {
        writeXML(grid);
    }

    ++outputId;
} // save

template <dim_t dim>
void WriterSliceHDF5_C<dim>::writeScalar(Real scalar, const std::string& name, hid_t file) const
{
    hid_t fspace = ::H5Screate(H5S_SCALAR);
    errCheck(fspace, "H5Screate");
    hid_t dataset = ::
            H5Dcreate(file, name.c_str(), dataType, fspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    errCheck(dataset, "H5Dcreate");
    hid_t access_plist = ::H5Pcreate(H5P_DATASET_XFER);
    errCheck(access_plist, "H5Pcreate");
#if defined(MPI_SESSION)
    errCheck(::H5Pset_dxpl_mpio(access_plist, H5FD_MPIO_COLLECTIVE), "H5Pset_dxpl_mpio");
#endif
    errCheck(::H5Dwrite(dataset, dataType, H5S_ALL, H5S_ALL, access_plist, &scalar), "H5Dwrite");

    errCheck(::H5Pclose(access_plist), "H5Pclose");
    errCheck(::H5Sclose(fspace), "H5Sclose");
    errCheck(::H5Dclose(dataset), "H5Dclose");
}

template <dim_t dim>
void WriterSliceHDF5_C<dim>::writeScalar(Int scalar, const std::string& name, hid_t file) const
{
    hid_t fspace = ::H5Screate(H5S_SCALAR);
    errCheck(fspace, "H5Screate");
    hid_t dataset = ::
            H5Dcreate(file, name.c_str(), dataType, fspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    errCheck(dataset, "H5Dcreate");
    hid_t access_plist = ::H5Pcreate(H5P_DATASET_XFER);
    errCheck(access_plist, "H5Pcreate");
#if defined(MPI_SESSION)
    errCheck(::H5Pset_dxpl_mpio(access_plist, H5FD_MPIO_COLLECTIVE), "H5Pset_dxpl_mpio");
#endif
    hid_t scalar_datatype = getHdf5DataType<Int>();
    errCheck(
            ::H5Dwrite(dataset, scalar_datatype, H5S_ALL, H5S_ALL, access_plist, &scalar),
            "H5Dwrite");

    errCheck(::H5Pclose(access_plist), "H5Pclose");
    errCheck(::H5Sclose(fspace), "H5Sclose");
    errCheck(::H5Dclose(dataset), "H5Dclose");
}

template <dim_t dim>
void WriterSliceHDF5_C<dim>::writeXML(const UniformGrid<dim>& grid) const
{
    const auto& restartId = IWriter<dim>::m_restartId;
    std::ostringstream restartNum;
    restartNum << std::setw(std::numeric_limits<Int>::digits10);
    restartNum << std::setfill('0');
    restartNum << restartId;

    const std::string xdmfFilenameFull {
            directory + '/' + prefix + "_slice_" + restartNum.str() + ".xmf"};
    std::ofstream xdmfFile(xdmfFilenameFull, std::ofstream::trunc);

    xdmfFile << "<?xml version=\"1.0\"?>\n";
    xdmfFile << "<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>\n";
    xdmfFile << "<Xdmf Version=\"2.0\">\n";
    xdmfFile << std::string(2, ' ') << "<Domain>\n";
    xdmfFile << std::string(4, ' ');
    xdmfFile << "<Grid";
    xdmfFile << " Name=" << '"' << "TimeSeries" << '"';
    xdmfFile << " GridType=" << '"' << "Collection" << '"';
    xdmfFile << " CollectionType=" << '"' << "Temporal" << '"';
    xdmfFile << ">\n";

    Int nbCells {1};
    for (int idim = 0; idim < dim; ++idim)
    {
        nbCells *= grid.m_nbCells[idim];
    }

    for (const auto& previous_output : IWriter<dim>::m_previous_outputs)
    {
        xdmfFile << std::string(6, ' ');
        xdmfFile << "<Grid Name=" << '"' << "output" << '"';
        xdmfFile << " GridType=" << '"' << "Uniform" << '"';
        xdmfFile << ">\n";
        xdmfFile << std::string(8, ' ');
        xdmfFile << "<Time Value=" << '"' << previous_output.second << '"' << "/>\n";

        // topology CoRectMesh
        xdmfFile << std::string(8, ' ');
        xdmfFile << "<Topology";
        xdmfFile << " TopologyType=" << '"' << "3DCoRectMesh" << '"';
        xdmfFile << " Dimensions=" << '"';
        for (int idim = 2; idim >= 0; --idim)
        {
            xdmfFile
                    << ((idim < dim && idim != m_idir_slice)
                                ? grid.m_dom[idim] * grid.m_nbCells[idim] + 1
                                : 2);
            xdmfFile << (idim == 0 ? "\"" : " ");
        }
        xdmfFile << "/>\n";

        // geometry
        xdmfFile << std::string(8, ' ');
        xdmfFile << "<Geometry";
        xdmfFile << " GeometryType=" << '"' << "ORIGIN_DXDYDZ" << '"';
        xdmfFile << ">\n";

        xdmfFile << std::string(10, ' ');
        xdmfFile << "<DataItem";
        xdmfFile << " Name=" << '"' << "Origin" << '"';
        xdmfFile << " NumberType=" << '"' << "Float" << '"';
        xdmfFile << " Precision=" << '"' << precision << '"';
        xdmfFile << " Dimensions=" << '"' << 3 << '"';
        xdmfFile << " Format=" << '"' << "XML" << '"';
        xdmfFile << ">\n";
        xdmfFile << std::string(12, ' ');
        for (int idim = 2; idim >= 0; --idim)
        {
            xdmfFile
                    << ((idim < dim && idim != m_idir_slice) ? grid.m_lowGlobal[idim]
                                                             : constants::zero);
            xdmfFile << (idim == 0 ? "\n" : " ");
        }
        xdmfFile << std::string(10, ' ') << "</DataItem>\n";

        xdmfFile << std::string(10, ' ');
        xdmfFile << "<DataItem";
        xdmfFile << " Name=" << '"' << "Spacing" << '"';
        xdmfFile << " NumberType=" << '"' << "Float" << '"';
        xdmfFile << " Precision=" << '"' << precision << '"';
        xdmfFile << " Dimensions=" << '"' << 3 << '"';
        xdmfFile << " Format=" << '"' << "XML" << '"';
        xdmfFile << ">\n";
        xdmfFile << std::string(12, ' ');
        for (int idim = 2; idim >= 0; --idim)
        {
            xdmfFile << ((idim < dim && idim != m_idir_slice) ? grid.m_dl[idim] : constants::zero);
            xdmfFile << (idim == 0 ? "\n" : " ");
        }
        xdmfFile << std::string(10, ' ') << "</DataItem>\n";

        xdmfFile << std::string(8, ' ') << "</Geometry>\n";

        // Write gamma
        xdmfFile << std::string(8, ' ');
        xdmfFile << "<Attribute";
        xdmfFile << " Center=" << '"' << "Grid" << '"';
        xdmfFile << " Name=" << '"' << "gamma" << '"';
        xdmfFile << " AttributeType=" << '"' << "Scalar" << '"';
        xdmfFile << ">\n";
        xdmfFile << std::string(10, ' ');
        xdmfFile << "<DataItem";
        xdmfFile << " NumberType=" << '"' << "Float" << '"';
        xdmfFile << " Precision=" << '"' << precision << '"';
        xdmfFile << " Dimensions=" << '"' << 1 << '"';
        xdmfFile << " Format=" << '"' << "HDF" << '"';
        xdmfFile << ">\n";
        xdmfFile << std::string(12, ' ') << getFilename(previous_output.first) << ":/"
                 << "gamma"
                 << "\n";
        xdmfFile << std::string(10, ' ') << "</DataItem>\n";
        xdmfFile << std::string(8, ' ') << "</Attribute>\n";
        // Write mmw
        xdmfFile << std::string(8, ' ');
        xdmfFile << "<Attribute";
        xdmfFile << " Center=" << '"' << "Grid" << '"';
        xdmfFile << " Name=" << '"' << "mmw" << '"';
        xdmfFile << " AttributeType=" << '"' << "Scalar" << '"';
        xdmfFile << ">\n";
        xdmfFile << std::string(10, ' ');
        xdmfFile << "<DataItem";
        xdmfFile << " NumberType=" << '"' << "Float" << '"';
        xdmfFile << " Precision=" << '"' << precision << '"';
        xdmfFile << " Dimensions=" << '"' << 1 << '"';
        xdmfFile << " Format=" << '"' << "HDF" << '"';
        xdmfFile << ">\n";
        xdmfFile << std::string(12, ' ') << getFilename(previous_output.first) << ":/"
                 << "mmw"
                 << "\n";
        xdmfFile << std::string(10, ' ') << "</DataItem>\n";
        xdmfFile << std::string(8, ' ') << "</Attribute>\n";
        // Write Rstar_h
        xdmfFile << std::string(8, ' ');
        xdmfFile << "<Attribute";
        xdmfFile << " Center=" << '"' << "Grid" << '"';
        xdmfFile << " Name=" << '"' << "Rstar_h" << '"';
        xdmfFile << " AttributeType=" << '"' << "Scalar" << '"';
        xdmfFile << ">\n";
        xdmfFile << std::string(10, ' ');
        xdmfFile << "<DataItem";
        xdmfFile << " NumberType=" << '"' << "Float" << '"';
        xdmfFile << " Precision=" << '"' << precision << '"';
        xdmfFile << " Dimensions=" << '"' << 1 << '"';
        xdmfFile << " Format=" << '"' << "HDF" << '"';
        xdmfFile << ">\n";
        xdmfFile << std::string(12, ' ') << getFilename(previous_output.first) << ":/"
                 << "Rstar_h"
                 << "\n";
        xdmfFile << std::string(10, ' ') << "</DataItem>\n";
        xdmfFile << std::string(8, ' ') << "</Attribute>\n";

        for (int ivar = 0; ivar < dim + 2; ++ivar)
        {
            xdmfFile << std::string(8, ' ');
            xdmfFile << "<Attribute";
            xdmfFile << " Center=" << '"' << "Cell" << '"';
            xdmfFile << " Name=" << '"' << varNames[ivar] << '"';
            xdmfFile << " AttributeType=" << '"' << "Scalar" << '"';
            xdmfFile << ">\n";
            xdmfFile << std::string(10, ' ');
            xdmfFile << "<DataItem";
            xdmfFile << " NumberType=" << '"' << "Float" << '"';
            xdmfFile << " Precision=" << '"' << precision << '"';

            xdmfFile << " Dimensions=\"";
            for (int idim = 2; idim >= 0; --idim)
            {
                xdmfFile
                        << ((idim < dim && idim != m_idir_slice)
                                    ? grid.m_dom[idim] * grid.m_nbCells[idim]
                                    : 1);
                xdmfFile << (idim == 0 ? "\"" : " ");
            }

            xdmfFile << " Format=" << '"' << "HDF" << '"';
            xdmfFile << ">\n";
            xdmfFile << std::string(12, ' ') << getFilename(previous_output.first) << ":/"
                     << varNames[ivar] << "\n";
            xdmfFile << std::string(10, ' ') << "</DataItem>\n";
            xdmfFile << std::string(8, ' ') << "</Attribute>\n";
        }
        // finalize grid file for the current time step
        xdmfFile << std::string(6, ' ') << "</Grid>\n";
    }

    // finalize Xdmf wrapper file
    xdmfFile << std::string(4, ' ') << "</Grid>\n";
    xdmfFile << std::string(2, ' ') << "</Domain>\n";
    xdmfFile << std::string(0, ' ') << "</Xdmf>\n";
}

template class WriterSliceHDF5_C<one_d>;
template class WriterSliceHDF5_C<two_d>;
template class WriterSliceHDF5_C<three_d>;

} // namespace ark::io
