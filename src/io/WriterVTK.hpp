#pragma once

#include <array>
#include <list>
#include <string>
#include <utility>
#include <vector>

#include "IWriter.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"
#include "Units.hpp"
#include "Utils.hpp"

namespace ark::io
{

template <dim_t dim>
class WriterVTK : public IWriter<dim>
{
public:
    WriterVTK() = default;

    WriterVTK(
            const UniformGrid<dim>& grid,
            const Params<dim>& params,
            const std::string& prefix,
            const std::vector<std::pair<int, std::string>>& variables);

    WriterVTK(const WriterVTK& x) = default;

    WriterVTK(WriterVTK&& x) = default;

    ~WriterVTK() override = default;

    WriterVTK& operator=(const WriterVTK& x) = default;

    WriterVTK& operator=(WriterVTK&& x) = default;

    void write(
            HostConstArrayDyn u,
            const UniformGrid<dim>& grid,
            Int iStep,
            Real time,
            Real gamma,
            Real mmw) final;

    void writeVtiAscii(
            HostConstArrayDyn u,
            const UniformGrid<dim>& grid,
            Int iStep,
            Real time,
            Real gamma,
            Real mmw) const;

    void writeVtiAppended(
            HostConstArrayDyn u,
            const UniformGrid<dim>& grid,
            Int iStep,
            Real time,
            Real gamma,
            Real mmw) const;

    void writePvd() const;

    std::string getFilename(Int iStep) const;

#if defined(MPI_SESSION)
    void writePvti(const UniformGrid<dim>& grid, Int iStep) const;

    std::string getFilename(Int iStep, int iPiece) const;
#endif

private:
    std::string format;
    std::string directory;
    std::string m_prefix;
    std::string endian_type;
    std::string header_type;
    std::string float_type;
    std::string xml_version;
    std::string vtk_version;
    std::array<std::array<Int, 3>, 2> p_ext;
    std::array<std::array<Int, 3>, 2> w_ext;
    std::array<Real, 3> origin;
    std::array<Real, 3> dl;
    std::vector<std::pair<int, std::string>> m_variables;
};

extern template class WriterVTK<one_d>;
extern template class WriterVTK<two_d>;
extern template class WriterVTK<three_d>;

} // namespace ark::io
