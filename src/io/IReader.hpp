#pragma once

#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark::io
{

template <dim_t dim>
class IReader
{
public:
    IReader() = default;

    IReader(const IReader& x) = default;

    IReader(IReader&& x) = default;

    virtual ~IReader() = default;

    IReader& operator=(const IReader& x) = default;

    IReader& operator=(IReader&& x) = default;

    virtual void read(
            HostArrayDyn u,
            const UniformGrid<dim>& grid,
            Int& iStep,
            Real& time,
            Int& outputId,
            Int& restartId)
            = 0;
};

} // namespace ark::io
