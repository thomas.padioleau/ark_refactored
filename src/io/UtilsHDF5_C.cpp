#include <hdf5.h>

#include "UtilsHDF5_C.hpp"

namespace utils
{

// Floating point types
template <>
hid_t getHdf5DataType<float>()
{
    return H5T_NATIVE_FLOAT;
}

template <>
hid_t getHdf5DataType<double>()
{
    return H5T_NATIVE_DOUBLE;
}

template <>
hid_t getHdf5DataType<long double>()
{
    return H5T_NATIVE_LDOUBLE;
}

// Integer types
template <>
hid_t getHdf5DataType<char>()
{
    return H5T_NATIVE_CHAR;
}

template <>
hid_t getHdf5DataType<int>()
{
    return H5T_NATIVE_INT;
}

template <>
hid_t getHdf5DataType<long int>()
{
    return H5T_NATIVE_LONG;
}

template <>
hid_t getHdf5DataType<long long int>()
{
    return H5T_NATIVE_LLONG;
}

} // namespace utils
