#pragma once

#include <iostream>
#include <stdexcept>
#include <string>

#include <hdf5.h>

namespace utils
{

template <class T>
hid_t getHdf5DataType();

template <class Integer>
void errCheck(Integer status, const std::string& fname)
{
    if (status < 0)
    {
        throw std::runtime_error(fname + " returned error code " + std::to_string(status));
    }
}

} // namespace utils
