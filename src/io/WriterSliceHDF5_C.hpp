#pragma once

#include <string>
#include <vector>

#include <hdf5.h>

#include "IWriter.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"
#include "Units.hpp"

namespace ark::io
{

template <dim_t dim>
class WriterSliceHDF5_C : public IWriter<dim>
{
public:
    WriterSliceHDF5_C() = default;

    WriterSliceHDF5_C(const UniformGrid<dim>& grid, int idir, const Params<dim>& params);

    WriterSliceHDF5_C(const WriterSliceHDF5_C& x) = default;

    WriterSliceHDF5_C(WriterSliceHDF5_C&& x) = default;

    ~WriterSliceHDF5_C();

    WriterSliceHDF5_C& operator=(const WriterSliceHDF5_C& x) = default;

    WriterSliceHDF5_C& operator=(WriterSliceHDF5_C&& x) = default;

    void write(
            HostConstArrayDyn u,
            const UniformGrid<dim>& grid,
            Int iStep,
            Real time,
            Real gamma,
            Real mmw) final;

    void writeScalar(Real scalar, const std::string& name, hid_t file) const;

    void writeScalar(Int scalar, const std::string& name, hid_t file) const;

    void writeXML(const UniformGrid<dim>& grid) const;

    void copy_to_buffer(
            std::vector<Real>& data,
            HostConstArrayDyn u,
            const UniformGrid<dim>& grid,
            int var) const;

    std::string getFilename(Int iStep) const;

private:
    std::string directory;
    std::string prefix;
    std::vector<std::string> varNames;
    hid_t dataType;
    int precision;
    int m_idir_slice;
    int m_local_slice_index;
#if defined(MPI_SESSION)
    ::MPI_Comm m_comm_slice;
#endif // MPI_SESSION
}; // class WriterSliceHDF5_C

extern template class WriterSliceHDF5_C<one_d>;
extern template class WriterSliceHDF5_C<two_d>;
extern template class WriterSliceHDF5_C<three_d>;

} // namespace ark::io
