#pragma once

#include <list>
#include <utility>

#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark::io
{

template <dim_t dim>
class IWriter
{
public:
    IWriter() = default;

    IWriter(const IWriter& x) = default;

    IWriter(IWriter&& x) = default;

    virtual ~IWriter() = default;

    IWriter& operator=(const IWriter& x) = default;

    IWriter& operator=(IWriter&& x) = default;

    virtual void write(
            HostConstArrayDyn u,
            const UniformGrid<dim>& grid,
            Int iStep,
            Real time,
            Real gamma,
            Real mmw)
            = 0;

    void setOutputId(Int id)
    {
        m_outputId = id;
    }

    void setRestartId(Int id)
    {
        m_restartId = id;
    }

protected:
    Int m_outputId = 0;
    Int m_restartId = 0;
    std::list<std::pair<Int, Real>> m_previous_outputs = {};
};

} // namespace ark::io
