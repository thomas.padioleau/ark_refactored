#include "IReader.hpp"
#include "Params.hpp"
#include "Reader.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"
#if defined(Ark_ENABLE_HDF5_C)
#include "ReaderHDF5_C.hpp"
#endif

#include <list>
#include <memory>
#include <utility>
#include <vector>

namespace ark::io
{

#if defined(Ark_ENABLE_HDF5_C)
template <dim_t dim>
Reader<dim>::Reader(
        const UniformGrid<dim>& grid,
        const Params<dim>& params,
        const std::vector<std::pair<int, std::string>>& variables)
{
    io_reader = std::make_shared<ReaderHDF5_C<dim>>(grid, params, variables);
}

template <dim_t dim>
void Reader<dim>::read(
        HostArrayDyn u,
        const UniformGrid<dim>& grid,
        Int& iStep,
        Real& time,
        Int& outputId,
        Int& restartId) const
{
    io_reader->read(u, grid, iStep, time, outputId, restartId);
}

#else

template <dim_t dim>
Reader<dim>::Reader(
        const UniformGrid<dim>&,
        const Params<dim>&,
        const std::vector<std::pair<int, std::string>>&)
{
}

template <dim_t dim>
void Reader<dim>::read(HostArrayDyn, const UniformGrid<dim>&, Int&, Real&, Int&, Int&) const
{
}
#endif

template class Reader<one_d>;
template class Reader<two_d>;
template class Reader<three_d>;

} // namespace ark::io
