#pragma once

#include <list>
#include <memory>
#include <vector>

#include "IWriter.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark::io
{

template <dim_t dim>
struct WriterFactory
{
    static std::shared_ptr<IWriter<dim>> New(
            const UniformGrid<dim>& grid,
            const Params<dim>& params,
            const std::string& type,
            const std::string& prefix,
            const std::vector<std::pair<int, std::string>>& variables);
};

extern template struct WriterFactory<one_d>;
extern template struct WriterFactory<two_d>;
extern template struct WriterFactory<three_d>;

} // namespace ark::io
