#pragma once

#include <list>
#include <string>
#include <utility>
#include <vector>

#include <hdf5.h>

#include "IReader.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"
#include "Units.hpp"

namespace ark::io
{

template <dim_t dim>
class ReaderHDF5_C : public IReader<dim>
{
public:
    ReaderHDF5_C() = default;

    ReaderHDF5_C(
            const UniformGrid<dim>& grid,
            const Params<dim>& params,
            const std::vector<std::pair<int, std::string>>& variables);

    ReaderHDF5_C(const ReaderHDF5_C& x) = default;

    ReaderHDF5_C(ReaderHDF5_C&& x) = default;

    ~ReaderHDF5_C() override = default;

    ReaderHDF5_C& operator=(const ReaderHDF5_C& x) = default;

    ReaderHDF5_C& operator=(ReaderHDF5_C&& x) = default;

    void read(
            HostArrayDyn u,
            const UniformGrid<dim>& grid,
            Int& iStep,
            Real& time,
            Int& outputId,
            Int& restartId) final;

    void readScalar(Real& scalar, const std::string& name, hid_t file) const;

    void readScalar(Int& scalar, const std::string& name, hid_t file) const;

    void copy_buffer(std::vector<Real>& data, HostArrayDyn u, const UniformGrid<dim>& grid, int var)
            const;

private:
    std::string filename;
    std::vector<std::pair<int, std::string>> m_variables;
    hid_t mdatatype;
    int precision;
}; // class ReaderHDF5_C

extern template class ReaderHDF5_C<one_d>;
extern template class ReaderHDF5_C<two_d>;
extern template class ReaderHDF5_C<three_d>;

} // namespace ark::io
