#pragma once

#include "Constants.hpp"
#include "FillingCurve.hpp"
#include "Types.hpp"
#if defined(MPI_SESSION)
#include "MpiCommCart.hpp"
#endif

namespace ark
{

template <dim_t dim>
class UniformGrid : public StridedFillingCurve<dim>
{
    using Super = StridedFillingCurve<dim>;

public:
    UniformGrid(
            const RealVector<dim>& low,
            const RealVector<dim>& up,
            const IntVector<dim>& nbCells,
            const IntVector<dim>& dom,
            const IntVector<dim>& ghostWidths);

    UniformGrid(
            const RealVector<dim>& low,
            const RealVector<dim>& up,
            const IntVector<dim>& nbCells,
            const IntVector<dim>& dom,
            Int ghostWidth);

    UniformGrid(const UniformGrid& x) = default;

    UniformGrid(UniformGrid&& x) noexcept = default;

    ~UniformGrid() = default;

    UniformGrid& operator=(const UniformGrid& x) = default;

    UniformGrid& operator=(UniformGrid&& x) noexcept = default;

    StridedFillingCurve<dim>& curve();

    StridedFillingCurve<dim> curve() const;

    KOKKOS_INLINE_FUNCTION
    RealVector<dim> getCellCenter(const IntVector<dim>& coords) const;

    KOKKOS_INLINE_FUNCTION
    RealVector<dim> getCellCenter(Int j) const;

    KOKKOS_INLINE_FUNCTION
    Real getdSdV(Int j, int iface) const;

    template <int idir, int iside>
    KOKKOS_INLINE_FUNCTION Real
            getdSdV(Int,
                    std::integral_constant<int, idir>,
                    std::integral_constant<int, iside>) const;

    KOKKOS_INLINE_FUNCTION
    Real dl(int idim) const;

    KOKKOS_INLINE_FUNCTION
    Real lo(int idim) const;

    KOKKOS_INLINE_FUNCTION
    Real hi(int idim) const;

    RealVector<dim> m_upGlobal;
    RealVector<dim> m_lowGlobal;
    RealVector<dim> m_up;
    RealVector<dim> m_low;
    IntVector<dim> m_dom;
    RealVector<dim> m_dl;
    RealVector<dim> m_invdl;
#if defined(MPI_SESSION)
    distributed_memory_session::MpiCommCart<dim> comm;
#endif
};

template <dim_t dim>
UniformGrid<dim>::UniformGrid(
        const RealVector<dim>& lowGlobal,
        const RealVector<dim>& upGlobal,
        const IntVector<dim>& nbCells,
        const IntVector<dim>& dom,
        const IntVector<dim>& ghostWidths)
    : Super {nbCells, ghostWidths}
    , m_upGlobal {upGlobal}
    , m_lowGlobal {lowGlobal}
    , m_up {upGlobal}
    , m_low {lowGlobal}
    , m_dom {dom}
    , m_dl {}
    , m_invdl
{
}
#if defined(MPI_SESSION)
, comm {}
#endif
{
#if defined(MPI_SESSION)
    std::array<int, dim> dom2;
    for (int i = 0; i < dim; ++i)
    {
        dom2[i] = static_cast<int>(dom[i]);
    }
    comm = distributed_memory_session::MpiCommCart<dim> {dom2, true, true};
    std::array<int, dim> CartCoords {comm.getCoords(comm.rank())};
    for (int idim = 0; idim < dim; ++idim)
    {
        const Real length_loc {(m_up[idim] - m_low[idim]) / static_cast<Real>(dom[idim])};
        m_low[idim] = m_lowGlobal[idim] + length_loc * static_cast<Real>(CartCoords[idim]);
        m_up[idim] = m_low[idim] + length_loc;
    }
#endif
    for (int idim = 0; idim < dim; ++idim)
    {
        m_dl[idim] = (m_up[idim] - m_low[idim]) / static_cast<Real>(nbCells[idim]);
        m_invdl[idim] = constants::one / m_dl[idim];
    }
}

template <dim_t dim>
UniformGrid<dim>::UniformGrid(
        const RealVector<dim>& lowGlobal,
        const RealVector<dim>& upGlobal,
        const IntVector<dim>& nbCells,
        const IntVector<dim>& dom,
        Int ghostWidth)
    : Super(nbCells, ghostWidth)
    , m_upGlobal(upGlobal)
    , m_lowGlobal(lowGlobal)
    , m_up(upGlobal)
    , m_low(lowGlobal)
    , m_dom(dom)
    , m_dl()
    , m_invdl()
#if defined(MPI_SESSION)
    , comm()
#endif
{
#if defined(MPI_SESSION)
    std::array<int, dim> dom2;
    for (int i = 0; i < dim; ++i)
    {
        dom2[i] = static_cast<int>(dom[i]);
    }
    comm = distributed_memory_session::MpiCommCart<dim> {dom2, true, true};
    std::array<int, dim> CartCoords {comm.getCoords(comm.rank())};
    for (int idim = 0; idim < dim; ++idim)
    {
        const Real length_loc {(m_up[idim] - m_low[idim]) / static_cast<Real>(dom[idim])};
        m_low[idim] = m_lowGlobal[idim] + length_loc * static_cast<Real>(CartCoords[idim]);
        m_up[idim] = m_low[idim] + length_loc;
    }
#endif
    for (int idim = 0; idim < dim; ++idim)
    {
        m_dl[idim] = (m_up[idim] - m_low[idim]) / static_cast<Real>(nbCells[idim]);
        m_invdl[idim] = constants::one / m_dl[idim];
    }
}

template <dim_t dim>
inline StridedFillingCurve<dim>& UniformGrid<dim>::curve()
{
    return *this;
}

template <dim_t dim>
inline StridedFillingCurve<dim> UniformGrid<dim>::curve() const
{
    return *this;
}

template <dim_t dim>
KOKKOS_INLINE_FUNCTION RealVector<dim> UniformGrid<dim>::getCellCenter(
        const IntVector<dim>& coords) const
{
    using constants::half;

    RealVector<dim> x {m_low};
    for (int idim = 0; idim < dim; ++idim)
    {
        x[idim] += (half + static_cast<Real>(coords[idim] - Super::m_ghostWidths[idim]))
                   * m_dl[idim];
    }
    return x;
}

template <dim_t dim>
KOKKOS_INLINE_FUNCTION RealVector<dim> UniformGrid<dim>::getCellCenter(Int j) const
{
    return getCellCenter(Super::indexToCoord(j));
}

template <dim_t dim>
KOKKOS_INLINE_FUNCTION Real UniformGrid<dim>::getdSdV(Int, int iface) const
{
    return m_invdl[iface / 2];
}

template <dim_t dim>
template <int idir, int iside>
KOKKOS_INLINE_FUNCTION Real UniformGrid<dim>::getdSdV(
        Int,
        std::integral_constant<int, idir>,
        std::integral_constant<int, iside>) const
{
    return m_invdl[idir];
}

template <dim_t dim>
KOKKOS_INLINE_FUNCTION Real UniformGrid<dim>::dl(int idim) const
{
    return m_dl[idim];
}

template <dim_t dim>
KOKKOS_INLINE_FUNCTION Real UniformGrid<dim>::lo(int idim) const
{
    return m_low[idim];
}

template <dim_t dim>
KOKKOS_INLINE_FUNCTION Real UniformGrid<dim>::hi(int idim) const
{
    return m_up[idim];
}

} // namespace ark
