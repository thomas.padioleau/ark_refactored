#pragma once

#include <cmath>

#include "BaseKernel.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "RayleighTaylorParams.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{
namespace problems
{

class RayleighTaylorInitKernel : BaseKernel<two_d>
{
    using Super = BaseKernel<two_d>;

    using Euler = EulerSystem<two_d>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;
    using VC = typename Euler::VarCons;
    using VP = typename Euler::VarPrim;

public:
    RayleighTaylorInitKernel(
            const Params<two_d>& params,
            const RayleighTaylorParams& pParams,
            RealArray<two_d> u,
            const UniformGrid<two_d>& grid)
        : m_params(params)
        , m_problem_params(pParams)
        , m_eos(params.thermo)
        , m_u(u)
        , m_grid(grid)
    {
    }

    KOKKOS_INLINE_FUNCTION
    Real phi(Int index) const
    {
        const RealVector<two_d> OX = m_grid.getCellCenter(index);
        Real phi = constants::zero;
        for (Int idim = 0; idim < two_d; ++idim)
        {
            phi -= m_params.hydro.g[idim] * OX[idim];
        }
        return phi;
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(Int ix) const
    {
        using namespace constants;

        for (Int iy = m_grid.m_ghostWidths[IY];
             iy < m_grid.m_nbCells[IY] + m_grid.m_ghostWidths[IY];
             ++iy)
        {
            const IntVector<two_d> coord = {ix, iy};
            const Int j = m_grid.coordToIndex(coord);
            const IntVector<two_d> coord_k = {ix, iy - 1};
            const Int k = m_grid.coordToIndex(coord_k);
            const ConsState u_k = getCons(m_u, k);
            const PrimState q_k = Euler::conservativeToPrimitive(u_k, m_eos);
            const RealVector<two_d> OX = m_grid.getCellCenter(coord);
            const Real Lx = m_params.mesh.up[IX] - m_params.mesh.low[IX];
            const Real Ly = m_params.mesh.up[IY] - m_params.mesh.low[IY];

            PrimState q_j;
            q_j.d = OX[IY] <= zero ? m_problem_params.density_bottom : m_problem_params.density_up;
            q_j.p = iy == m_grid.m_ghostWidths[IY]
                            ? m_problem_params.pressure_bottom
                            : q_k.p + half * (q_k.d + q_j.d) * (phi(k) - phi(j));
            q_j.v(IX) = zero;
            q_j.v(IY) = m_problem_params.perturbation * (one + std::cos(pi_2 * OX[IX] / Lx))
                        * (one + std::cos(pi_2 * OX[IY] / Ly)) / four;

            const ConsState u_j = Euler::primitiveToConservative(q_j, m_eos);
            set(m_u, j, u_j);
        }
    }

    Params<two_d> m_params;
    RayleighTaylorParams m_problem_params;
    EquationOfState m_eos;
    RealArray<two_d> m_u;
    UniformGrid<two_d> m_grid;
};

} // namespace problems
} // namespace ark
