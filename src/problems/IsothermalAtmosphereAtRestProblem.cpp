#include <memory>

#include "IsothermalAtmosphereAtRestBoundariesKernel.hpp"
#include "IsothermalAtmosphereAtRestInitKernel.hpp"
#include "IsothermalAtmosphereAtRestParams.hpp"
#include "IsothermalAtmosphereAtRestProblem.hpp"
#include "Params.hpp"
#include "Print.hpp"
#include "Problem.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{
namespace problems
{

IsothermalAtmosphereAtRestProblem::IsothermalAtmosphereAtRestProblem(
        const std::shared_ptr<Params<one_d>>& params)
    : Problem<one_d>(params)
    , m_prob_params(std::make_shared<IsothermalAtmosphereAtRestParams>(params->reader))
{
}

void IsothermalAtmosphereAtRestProblem::initialize(
        const RealArray<one_d>& u,
        const UniformGrid<one_d>& grid) const
{
    Print() << "Initializing Isothermal atmosphere at equilibrium problem... ";

    IsothermalAtmosphereAtRestInitKernel kernel(*m_params, *m_prob_params, u, grid);
    Kokkos::RangePolicy<Int> range(0, 1);
    Kokkos::parallel_for(
            "Isothermal atmosphere initialization kernel - RangePolicy",
            range,
            kernel);

    Print() << "done" << std::endl;
}

void IsothermalAtmosphereAtRestProblem::make_boundaries_user(
        const RealArray<one_d>& u,
        const UniformGrid<one_d>& grid,
        int,
        int iside)
{
    IsothermalAtmosphereAtRestBoundariesKernel kernel(u, grid, *m_params, *m_prob_params);
    if (iside == 0)
    {
        Kokkos::RangePolicy<IsothermalAtmosphereAtRestBoundariesKernel::downTag> range(0, 1);
        Kokkos::parallel_for(range, kernel);
    }
    else
    {
        Kokkos::RangePolicy<IsothermalAtmosphereAtRestBoundariesKernel::upTag> range(0, 1);
        Kokkos::parallel_for(range, kernel);
    }
}

} // namespace problems
} // namespace ark
