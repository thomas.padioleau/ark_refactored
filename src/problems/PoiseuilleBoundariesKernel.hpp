#pragma once

#include "BaseKernel.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "PoiseuilleParams.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

namespace problems
{

class PoiseuilleBoundariesKernel : public BaseKernel<three_d>
{
public:
    using Super = BaseKernel<three_d>;

    using Euler = EulerSystem<three_d>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;
    using VC = typename Euler::VarCons;
    using VP = typename Euler::VarPrim;

    PoiseuilleBoundariesKernel(
            const Params<three_d>& params,
            const PoiseuilleParams& prob_params,
            const RealArray<three_d>& u,
            const UniformGrid<three_d>& grid,
            int idim,
            int)
        : m_u(u)
        , m_eos(params.thermo)
        , m_grid(grid)
        , m_idim(idim)
        , m_flow_dir(prob_params.flow_dir)
        , m_pGradient(prob_params.p_gradient)
        , m_p0(prob_params.p0)
    {
        if (m_idim != m_flow_dir)
        {
            std::clog << "WARNING you are likely using wrong boundary types\n";
        }
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(Tags::SideL_t, const Int i, const Int j, const Int k) const
    {
        const int index = m_grid.coordToIndex({i, j, k});

        IntVector<three_d> coord0 = {i, j, k};
        coord0[m_idim] = m_grid.m_ghostWidths[m_idim];
        const int index0 = m_grid.coordToIndex(coord0);
        const ConsState u_j0 = Super::getCons(m_u, index0);

        PrimState q_j = Euler::conservativeToPrimitive(u_j0, m_eos);
        q_j.p = m_p0;

        const ConsState u_j = Euler::primitiveToConservative(q_j, m_eos);
        Super::set(m_u, index, u_j);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(Tags::SideR_t, const Int i, const Int j, const Int k) const
    {
        const int index = m_grid.coordToIndex({i, j, k});

        IntVector<three_d> coord0 = {i, j, k};
        coord0[m_idim] = m_grid.m_nbCells[m_idim] + m_grid.m_ghostWidths[m_idim] - 1;
        const int index0 = m_grid.coordToIndex(coord0);
        const ConsState u_j0 = Super::getCons(m_u, index0);
        const RealVector<three_d> OX0 = m_grid.getCellCenter(coord0);

        const RealVector<three_d> OX = m_grid.getCellCenter(index);

        PrimState q_j = Euler::conservativeToPrimitive(u_j0, m_eos);
        q_j.p += (OX[m_flow_dir] - OX0[m_flow_dir]) * m_pGradient;

        const ConsState u_j = Euler::primitiveToConservative(q_j, m_eos);
        Super::set(m_u, index, u_j);
    }

    RealArray<three_d> m_u;
    EquationOfState m_eos;
    UniformGrid<three_d> m_grid;
    int m_idim;
    int m_flow_dir;
    Real m_pGradient;
    Real m_p0;
};

} // namespace problems

} // namespace ark
