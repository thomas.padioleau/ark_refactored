#include <memory>

#include "GreshoInitKernel.hpp"
#include "GreshoParams.hpp"
#include "GreshoProblem.hpp"
#include "Params.hpp"
#include "Print.hpp"

namespace ark
{
namespace problems
{

GreshoProblem::GreshoProblem(const std::shared_ptr<Params<two_d>>& params)
    : Problem<two_d>(params)
    , m_problem_params(std::make_shared<GreshoParams>(params->reader))
{
}

void GreshoProblem::initialize(const RealArray<two_d>& u, const UniformGrid<two_d>& grid) const
{
    Print() << "Initializing Gresho problem... ";

    GreshoInitKernel kernel(*m_params, *m_problem_params, u, grid);
    Kokkos::RangePolicy<Int> range(0, grid.nbCells());
    Kokkos::parallel_for("Gresho initialization kernel - RangePolicy", range, kernel);

    Print() << "done\n";
}

} // namespace problems
} // namespace ark
