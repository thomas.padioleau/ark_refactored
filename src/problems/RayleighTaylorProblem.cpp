#include <memory>

#include "Print.hpp"
#include "RayleighTaylorBoundariesKernel.hpp"
#include "RayleighTaylorInitKernel.hpp"
#include "RayleighTaylorProblem.hpp"

namespace ark
{
namespace problems
{

RayleighTaylorProblem::RayleighTaylorProblem(const std::shared_ptr<Params<two_d>>& params)
    : Problem<two_d>(params)
    , m_problem_params(std::make_shared<RayleighTaylorParams>(params->reader))
{
}

void RayleighTaylorProblem::initialize(const RealArray<two_d>& u, const UniformGrid<two_d>& grid)
        const
{
    Print() << "Initializing Rayleigh-Taylor problem... ";

    RayleighTaylorInitKernel kernel(*m_params, *m_problem_params, u, grid);
    Kokkos::RangePolicy<Int> range(0, grid.m_nbCells[IX] + 2 * grid.m_ghostWidths[IX]);
    Kokkos::parallel_for("Rayleigh-Taylor initialization kernel - RangePolicy", range, kernel);

    Print() << "done\n";
}

void RayleighTaylorProblem::make_boundaries_user(
        const RealArray<two_d>& u,
        const UniformGrid<two_d>& grid,
        int,
        int iside)
{
    RayleighTaylorBoundariesKernel kernel(u, grid, *m_params);
    if (iside == 0)
    {
        Kokkos::RangePolicy<Int, RayleighTaylorBoundariesKernel::downTag>
                range(0, grid.m_nbCells[IX] + 2 * grid.m_ghostWidths[IX]);
        Kokkos::parallel_for(range, kernel);
    }
    else
    {
        Kokkos::RangePolicy<Int, RayleighTaylorBoundariesKernel::upTag>
                range(0, grid.m_nbCells[IX] + 2 * grid.m_ghostWidths[IX]);
        Kokkos::parallel_for(range, kernel);
    }
}

} // namespace problems
} // namespace ark
