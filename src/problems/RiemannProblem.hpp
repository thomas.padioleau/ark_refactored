#pragma once

#include <memory>

#include "Params.hpp"
#include "Problem.hpp"
#include "RiemannParams.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{
namespace problems
{

template <dim_t dim>
struct RiemannProblem : Problem<dim>
{
    RiemannProblem(const std::shared_ptr<Params<dim>>& params);

    void initialize(const RealArray<dim>& u, const UniformGrid<dim>& grid) const final;

    std::shared_ptr<RiemannParams<dim>> m_problem_params;
};

extern template struct RiemannProblem<one_d>;
extern template struct RiemannProblem<two_d>;
extern template struct RiemannProblem<three_d>;

} // namespace problems
} // namespace ark
