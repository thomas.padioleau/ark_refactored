#pragma once

#include <memory>

#include "Params.hpp"
#include "PoiseuilleParams.hpp"
#include "Problem.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{
namespace problems
{

class PoiseuilleProblem : public Problem<three_d>
{
public:
    PoiseuilleProblem(const std::shared_ptr<Params<three_d>>& params);

    PoiseuilleProblem(const PoiseuilleProblem& x) = default;

    PoiseuilleProblem(PoiseuilleProblem&& x) = default;

    ~PoiseuilleProblem() override = default;

    PoiseuilleProblem& operator=(const PoiseuilleProblem& x) = default;

    PoiseuilleProblem& operator=(PoiseuilleProblem&& x) = default;

    void initialize(const RealArray<three_d>& u, const UniformGrid<three_d>& grid) const final;

    void make_boundaries_user(
            const RealArray<three_d>& u,
            const UniformGrid<three_d>& grid,
            int idim,
            int iside) final;

    std::shared_ptr<PoiseuilleParams> m_prob_params;
};

} // namespace problems
} // namespace ark
