#pragma once

#include "BaseKernel.hpp"
#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "IsothermalAtmosphereAtRestParams.hpp"
#include "Params.hpp"
#include "Problem.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{
namespace problems
{

class IsothermalAtmosphereAtRestInitKernel : BaseKernel<one_d>
{
    using Super = BaseKernel<one_d>;

    using Euler = EulerSystem<one_d>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;
    using VC = typename Euler::VarCons;
    using VP = typename Euler::VarPrim;

public:
    IsothermalAtmosphereAtRestInitKernel(
            const Params<one_d>& params,
            const IsothermalAtmosphereAtRestParams& prob_params,
            const RealArray<one_d>& u,
            const UniformGrid<one_d>& grid)
        : m_params(params)
        , m_prob_params(prob_params)
        , m_eos(params.thermo)
        , m_u(u)
        , m_grid(grid)
        , Rstar(code_units::constants::Rstar_h / m_params.thermo.mmw)
    {
    }

    KOKKOS_INLINE_FUNCTION
    Real phi(Int index) const
    {
        const RealVector<one_d> OX {m_grid.getCellCenter(index)};
        Real phi {};
        for (Int idim = 0; idim < one_d; ++idim)
        {
            phi -= m_params.hydro.g[idim] * OX[idim];
        }
        return phi;
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(Int p) const
    {
        using namespace constants;

        if (p == 0)
        {
            for (Int ix = m_grid.m_ghostWidths[IX];
                 ix < m_grid.m_nbCells[IX] + m_grid.m_ghostWidths[IX];
                 ++ix)
            {
                const Int j = ix;
                const Int k = ix - 1;

                const ConsState u_k = getCons(m_u, k);
                const PrimState q_k = Euler::conservativeToPrimitive(u_k, m_eos);

                const Real rho_k = ix == m_grid.m_ghostWidths[IX]
                                           ? m_prob_params.pressure_bottom
                                                     / (Rstar * m_prob_params.temperature)
                                           : q_k.d;

                PrimState q_j;
                q_j.d = rho_k
                        * ((two * Rstar * m_prob_params.temperature + (phi(k) - phi(j)))
                           / (two * Rstar * m_prob_params.temperature - (phi(k) - phi(j))));
                q_j.p = q_j.d * Rstar * m_prob_params.temperature;
                q_j.v(IX) = zero;

                const ConsState u_j = Euler::primitiveToConservative(q_j, m_eos);
                set(m_u, j, u_j);
            }
        }
    }

    Params<one_d> m_params;
    IsothermalAtmosphereAtRestParams m_prob_params;
    EquationOfState m_eos;
    RealArray<one_d> m_u;
    UniformGrid<one_d> m_grid;
    Real Rstar;
};

} // namespace problems
} // namespace ark
