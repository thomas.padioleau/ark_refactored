#pragma once

#include <cmath>

#include "BaseKernel.hpp"
#include "EulerSystem.hpp"
#include "GreshoParams.hpp"
#include "Params.hpp"
#include "Problem.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{
namespace problems
{

class GreshoInitKernel : public BaseKernel<two_d>
{
    using Super = BaseKernel<two_d>;

    using Euler = EulerSystem<two_d>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;
    using VC = typename Euler::VarCons;
    using VP = typename Euler::VarPrim;

public:
    GreshoInitKernel(
            const Params<two_d>& params,
            const GreshoParams& pParams,
            const RealArray<two_d>& u,
            const UniformGrid<two_d>& grid)
        : m_params(params)
        , m_problem_params(pParams)
        , m_eos(params.thermo)
        , m_u(u)
        , m_grid(grid)
    {
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(Int j) const
    {
        using namespace constants;

        const RealVector<two_d> OX = m_grid.getCellCenter(j);
        const RealVector<two_d> CX
                = {OX[IX] - m_problem_params.OC[IX], OX[IY] - m_problem_params.OC[IY]};
        const Real r = std::sqrt(CX[IX] * CX[IX] + CX[IY] * CX[IY]);
        const Real theta = std::atan2(CX[IY], CX[IX]);

        Real vtheta = zero;
        Real p = zero;
        if (r <= two * tenth)
        {
            vtheta = five * r;
            p = m_problem_params.p0 + (five * five * half) * r * r;
        }
        else if (r <= four * tenth)
        {
            vtheta = two - five * r;
            p = m_problem_params.p0 + (five * five * half) * r * r
                + four * (one - five * r + std::log(five * r));
        }
        else
        {
            vtheta = zero;
            p = m_problem_params.p0 - two + std::log(four * four);
        }

        PrimState q_j;
        q_j.d = m_problem_params.rho;
        q_j.p = p;
        q_j.v(IX) = -vtheta * std::sin(theta);
        q_j.v(IY) = +vtheta * std::cos(theta);

        const ConsState u_j = Euler::primitiveToConservative(q_j, m_eos);
        set(m_u, j, u_j);
    }

    Params<two_d> m_params;
    GreshoParams m_problem_params;
    EquationOfState m_eos;
    RealArray<two_d> m_u;
    UniformGrid<two_d> m_grid;
};

} // namespace problems
} // namespace ark
