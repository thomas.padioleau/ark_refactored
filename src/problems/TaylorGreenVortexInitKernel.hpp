#pragma once

#include <cmath>

#include "BaseKernel.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "Problem.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{
namespace problems
{

class TaylorGreenVortexInitKernel : BaseKernel<two_d>
{
    using Super = BaseKernel<two_d>;

    using Euler = EulerSystem<two_d>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;
    using VC = typename Euler::VarCons;
    using VP = typename Euler::VarPrim;

public:
    TaylorGreenVortexInitKernel(
            const Params<two_d>& params,
            const RealArray<two_d>& u,
            const UniformGrid<two_d>& grid)
        : m_params(params)
        , m_eos(params.thermo)
        , m_u(u)
        , m_grid(grid)
    {
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(Int j) const
    {
        using namespace constants;
        const RealVector<two_d> OX = m_grid.getCellCenter(j);

        const Real u0 = one;
        const Real p0 = ten * ten;
        const Real r0 = one;

        PrimState q_j;
        q_j.d = one;
        q_j.p = p0
                + r0 * u0 * u0 / four
                          * (std::cos(two * pi_2 * OX[IX]) + std::cos(two * pi_2 * OX[IY]));
        q_j.v(IX) = +u0 * std::sin(pi_2 * OX[IX]) * std::cos(pi_2 * OX[IY]);
        q_j.v(IY) = -u0 * std::cos(pi_2 * OX[IX]) * std::sin(pi_2 * OX[IY]);

        const ConsState u_j = Euler::primitiveToConservative(q_j, m_eos);
        set(m_u, j, u_j);
    }

    Params<two_d> m_params;
    EquationOfState m_eos;
    RealArray<two_d> m_u;
    UniformGrid<two_d> m_grid;
};

} // namespace problems
} // namespace ark
