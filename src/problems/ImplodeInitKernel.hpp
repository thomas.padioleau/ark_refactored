#pragma once

#include "BaseKernel.hpp"
#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "Problem.hpp"
#include "UniformGrid.hpp"

namespace ark
{
namespace problems
{

template <dim_t dim>
class ImplodeInitKernel : public BaseKernel<dim>
{
    using Super = BaseKernel<dim>;

    using Euler = EulerSystem<dim>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;
    using VC = typename Euler::VarCons;
    using VP = typename Euler::VarPrim;

public:
    ImplodeInitKernel(
            const Params<dim>& params,
            const RealArray<dim>& u,
            const UniformGrid<dim>& grid)
        : m_eos(params.thermo)
        , m_u(u)
        , m_grid(grid)
    {
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(Int j) const
    {
        using namespace constants;

        const RealVector<dim> OX = m_grid.getCellCenter(j);
        Real tmp = zero;
        for (Int idim = 0; idim < dim; ++idim)
        {
            tmp += OX[idim];
        }

        PrimState q_j;
        q_j.d = tmp < half ? one : eigth;
        q_j.p = tmp < half ? one : tenth + four * hundredth;
        for (int idim = 0; idim < dim; ++idim)
        {
            q_j.v(idim) = zero;
        }

        const ConsState u_j = Euler::primitiveToConservative(q_j, m_eos);
        Super::set(m_u, j, u_j);
    }

    EquationOfState m_eos;
    RealArray<dim> m_u;
    UniformGrid<dim> m_grid;
};

} // namespace problems
} // namespace ark
