#pragma once

#include <memory>

#include "Params.hpp"
#include "Problem.hpp"
#include "RayleighBenardParams.hpp"
#include "Types.hpp"

namespace ark
{
namespace problems
{

struct RayleighBenardProblem : Problem<two_d>
{
    RayleighBenardProblem(const std::shared_ptr<Params<two_d>>& params);
    void initialize(const RealArray<two_d>& u, const UniformGrid<two_d>& grid) const final;
    void make_boundaries_user(
            const RealArray<two_d>& u,
            const UniformGrid<two_d>& grid,
            int idim,
            int iside) final;

    std::shared_ptr<RayleighBenardParams> m_problem_params;
};

} // namespace problems
} // namespace ark
