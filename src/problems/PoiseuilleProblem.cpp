#include "PoiseuilleBoundariesKernel.hpp"
#include "PoiseuilleInitKernel.hpp"
#include "PoiseuilleProblem.hpp"
#include "Print.hpp"
#include "Types.hpp"

namespace ark
{

namespace problems
{

PoiseuilleProblem::PoiseuilleProblem(const std::shared_ptr<Params<three_d>>& params)
    : Problem<three_d>(params)
    , m_prob_params(std::make_shared<PoiseuilleParams>(params->reader))
{
}

void PoiseuilleProblem::initialize(const RealArray<three_d>& u, const UniformGrid<three_d>& grid)
        const
{
    PoiseuilleInitKernel kernel(*m_params, *m_prob_params, u, grid);
    Kokkos::parallel_for(grid.nbCells(), kernel);
}

void PoiseuilleProblem::make_boundaries_user(
        const RealArray<three_d>& u,
        const UniformGrid<three_d>& grid,
        int idim,
        int iside)
{
    PoiseuilleBoundariesKernel kernel(*m_params, *m_prob_params, u, grid, idim, iside);
    if (iside == IL)
    {
        IntVector<three_d> start = {0, 0, 0};
        IntVector<three_d> end
                = {grid.m_nbCells[IX] + 2 * grid.m_ghostWidths[IX],
                   grid.m_nbCells[IY] + 2 * grid.m_ghostWidths[IY],
                   grid.m_nbCells[IZ] + 2 * grid.m_ghostWidths[IZ]};
        end[idim] = grid.m_ghostWidths[idim];
        Kokkos::MDRangePolicy<Kokkos::Rank<3>, Tags::SideL_t>
                policy({start[IX], start[IY], start[IZ]}, {end[IX], end[IY], end[IZ]});
        Kokkos::parallel_for(policy, kernel);
    }
    if (iside == IR)
    {
        IntVector<three_d> start = {0, 0, 0};
        start[idim] = grid.m_nbCells[idim] + grid.m_ghostWidths[idim];
        IntVector<three_d> end
                = {grid.m_nbCells[IX] + 2 * grid.m_ghostWidths[IX],
                   grid.m_nbCells[IY] + 2 * grid.m_ghostWidths[IY],
                   grid.m_nbCells[IZ] + 2 * grid.m_ghostWidths[IZ]};
        Kokkos::MDRangePolicy<Kokkos::Rank<3>, Tags::SideR_t>
                policy({start[IX], start[IY], start[IZ]}, {end[IX], end[IY], end[IZ]});
        Kokkos::parallel_for(policy, kernel);
    }
}

} // namespace problems

} // namespace ark
