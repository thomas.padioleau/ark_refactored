#include <memory>

#include "Params.hpp"
#include "Print.hpp"
#include "Problem.hpp"
#include "RiemannInitKernel.hpp"
#include "RiemannParams.hpp"
#include "RiemannProblem.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{
namespace problems
{

template <dim_t dim>
RiemannProblem<dim>::RiemannProblem(const std::shared_ptr<Params<dim>>& params)
    : Problem<dim>(params)
    , m_problem_params(std::make_shared<RiemannParams<dim>>(params->reader))
{
}

template <dim_t dim>
void RiemannProblem<dim>::initialize(const RealArray<dim>& u, const UniformGrid<dim>& grid) const
{
    Print() << "Initializing shock tube problem... ";

    RiemannInitKernel<dim> kernel(*Problem<dim>::m_params, *m_problem_params, u, grid);
    Kokkos::RangePolicy<Int> range(0, grid.nbCells());
    Kokkos::parallel_for("Riemann initialization kernel - RangePolicy", range, kernel);

    Print() << "done\n";
}

template struct RiemannProblem<one_d>;
template struct RiemannProblem<two_d>;
template struct RiemannProblem<three_d>;

} // namespace problems
} // namespace ark
