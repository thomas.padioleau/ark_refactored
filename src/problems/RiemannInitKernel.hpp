#pragma once

#include "BaseKernel.hpp"
#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "RiemannParams.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{
namespace problems
{

template <dim_t dim>
class RiemannInitKernel : public BaseKernel<dim>
{
    using Super = BaseKernel<dim>;

    using Euler = EulerSystem<dim>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;
    using VC = typename Euler::VarCons;
    using VP = typename Euler::VarPrim;

public:
    RiemannInitKernel(
            const Params<dim>& params,
            const RiemannParams<dim>& pParams,
            const RealArray<dim>& u,
            const UniformGrid<dim>& grid)
        : m_eos(params.thermo)
        , m_u(u)
        , m_grid(grid)
        , m_problem_params(pParams)
    {
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(Int j) const
    {
        const RealVector<dim> OX = m_grid.getCellCenter(j);

        PrimState q_j;
        q_j.d = OX[IX] < m_problem_params.center ? m_problem_params.density_left
                                                 : m_problem_params.density_right;
        q_j.p = OX[IX] < m_problem_params.center ? m_problem_params.pressure_left
                                                 : m_problem_params.pressure_right;
        q_j.v(IX) = OX[IX] < m_problem_params.center ? m_problem_params.velocity_left
                                                     : m_problem_params.velocity_right;
        for (Int ix = IY; ix < dim; ++ix)
        {
            q_j.v(ix) = constants::zero;
        }

        const ConsState u_j = Euler::primitiveToConservative(q_j, m_eos);
        Super::set(m_u, j, u_j);
    }

    EquationOfState m_eos;
    RealArray<dim> m_u;
    UniformGrid<dim> m_grid;
    RiemannParams<dim> m_problem_params;
};

} // namespace problems
} // namespace ark
