#pragma once

#include <cmath>

#include "BaseKernel.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "RayleighBenardParams.hpp"
#include "Types.hpp"
#include "Units.hpp"

namespace ark
{
namespace problems
{

class RayleighBenardBoundariesKernel : public BaseKernel<two_d>
{
    using Super = BaseKernel<two_d>;

    using Euler = EulerSystem<two_d>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;
    using VC = typename Euler::VarCons;
    using VP = typename Euler::VarPrim;

public:
    struct downTag
    {
    };
    struct upTag
    {
    };

    RayleighBenardBoundariesKernel(
            const RealArray<two_d>& u,
            const UniformGrid<two_d>& grid,
            const Params<two_d>& params)
        : m_u(u)
        , m_grid(grid)
        , m_params(params)
        , m_eos(params.thermo)
    {
    }

    KOKKOS_INLINE_FUNCTION
    Real phi(IntVector<two_d> coord) const
    {
        RealVector<two_d> OX = m_grid.getCellCenter(coord);
        return -m_params.hydro.g[IX] * OX[IX] - m_params.hydro.g[IY] * OX[IY];
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const downTag&, Int ix) const
    {
        using namespace constants;

        const Real Rstar = code_units::constants::Rstar_h / m_params.thermo.mmw;
        const Real beta = -1.0;

        for (Int iy_ = m_grid.m_ghostWidths[IY]; iy_ >= 1; --iy_)
        {
            const Int iy = iy_ - 1;
            const IntVector<two_d> coord = {ix, iy};
            const Int j = m_grid.coordToIndex(coord);
            const IntVector<two_d> coord0 = {ix, 2 * m_grid.m_ghostWidths[IY] - 1 - iy};
            const Int k = m_grid.coordToIndex(coord0);
            const ConsState u_k = getCons(m_u, k);
            const PrimState q_k = Euler::conservativeToPrimitive(u_k, m_eos);
            const Real T_k = m_eos.computeTemperature(q_k.d, q_k.p);

            PrimState q_j;
            const Real T_j {
                    T_k - static_cast<Real>(coord0[IY] - coord[IY]) * beta * m_grid.m_dl[IY]};

            const Real Dphi = phi(coord0) - phi(coord);
            q_j.d = (two * q_k.p + q_k.d * Dphi) / (two * Rstar * T_j - Dphi);
            q_j.p = q_j.d * Rstar * T_j;
            q_j.v(IX) = +q_k.v(IX);
            q_j.v(IY) = -q_k.v(IY);

            const ConsState u_j = Euler::primitiveToConservative(q_j, m_eos);
            set(m_u, j, u_j);
        }
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const upTag&, Int ix) const
    {
        using namespace constants;

        const Real Rstar = code_units::constants::Rstar_h / m_params.thermo.mmw;

        for (Int iy = m_grid.m_nbCells[IY] + m_grid.m_ghostWidths[IY];
             iy < m_grid.m_nbCells[IY] + 2 * m_grid.m_ghostWidths[IY];
             ++iy)
        {
            const IntVector<two_d> coord = {ix, iy};
            const Int j = m_grid.coordToIndex(coord);
            const IntVector<two_d> coord0
                    = {ix, 2 * m_grid.m_nbCells[IY] + 2 * m_grid.m_ghostWidths[IY] - 1 - iy};
            const Int k = m_grid.coordToIndex(coord0);
            const ConsState u_k = getCons(m_u, k);
            const PrimState q_k = Euler::conservativeToPrimitive(u_k, m_eos);

            PrimState q_j;
            const Real T_j = two;

            const Real Dphi = phi(coord) - phi(coord0);
            q_j.d = (two * q_k.p - q_k.d * Dphi) / (two * Rstar * T_j + Dphi);
            q_j.p = q_j.d * Rstar * T_j;
            q_j.v(IX) = +q_k.v(IX);
            q_j.v(IY) = -q_k.v(IY);

            const ConsState u_j = Euler::primitiveToConservative(q_j, m_eos);
            set(m_u, j, u_j);
        }
    }

    RealArray<two_d> m_u;
    UniformGrid<two_d> m_grid;
    Params<two_d> m_params;
    EquationOfState m_eos;
};

} // namespace problems
} // namespace ark
