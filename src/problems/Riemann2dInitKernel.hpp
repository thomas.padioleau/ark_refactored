#pragma once

#include <cmath>

#include "BaseKernel.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "Problem.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{
namespace problems
{

class Riemann2dInitKernel : public BaseKernel<two_d>
{
    using Super = BaseKernel<two_d>;

    using Euler = EulerSystem<two_d>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;
    using VC = typename Euler::VarCons;
    using VP = typename Euler::VarPrim;

public:
    Riemann2dInitKernel(
            const Params<two_d>& params,
            const RealArray<two_d>& u,
            const UniformGrid<two_d>& grid)
        : m_params(params)
        , m_eos(params.thermo)
        , m_u(u)
        , m_grid(grid)
        , x_mid(constants::half)
        , y_mid(constants::half)
    {
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(Int j) const
    {
        using namespace constants;
        const RealVector<two_d> OX {m_grid.getCellCenter(j)};

        PrimState q_j;
        if (OX[IX] > x_mid)
        {
            if (OX[IY] > y_mid)
            {
                // quarter 0
                q_j.d = +one;
                q_j.p = +one;
                q_j.v(IX) = +quarter - one;
                q_j.v(IY) = -half;
            }
            else
            {
                // quarter 3
                q_j.d = +three;
                q_j.p = +one;
                q_j.v(IX) = +one - quarter;
                q_j.v(IY) = -half;
            }
        }
        else
        {
            if (OX[IY] > y_mid)
            {
                // quarter 1
                q_j.d = +two;
                q_j.p = +one;
                q_j.v(IX) = +quarter - one;
                q_j.v(IY) = +half;
            }
            else
            {
                // quarter 2
                q_j.d = +one;
                q_j.p = +one;
                q_j.v(IX) = +one - quarter;
                q_j.v(IY) = +half;
            }
        }

        const ConsState u_j {Euler::primitiveToConservative(q_j, m_eos)};
        set(m_u, j, u_j);
    }

    Params<two_d> m_params;
    EquationOfState m_eos;
    RealArray<two_d> m_u;
    UniformGrid<two_d> m_grid;
    Real x_mid, y_mid;
};
} // namespace problems
} // namespace ark
