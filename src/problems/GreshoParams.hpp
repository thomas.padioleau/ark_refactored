#pragma once

#include "inih/INIReader.hpp"

#include "Params.hpp"
#include "Types.hpp"

namespace ark
{
namespace problems
{

struct GreshoParams
{
    GreshoParams(const INIReader& reader)
    {
        using namespace constants;

        OC[IX] = reader.GetReal("problem", "xcenter", OC[IX]);
        OC[IY] = reader.GetReal("problem", "ycenter", OC[IY]);
        rho = reader.GetReal("problem", "density", rho);
        mach = reader.GetReal("problem", "mach", mach);
        p0 = rho / (mach * mach);
    }

    RealVector<two_d> OC = {{constants::half, constants::half}};
    Real mach = constants::tenth;
    Real rho = constants::one;
    Real p0 = constants::zero;
};

} // namespace problems
} // namespace ark
