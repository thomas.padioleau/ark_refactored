#include <memory>

#include "BaseKernel.hpp"
#include "ImplodeInitKernel.hpp"
#include "ImplodeProblem.hpp"
#include "Params.hpp"
#include "Print.hpp"
#include "Problem.hpp"
#include "Types.hpp"

namespace ark
{
namespace problems
{

template <dim_t dim>
ImplodeProblem<dim>::ImplodeProblem(const std::shared_ptr<Params<dim>>& params)
    : Problem<dim>(params)
{
}

template <dim_t dim>
void ImplodeProblem<dim>::initialize(const RealArray<dim>& u, const UniformGrid<dim>& grid) const
{
    Print() << "Initializing implode problem... ";

    ImplodeInitKernel<dim> kernel(*Problem<dim>::m_params, u, grid);
    Kokkos::RangePolicy<Int> range(0, grid.nbCells());
    Kokkos::parallel_for("Implode initialization kernel - RangePolicy", range, kernel);

    Print() << "done\n";
}

template struct ImplodeProblem<one_d>;
template struct ImplodeProblem<two_d>;
template struct ImplodeProblem<three_d>;

} // namespace problems
} // namespace ark
