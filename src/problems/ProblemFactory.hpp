#pragma once

#include <memory>

#include "Params.hpp"
#include "Problem.hpp"
#include "Types.hpp"

namespace ark
{
namespace problems
{

// Be careful, here we do not use explicit template instantiation but
// full specialization.
template <dim_t dim>
class ProblemFactory
{
public:
    static std::shared_ptr<Problem<dim>> New(
            const std::string& problem_name,
            const std::shared_ptr<Params<dim>>& params);
};

} // namespace problems
} // namespace ark
