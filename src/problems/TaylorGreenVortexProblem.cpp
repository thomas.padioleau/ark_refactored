#include <memory>

#include "Params.hpp"
#include "Print.hpp"
#include "Problem.hpp"
#include "TaylorGreenVortexInitKernel.hpp"
#include "TaylorGreenVortexProblem.hpp"
#include "Types.hpp"

namespace ark
{
namespace problems
{

TaylorGreenVortexProblem::TaylorGreenVortexProblem(const std::shared_ptr<Params<two_d>>& params)
    : Problem<two_d>(params)
{
}

void TaylorGreenVortexProblem::initialize(const RealArray<two_d>& u, const UniformGrid<two_d>& grid)
        const
{
    Print() << "Initializing TaylorGreenVortex problem... ";

    TaylorGreenVortexInitKernel kernel(*m_params, u, grid);
    Kokkos::RangePolicy<Int> range(0, grid.nbCells());
    Kokkos::parallel_for("Taylor-Green initialization kernel - RangePolicy", range, kernel);

    Print() << "done\n";
}

} // namespace problems
} // namespace ark
