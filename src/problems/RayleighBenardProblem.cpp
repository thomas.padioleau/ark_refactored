#include <memory>

#include "Print.hpp"
#include "RayleighBenardBoundariesKernel.hpp"
#include "RayleighBenardInitKernel.hpp"
#include "RayleighBenardParams.hpp"
#include "RayleighBenardProblem.hpp"

namespace ark
{
namespace problems
{

RayleighBenardProblem::RayleighBenardProblem(const std::shared_ptr<Params<two_d>>& params)
    : Problem<two_d>(params)
    , m_problem_params(std::make_shared<RayleighBenardParams>(params->reader))
{
}

void RayleighBenardProblem::initialize(const RealArray<two_d>& u, const UniformGrid<two_d>& grid)
        const
{
    Print() << "Initializing Rayleigh-Bénard problem... ";

    RayleighBenardInitKernel kernel(*m_params, *m_problem_params, u, grid);
    Kokkos::RangePolicy<Int> range(0, grid.m_nbCells[IX] + 2 * grid.m_ghostWidths[IX]);
    Kokkos::parallel_for("Rayleigh-Bénard initialization kernel - RangePolicy", range, kernel);

    Print() << "done\n";
}

void RayleighBenardProblem::make_boundaries_user(
        const RealArray<two_d>& u,
        const UniformGrid<two_d>& grid,
        int,
        int iside)
{
    RayleighBenardBoundariesKernel kernel(u, grid, *m_params);
    if (iside == 0)
    {
        Kokkos::RangePolicy<Int, RayleighBenardBoundariesKernel::downTag>
                range(0, grid.m_nbCells[IX] + 2 * grid.m_ghostWidths[IX]);
        Kokkos::parallel_for(range, kernel);
    }
    else
    {
        Kokkos::RangePolicy<Int, RayleighBenardBoundariesKernel::upTag>
                range(0, grid.m_nbCells[IX] + 2 * grid.m_ghostWidths[IX]);
        Kokkos::parallel_for(range, kernel);
    }
}

} // namespace problems
} // namespace ark
