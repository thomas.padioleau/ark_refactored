#pragma once

#include "BaseKernel.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "RayleighTaylorParams.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{
namespace problems
{

class RayleighTaylorBoundariesKernel : public BaseKernel<two_d>
{
    using Super = BaseKernel<two_d>;

    using Euler = EulerSystem<two_d>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;
    using VC = typename Euler::VarCons;
    using VP = typename Euler::VarPrim;

public:
    struct downTag
    {
    };
    struct upTag
    {
    };

    RayleighTaylorBoundariesKernel(
            const RealArray<two_d>& u,
            const UniformGrid<two_d>& grid,
            const Params<two_d>& params)
        : m_u(u)
        , m_grid(grid)
        , m_params(params)
        , m_eos(params.thermo)
    {
    }

    KOKKOS_INLINE_FUNCTION
    Real phi(Int index) const
    {
        const RealVector<two_d> OX = m_grid.getCellCenter(index);
        Real phi {};
        for (Int idim2 = 0; idim2 < two_d; ++idim2)
        {
            phi -= m_params.hydro.g[idim2] * OX[idim2];
        }
        return phi;
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const downTag&, Int ix) const
    {
        using namespace constants;

        for (Int iy_ = m_grid.m_ghostWidths[IY]; iy_ >= 1; --iy_)
        {
            const Int iy = iy_ - 1;
            const IntVector<two_d> coord = {ix, iy};
            const Int j = m_grid.coordToIndex(coord);
            const IntVector<two_d> coord0 = {ix, iy + 1};
            const Int k = m_grid.coordToIndex(coord0);
            const ConsState u_k = getCons(m_u, k);
            const PrimState q_k = Euler::conservativeToPrimitive(u_k, m_eos);

            PrimState q_j;
            q_j.d = q_k.d;
            q_j.p = q_k.p + half * (q_k.d + q_j.d) * (phi(k) - phi(j));
            q_j.v(IX) = q_k.v(IX);
            q_j.v(IY) = -q_k.v(IY);

            const ConsState u_j = Euler::primitiveToConservative(q_j, m_eos);
            set(m_u, j, u_j);
        }
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const upTag&, Int ix) const
    {
        using namespace constants;

        for (Int iy = m_grid.m_nbCells[IY] + m_grid.m_ghostWidths[IY];
             iy < m_grid.m_nbCells[IY] + 2 * m_grid.m_ghostWidths[IY];
             ++iy)
        {
            const IntVector<two_d> coord = {ix, iy};
            const Int j = m_grid.coordToIndex(coord);
            const IntVector<two_d> coord0 = {ix, iy - 1};
            const Int k = m_grid.coordToIndex(coord0);
            const ConsState u_k = getCons(m_u, k);
            const PrimState q_k = Euler::conservativeToPrimitive(u_k, m_eos);

            PrimState q_j;
            q_j.d = q_k.d;
            q_j.p = q_k.p + half * (q_k.d + q_j.d) * (phi(k) - phi(j));
            q_j.v(IX) = q_k.v(IX);
            q_j.v(IY) = -q_k.v(IY);

            const ConsState u_j = Euler::primitiveToConservative(q_j, m_eos);
            set(m_u, j, u_j);
        }
    }

    RealArray<two_d> m_u;
    UniformGrid<two_d> m_grid;
    Params<two_d> m_params;
    EquationOfState m_eos;
};

} // namespace problems
} // namespace ark
