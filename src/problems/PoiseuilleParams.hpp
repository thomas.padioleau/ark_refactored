#pragma once

#include "inih/INIReader.hpp"

#include "Types.hpp"

namespace ark
{

namespace problems
{

struct PoiseuilleParams
{
    PoiseuilleParams(const INIReader& reader)
    {
        flow_dir = reader.GetInteger("problem", "flow_dir", flow_dir);
        p_gradient = reader.GetReal("problem", "p_gradient", p_gradient);
        p0 = reader.GetReal("problem", "p0", p0);
    }

    int flow_dir = IX;
    Real p_gradient = -8e-6;
    Real p0 = 0.0240096;
};

} // namespace problems

} // namespace ark
