#pragma once

#include "inih/INIReader.hpp"

#include "Constants.hpp"
#include "Params.hpp"

namespace ark
{
namespace problems
{

struct IsothermalAtmosphereAtRestParams
{
    IsothermalAtmosphereAtRestParams(const INIReader& reader)
    {
        pressure_bottom = reader.GetReal("problem", "pressure_bottom", pressure_bottom);
        temperature = reader.GetReal("problem", "temperature", temperature);
    }

    Real pressure_bottom = constants::hundred * constants::thousand;
    Real temperature = constants::three * constants::hundred;
};

} // namespace problems
} // namespace ark
