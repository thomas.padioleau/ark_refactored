#pragma once

#include <memory>

#include "Params.hpp"
#include "Problem.hpp"
#include "Types.hpp"

namespace ark
{
namespace problems
{

template <dim_t dim>
struct ImplodeProblem : Problem<dim>
{
    ImplodeProblem(const std::shared_ptr<Params<dim>>& params);

    void initialize(const RealArray<dim>& u, const UniformGrid<dim>& grid) const final;
};

extern template struct ImplodeProblem<one_d>;
extern template struct ImplodeProblem<two_d>;
extern template struct ImplodeProblem<three_d>;

} // namespace problems
} // namespace ark
