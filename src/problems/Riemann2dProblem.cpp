#include <memory>

#include "Params.hpp"
#include "Print.hpp"
#include "Problem.hpp"
#include "Riemann2dInitKernel.hpp"
#include "Riemann2dProblem.hpp"
#include "Types.hpp"

namespace ark
{
namespace problems
{

Riemann2dProblem::Riemann2dProblem(const std::shared_ptr<Params<two_d>>& params)
    : Problem<two_d>(params)
{
}

void Riemann2dProblem::initialize(const RealArray<two_d>& u, const UniformGrid<two_d>& grid) const
{
    Print() << "Initializing Riemann2d problem... ";

    Riemann2dInitKernel kernel(*m_params, u, grid);
    Kokkos::RangePolicy<Int> range(0, grid.nbCells());
    Kokkos::parallel_for("Riemann2d initialization kernel - RangePolicy", range, kernel);

    Print() << "done\n";
}

} // namespace problems
} // namespace ark
