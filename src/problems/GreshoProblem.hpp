#pragma once

#include <memory>

#include "GreshoParams.hpp"
#include "Params.hpp"
#include "Problem.hpp"
#include "Types.hpp"

namespace ark
{
namespace problems
{

struct GreshoProblem : Problem<two_d>
{
    GreshoProblem(const std::shared_ptr<Params<two_d>>& params);

    void initialize(const RealArray<two_d>& u, const UniformGrid<two_d>& grid) const final;

    std::shared_ptr<GreshoParams> m_problem_params;
};

} // namespace problems
} // namespace ark
