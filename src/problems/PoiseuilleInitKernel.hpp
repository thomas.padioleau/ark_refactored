#pragma once

#include "BaseKernel.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "PoiseuilleParams.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{

namespace problems
{

class PoiseuilleInitKernel : public BaseKernel<three_d>
{
public:
    using Super = BaseKernel<three_d>;

    using Euler = EulerSystem<three_d>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;
    using VC = typename Euler::VarCons;
    using VP = typename Euler::VarPrim;

    PoiseuilleInitKernel(
            const Params<three_d>& params,
            const PoiseuilleParams& prob_params,
            const RealArray<three_d>& u,
            const UniformGrid<three_d>& grid)
        : m_u(u)
        , m_eos(params.thermo)
        , m_grid(grid)
        , m_flow_dir(prob_params.flow_dir)
        , m_pGradient(prob_params.p_gradient)
        , m_p0(prob_params.p0)
    {
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const Int j) const
    {
        PrimState q_j;
        q_j.d = constants::one;
        q_j.p = m_p0;
        q_j.v(IX) = constants::zero;
        q_j.v(IY) = constants::zero;
        q_j.v(IZ) = constants::zero;

        ConsState u_j = Euler::primitiveToConservative(q_j, m_eos);
        Super::set(m_u, j, u_j);
    }

    RealArray<three_d> m_u;
    EquationOfState m_eos;
    UniformGrid<three_d> m_grid;
    int m_flow_dir;
    Real m_pGradient;
    Real m_p0;
};

} // namespace problems

} // namespace ark
