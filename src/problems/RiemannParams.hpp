#pragma once

#include "inih/INIReader.hpp"

#include "Constants.hpp"
#include "Params.hpp"
#include "Types.hpp"

namespace ark
{
namespace problems
{

template <dim_t dim>
struct RiemannParams
{
    RiemannParams(const INIReader& reader)
    {
        density_left = reader.GetReal("problem", "density_left", density_left);
        density_right = reader.GetReal("problem", "density_right", density_right);
        pressure_left = reader.GetReal("problem", "pressure_left", pressure_left);
        pressure_right = reader.GetReal("problem", "pressure_right", pressure_right);
        velocity_left = reader.GetReal("problem", "velocity_left", velocity_left);
        velocity_right = reader.GetReal("problem", "velocity_right", velocity_right);
        center = reader.GetReal("problem", "center", center);
    }

    Real density_left = constants::one;
    Real pressure_left = constants::one;
    Real density_right = constants::eigth;
    Real pressure_right = constants::tenth;
    Real velocity_left = constants::zero;
    Real velocity_right = constants::zero;
    Real center = constants::half;
};

} // namespace problems
} // namespace ark
