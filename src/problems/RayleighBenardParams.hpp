#pragma once

#include "inih/INIReader.hpp"

#include "Params.hpp"
#include "Types.hpp"

namespace ark
{
namespace problems
{

struct RayleighBenardParams
{
    RayleighBenardParams(const INIReader& reader)
    {
        density_up = reader.GetReal("problem", "density_up", density_up);
        perturbation = reader.GetReal("problem", "perturbation", perturbation);
    }

    Real density_up = constants::two;
    Real perturbation = constants::tenth * constants::tenth;
};

} // namespace problems
} // namespace ark
