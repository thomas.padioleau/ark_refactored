#pragma once

#include <cmath>

#include "EulerSystem.hpp"
#include "Params.hpp"
#include "Problem.hpp"
#include "RayleighBenardParams.hpp"
#include "Types.hpp"

namespace ark
{
namespace problems
{

class RayleighBenardInitKernel : public BaseKernel<two_d>
{
    using Super = BaseKernel<two_d>;

    using Euler = EulerSystem<two_d>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;
    using VC = typename Euler::VarCons;
    using VP = typename Euler::VarPrim;

public:
    RayleighBenardInitKernel(
            const Params<two_d>& params,
            const RayleighBenardParams& pParams,
            const RealArray<two_d>& u,
            const UniformGrid<two_d>& grid)
        : m_params(params)
        , m_problem_params(pParams)
        , m_eos(params.thermo)
        , m_u(u)
        , m_grid(grid)
    {
    }

    KOKKOS_INLINE_FUNCTION
    Real phi(Int index) const
    {
        const RealVector<two_d> OX = m_grid.getCellCenter(index);
        Real phi {};
        for (Int idim = 0; idim < two_d; ++idim)
        {
            phi -= m_params.hydro.g[idim] * OX[idim];
        }
        return phi;
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(Int ix) const
    {
        using namespace constants;

        if (ix >= m_grid.m_ghostWidths[IX] && ix < m_grid.m_nbCells[IX] + m_grid.m_ghostWidths[IX])
        {
            const Real Lx = m_params.mesh.up[IX] - m_params.mesh.low[IX];
            const Real Ly = m_params.mesh.up[IY] - m_params.mesh.low[IY];
            Real beta = -one;
            Real rho_top = one;
            Real T_top = two;
            Real T_bot = T_top - beta * Ly;
            Real m = one;
            Real rho = rho_top * std::pow(T_bot / T_top, m);
            Real T = T_bot;
            const Real Rstar = code_units::constants::Rstar_h / m_params.thermo.mmw;
            const RealVector<two_d> dl = m_grid.m_dl;

            for (Int iy = m_grid.m_ghostWidths[IY];
                 iy < m_grid.m_nbCells[IY] + m_grid.m_ghostWidths[IY];
                 ++iy)
            {
                const IntVector<two_d> coord = {ix, iy};
                const Int j = m_grid.coordToIndex(coord);
                const IntVector<two_d> coord0 = {ix, iy - 1};
                const Int k = m_grid.coordToIndex(coord0);
                const RealVector<two_d> OX = m_grid.getCellCenter(coord);

                PrimState q_j;
                q_j.d = rho;
                q_j.p = rho * Rstar * T;
                q_j.v(IX) = hundredth * hundredth
                            * (std::sin(pi_2 * (OX[IX] - half * Lx) / Lx)
                               * std::sin(pi_2 * (OX[IY] - half * five) / Ly));
                q_j.v(IY) = hundredth * hundredth
                            * (std::cos(pi_2 * (OX[IX] - half * Lx) / Lx)
                               * std::cos(pi_2 * (OX[IY] - half * five) / Ly));

                const ConsState u_j = Euler::primitiveToConservative(q_j, m_eos);
                set(m_u, j, u_j);

                rho *= (two * Rstar * T + (phi(k) - phi(j)))
                       / (two * Rstar * (T + beta * dl[IY]) - (phi(k) - phi(j)));
                T += beta * dl[IY];
            }
        }
    }

    Params<two_d> m_params;
    RayleighBenardParams m_problem_params;
    EquationOfState m_eos;
    RealArray<two_d> m_u;
    UniformGrid<two_d> m_grid;
};

} // namespace problems
} // namespace ark
