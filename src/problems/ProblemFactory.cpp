#include <exception>
#include <memory>

#include "GreshoProblem.hpp"
#include "ImplodeProblem.hpp"
#include "IsothermalAtmosphereAtRestProblem.hpp"
#include "Params.hpp"
#include "PoiseuilleProblem.hpp"
#include "Problem.hpp"
#include "ProblemFactory.hpp"
#include "RayleighBenardProblem.hpp"
#include "RayleighTaylorProblem.hpp"
#include "Riemann2dProblem.hpp"
#include "RiemannProblem.hpp"
#include "TaylorGreenVortexProblem.hpp"
#include "Types.hpp"

namespace ark
{
namespace problems
{

template <>
std::shared_ptr<Problem<one_d>> ProblemFactory<one_d>::New(
        const std::string& problem_name,
        const std::shared_ptr<Params<one_d>>& params)
{
    std::shared_ptr<Problem<one_d>> problem = nullptr;
    if (problem_name == "riemann")
    {
        problem = std::make_shared<RiemannProblem<one_d>>(params);
    }
    else if (problem_name == "implode")
    {
        problem = std::make_shared<ImplodeProblem<one_d>>(params);
    }
    else if (problem_name == "isothermal_atmosphere_at_rest")
    {
        problem = std::make_shared<IsothermalAtmosphereAtRestProblem>(params);
    }
    else
    {
        throw std::runtime_error("Invalid problem.");
    }
    return problem;
}

template <>
std::shared_ptr<Problem<two_d>> ProblemFactory<two_d>::New(
        const std::string& problem_name,
        const std::shared_ptr<Params<two_d>>& params)
{
    std::shared_ptr<Problem<two_d>> problem = nullptr;
    if (problem_name == "riemann")
    {
        problem = std::make_shared<RiemannProblem<two_d>>(params);
    }
    else if (problem_name == "riemann2d")
    {
        problem = std::make_shared<Riemann2dProblem>(params);
    }
    else if (problem_name == "implode")
    {
        problem = std::make_shared<ImplodeProblem<two_d>>(params);
    }
    else if (problem_name == "gresho")
    {
        problem = std::make_shared<GreshoProblem>(params);
    }
    else if (problem_name == "rayleigh_taylor")
    {
        problem = std::make_shared<RayleighTaylorProblem>(params);
    }
    else if (problem_name == "rayleigh_benard")
    {
        problem = std::make_shared<RayleighBenardProblem>(params);
    }
    else if (problem_name == "taylor_green")
    {
        problem = std::make_shared<TaylorGreenVortexProblem>(params);
    }
    else
    {
        throw std::runtime_error("Invalid problem.");
    }
    return problem;
}

template <>
std::shared_ptr<Problem<three_d>> ProblemFactory<three_d>::New(
        const std::string& problem_name,
        const std::shared_ptr<Params<three_d>>& params)
{
    std::shared_ptr<Problem<three_d>> problem = nullptr;
    if (problem_name == "poiseuille")
    {
        problem = std::make_shared<PoiseuilleProblem>(params);
    }
    else if (problem_name == "riemann")
    {
        problem = std::make_shared<RiemannProblem<three_d>>(params);
    }
    else if (problem_name == "implode")
    {
        problem = std::make_shared<ImplodeProblem<three_d>>(params);
    }
    else
    {
        throw std::runtime_error("Invalid problem.");
    }
    return problem;
}

} // namespace problems
} // namespace ark
