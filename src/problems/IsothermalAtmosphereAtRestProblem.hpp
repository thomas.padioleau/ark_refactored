#pragma once

#include <memory>

#include "IsothermalAtmosphereAtRestParams.hpp"
#include "Params.hpp"
#include "Problem.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark
{
namespace problems
{

struct IsothermalAtmosphereAtRestProblem : Problem<one_d>
{
    IsothermalAtmosphereAtRestProblem(const std::shared_ptr<Params<one_d>>& params);
    void initialize(const RealArray<one_d>& u, const UniformGrid<one_d>& grid) const final;
    void make_boundaries_user(
            const RealArray<one_d>& u,
            const UniformGrid<one_d>& grid,
            int idim,
            int iside) final;

    std::shared_ptr<IsothermalAtmosphereAtRestParams> m_prob_params;
};

} // namespace problems
} // namespace ark
