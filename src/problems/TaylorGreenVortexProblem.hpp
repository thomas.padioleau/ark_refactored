#pragma once

#include <memory>

#include "Params.hpp"
#include "Problem.hpp"
#include "Types.hpp"

namespace ark
{
namespace problems
{

struct TaylorGreenVortexProblem : Problem<two_d>
{
    TaylorGreenVortexProblem(const std::shared_ptr<Params<two_d>>& params);
    void initialize(const RealArray<two_d>& u, const UniformGrid<two_d>& grid) const final;
};

} // namespace problems
} // namespace ark
