#pragma once

#include "Types.hpp"

namespace ark
{

template <dim_t dim>
class StridedFillingCurve
{
public:
    StridedFillingCurve();

    StridedFillingCurve(IntVector<dim> nbCells, const IntVector<dim>& ghostWidths);

    StridedFillingCurve(IntVector<dim> nbCells, Int ghostWidth);

    StridedFillingCurve(const StridedFillingCurve& x) = default;

    StridedFillingCurve(StridedFillingCurve&& x) = default;

    ~StridedFillingCurve() = default;

    StridedFillingCurve& operator=(const StridedFillingCurve& x) = default;

    StridedFillingCurve& operator=(StridedFillingCurve&& x) = default;

    KOKKOS_INLINE_FUNCTION
    IntVector<dim> indexToCoord(Int index) const;

    KOKKOS_INLINE_FUNCTION
    Int coordToIndex(const IntVector<dim>& coord) const;

    KOKKOS_INLINE_FUNCTION
    Int faceToDim(Int face) const;

    KOKKOS_INLINE_FUNCTION
    Int faceToSide(Int face) const;

    KOKKOS_INLINE_FUNCTION
    constexpr Int faces(Int) const;

    KOKKOS_INLINE_FUNCTION
    Int getNeighbour(Int index, Int face) const;

    KOKKOS_INLINE_FUNCTION
    constexpr Int nbNeighbours() const;

    KOKKOS_INLINE_FUNCTION
    bool belongsToInnerDomain(const IntVector<dim>& coords, Int width = 0) const;

    KOKKOS_INLINE_FUNCTION
    bool belongsToInnerDomain(Int index, Int width = 0) const;

    KOKKOS_INLINE_FUNCTION
    Int nbCells() const;

    Int m_size;
    IntVector<dim> m_ghostWidths;
    IntVector<dim> m_nbCells;
    IntVector<dim + 1> m_strides;
    IntVector<2 * dim> m_neighbour_strides;
};

template <dim_t dim>
StridedFillingCurve<dim>::StridedFillingCurve()
    : m_size(1)
    , m_ghostWidths()
    , m_nbCells()
    , m_strides()
    , m_neighbour_strides()
{
}

template <dim_t dim>
StridedFillingCurve<dim>::StridedFillingCurve(
        IntVector<dim> nbCells,
        const IntVector<dim>& ghostWidths)
    : m_size(1)
    , m_ghostWidths(ghostWidths)
    , m_nbCells(nbCells)
    , m_strides()
    , m_neighbour_strides()
{
    for (Int idim = 0; idim < dim; ++idim)
    {
        m_size *= nbCells[idim] + 2 * m_ghostWidths[idim];
    }
    for (Int idim = 0; idim <= dim; ++idim)
    {
        m_strides[idim] = (idim == 0) ? 1
                                      : m_strides[idim - 1]
                                                * (nbCells[idim - 1] + 2 * m_ghostWidths[idim - 1]);
    }
    for (Int face = 0; face < 2 * dim; ++face)
    {
        m_neighbour_strides[face] = faceToSide(face) * m_strides[faceToDim(face)];
    }
}

template <dim_t dim>
StridedFillingCurve<dim>::StridedFillingCurve(IntVector<dim> nbCells, Int ghostWidth)
    : m_size(1)
    , m_ghostWidths()
    , m_nbCells(nbCells)
    , m_strides()
    , m_neighbour_strides()
{
    for (Int idim = 0; idim < dim; ++idim)
    {
        m_ghostWidths[idim] = ghostWidth;
    }
    for (Int idim = 0; idim < dim; ++idim)
    {
        m_size *= nbCells[idim] + 2 * m_ghostWidths[idim];
    }
    for (Int idim = 0; idim <= dim; ++idim)
    {
        m_strides[idim] = (idim == 0) ? 1
                                      : m_strides[idim - 1]
                                                * (nbCells[idim - 1] + 2 * m_ghostWidths[idim - 1]);
    }
    for (Int face = 0; face < 2 * dim; ++face)
    {
        m_neighbour_strides[face] = faceToSide(face) * m_strides[faceToDim(face)];
    }
}

template <>
KOKKOS_INLINE_FUNCTION IntVector<one_d> StridedFillingCurve<one_d>::indexToCoord(Int index) const
{
    return IntVector<one_d> {{index}};
}

template <>
KOKKOS_INLINE_FUNCTION IntVector<two_d> StridedFillingCurve<two_d>::indexToCoord(Int index) const
{
    IntVector<two_d> coord;
    coord[IY] = index / m_strides[IY];
    coord[IX] = index - coord[IY] * m_strides[IY];
    return coord;
}

template <>
KOKKOS_INLINE_FUNCTION IntVector<three_d> StridedFillingCurve<three_d>::indexToCoord(
        Int index) const
{
    IntVector<three_d> coord;
    coord[IZ] = index / m_strides[IZ];
    coord[IY] = (index - coord[IZ] * m_strides[IZ]) / m_strides[IY];
    coord[IX] = index - coord[IY] * m_strides[IY] - coord[IZ] * m_strides[IZ];
    return coord;
}

template <>
KOKKOS_INLINE_FUNCTION Int
StridedFillingCurve<one_d>::coordToIndex(const IntVector<one_d>& coord) const
{
    return coord[IX] * m_strides[IX];
}

template <>
KOKKOS_INLINE_FUNCTION Int
StridedFillingCurve<two_d>::coordToIndex(const IntVector<two_d>& coord) const
{
    return coord[IX] * m_strides[IX] + coord[IY] * m_strides[IY];
}

template <>
KOKKOS_INLINE_FUNCTION Int
StridedFillingCurve<three_d>::coordToIndex(const IntVector<three_d>& coord) const
{
    return coord[IX] * m_strides[IX] + coord[IY] * m_strides[IY] + coord[IZ] * m_strides[IZ];
}

template <dim_t dim>
KOKKOS_INLINE_FUNCTION Int StridedFillingCurve<dim>::faceToDim(Int face) const
{
    return face / 2;
}

template <dim_t dim>
KOKKOS_INLINE_FUNCTION Int StridedFillingCurve<dim>::faceToSide(Int face) const
{
    Int side = face - 2 * faceToDim(face);
    return (side == 0) ? -1 : 1;
}

template <dim_t dim>
KOKKOS_INLINE_FUNCTION constexpr Int StridedFillingCurve<dim>::faces(Int) const
{
    return 2 * dim;
}

template <dim_t dim>
KOKKOS_INLINE_FUNCTION Int StridedFillingCurve<dim>::getNeighbour(Int index, Int face) const
{
    return index + m_neighbour_strides[face];
}

template <dim_t dim>
KOKKOS_INLINE_FUNCTION constexpr Int StridedFillingCurve<dim>::nbNeighbours() const
{
    return 2 * dim;
}

template <dim_t dim>
KOKKOS_FORCEINLINE_FUNCTION Int StridedFillingCurve<dim>::nbCells() const
{
    return m_size;
}

template <dim_t dim>
KOKKOS_INLINE_FUNCTION bool StridedFillingCurve<dim>::belongsToInnerDomain(
        const IntVector<dim>& coords,
        Int width) const
{
    bool belongsToInnerDomain_b {true};
    for (Int idim = 0; idim < dim; ++idim)
    {
        belongsToInnerDomain_b
                *= ((coords[idim] >= m_ghostWidths[idim] - width)
                    * (coords[idim] < m_ghostWidths[idim] + m_nbCells[idim] + width));
    }
    return belongsToInnerDomain_b;
}

template <dim_t dim>
KOKKOS_INLINE_FUNCTION bool StridedFillingCurve<dim>::belongsToInnerDomain(Int index, Int width)
        const
{
    return belongsToInnerDomain(indexToCoord(index), width);
}

} // namespace ark
