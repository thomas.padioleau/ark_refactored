#pragma once

#include <iosfwd>
#include <string>

namespace ark
{

void initialize(int& argc, char**& argv);

void abort(const std::string& msg);

void print_configuration(std::ostream& os);

void finalize();

} // namespace ark
