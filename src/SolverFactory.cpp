#include <exception>
#include <memory>
#include <string>

#include "godunov_ARWB_solver/GodunovARWB.hpp"
#include "godunov_solver/Godunov.hpp"
#include "split_allregime_solver/SplitAllRegime.hpp"

#include "Problem.hpp"
#include "Solver.hpp"
#include "SolverFactory.hpp"

namespace ark
{

template <dim_t dim>
std::shared_ptr<Solver> SolverFactory<dim>::New(
        const std::string& solver_name,
        std::shared_ptr<Problem<dim>> problem)
{
    std::shared_ptr<Solver> solver = nullptr;
    if (solver_name == "split_all_regime")
    {
        solver = std::make_shared<SplitAllRegimeSolver<dim>>(problem);
    }
    else if (solver_name == "godunov")
    {
        solver = std::make_shared<GodunovSolver<dim>>(problem);
    }
    else if (solver_name == "godunov_ARWB")
    {
        solver = std::make_shared<GodunovARWBSolver<dim>>(problem);
    }
    else
    {
        throw std::runtime_error("Invalid solver.");
    }

    return solver;
}

template class SolverFactory<one_d>;
template class SolverFactory<two_d>;
template class SolverFactory<three_d>;

} // namespace ark
