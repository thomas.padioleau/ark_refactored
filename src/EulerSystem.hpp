#pragma once

#include <vector>

#include <Kokkos_Core.hpp>

#include "linalg/Vector.hpp"
#include "thermodynamics/PerfectGas.hpp"

#include "Constants.hpp"
#include "Types.hpp"
#include "Units.hpp"
#include "Utils.hpp"

namespace ark
{

template <dim_t Dimension>
struct EulerVarPrimMD;

template <>
struct EulerVarPrimMD<one_d>
{
    enum : int
    {
        ID = 0,
        IP = 1,
        IUx = 2
    };
};

template <>
struct EulerVarPrimMD<two_d>
{
    enum : int
    {
        ID = 0,
        IP = 1,
        IUx = 2,
        IUy = 3
    };
};

template <>
struct EulerVarPrimMD<three_d>
{
    enum : int
    {
        ID = 0,
        IP = 1,
        IUx = 2,
        IUy = 3,
        IUz = 4
    };
};

template <dim_t Dimension>
struct EulerVarConsMD;

template <>
struct EulerVarConsMD<one_d>
{
    enum : int
    {
        ID = 0,
        IE = 1,
        IMx = 2
    };
};

template <>
struct EulerVarConsMD<two_d>
{
    enum : int
    {
        ID = 0,
        IE = 1,
        IMx = 2,
        IMy = 3
    };
};

template <>
struct EulerVarConsMD<three_d>
{
    enum : int
    {
        ID = 0,
        IE = 1,
        IMx = 2,
        IMy = 3,
        IMz = 4
    };
};

template <dim_t Dimension_>
struct EulerSystem
{
    static constexpr dim_t Dimension {Dimension_};
    static constexpr int dim {Dimension};
    static constexpr int nbvar {2 + dim};

    using EquationOfState = thermodynamics::PerfectGas;

    using VarPrim = EulerVarPrimMD<Dimension>;
    using VarCons = EulerVarConsMD<Dimension>;

    struct ConsState
    {
        Real d;
        Real e;
        Vector<dim, Real> m;
    };

    struct PrimState
    {
        Real d;
        Real p;
        Vector<dim, Real> v;
    };

    EulerSystem() = delete;

    EulerSystem(const EulerSystem& x) = delete;

    EulerSystem(EulerSystem&& x) = delete;

    ~EulerSystem() = delete;

    EulerSystem& operator=(const EulerSystem& x) = delete;

    EulerSystem& operator=(EulerSystem&& x) = delete;

    static std::vector<std::string> cons_names()
    {
        std::vector<std::string> names(nbvar);
        names.at(VarCons::ID) = "d";
        names.at(VarCons::IE) = "E";
        names.at(VarCons::IMx + 0) = "mx";
        if (Dimension >= two_d)
        {
            names.at(VarCons::IMx + 1) = "my";
        }
        if (Dimension >= three_d)
        {
            names.at(VarCons::IMx + 2) = "mz";
        }
        return names;
    }

    static std::vector<std::string> prim_names()
    {
        std::vector<std::string> names(nbvar);
        names.at(VarPrim::ID) = "d";
        names.at(VarPrim::IP) = "p";
        names.at(VarPrim::IUx + 0) = "ux";
        if (Dimension >= two_d)
        {
            names.at(VarPrim::IUx + 1) = "uy";
        }
        if (Dimension >= three_d)
        {
            names.at(VarPrim::IUx + 2) = "uz";
        }
        return names;
    }

    KOKKOS_FORCEINLINE_FUNCTION
    static Real computeInternalEnergy(const PrimState& q, const EquationOfState& eos) noexcept
    {
        return eos.computeInternalEnergy(q.d, q.p);
    }

    KOKKOS_FORCEINLINE_FUNCTION
    static Real computeTemperature(const PrimState& q, const EquationOfState& eos) noexcept
    {
        return eos.computeTemperature(q.d, q.p);
    }

    KOKKOS_FORCEINLINE_FUNCTION
    static Real computeSpeedOfSound(const PrimState& q, const EquationOfState& eos) noexcept
    {
        return eos.computeSpeedOfSound(q.d, q.p);
    }

    KOKKOS_FORCEINLINE_FUNCTION
    static Real computeKineticEnergy(const PrimState& q) noexcept
    {
        return constants::half * q.d * dot(q.v, q.v);
    }

    KOKKOS_FORCEINLINE_FUNCTION
    static Real computeKineticEnergy(const ConsState& u) noexcept
    {
        return constants::half * dot(u.m, u.m) / u.d;
    }

    KOKKOS_FORCEINLINE_FUNCTION
    static PrimState conservativeToPrimitive(
            const ConsState& u,
            const EquationOfState& eos) noexcept
    {
        const Real ekin = computeKineticEnergy(u);
        PrimState q;
        q.d = u.d;
        q.p = eos.computePressure(u.d, u.e - ekin);
        q.v = u.m;
        q.v *= constants::one / u.d;
        return q;
    }

    KOKKOS_FORCEINLINE_FUNCTION
    static ConsState primitiveToConservative(
            const PrimState& q,
            const EquationOfState& eos) noexcept
    {
        const Real eint = eos.computeInternalEnergy(q.d, q.p);
        const Real ekin = computeKineticEnergy(q);
        ConsState u;
        u.d = q.d;
        u.e = eint + ekin;
        u.m = q.v;
        u.m *= q.d;
        return u;
    }

    template <int dir>
    KOKKOS_FORCEINLINE_FUNCTION static ConsState Flux(
            const PrimState& q,
            const EquationOfState& eos,
            const std::integral_constant<int, dir> dir_tag) noexcept
    {
        const Real eint = eos.computeInternalEnergy(q.d, q.p);
        const Real ekin = computeKineticEnergy(q);
        const Real vn = q.v(dir_tag);
        ConsState flux;
        flux.d = q.d * vn;
        flux.e = (eint + ekin + q.p) * vn;
        flux.m = q.v;
        flux.m *= q.d * vn;
        flux.m(dir_tag) += q.p;
        return flux;
    }

    template <int dir>
    KOKKOS_FORCEINLINE_FUNCTION static ConsState Flux(
            const ConsState& u,
            const EquationOfState& eos,
            const std::integral_constant<int, dir> dir_tag) noexcept
    {
        const Real ekin = computeKineticEnergy(u);
        const Real eint = u.e - ekin;
        const Real p = eos.computePressure(u.d, eint);
        const Real vn = u.m(dir_tag) / u.d;
        ConsState flux;
        flux.d = u.d * vn;
        flux.e = (u.e + p) * vn;
        flux.m = u.m;
        flux.m *= vn;
        flux.m(dir_tag) += p;
        return flux;
    }
};

} // namespace ark
