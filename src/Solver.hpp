#pragma once

#include <map>
#include <string>

#include "Constants.hpp"
#include "Types.hpp"

namespace ark
{

class Solver
{
public:
    Solver() = default;

    Solver(const Solver& x) = default;

    Solver(Solver&& x) = default;

    virtual ~Solver() = default;

    Solver& operator=(const Solver& x) = default;

    Solver& operator=(Solver&& x) = default;

    virtual Real computeTimeStep() = 0;

    virtual void nextIteration(Real dt) = 0;

    virtual void saveOutput() = 0;

    virtual std::size_t memoryUsage() const = 0;

    virtual Real time() const;

    virtual Int iteration() const;

    Real m_t = constants::zero;
    Int m_iteration = 0;
};

inline Real Solver::time() const
{
    return m_t;
}

inline Int Solver::iteration() const
{
    return m_iteration;
}

} // namespace ark
