#pragma once

#include <cmath>

#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark::riemann
{

template <dim_t dim>
class AllRegime
{
public:
    using Euler = EulerSystem<dim>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;

    AllRegime(const Params<dim>& params, const EquationOfState& eos)
        : m_eos(eos)
        , m_K(params.hydro.K)
    {
    }

    AllRegime(const AllRegime& rhs) = default;

    AllRegime(AllRegime&& rhs) noexcept = default;

    ~AllRegime() = default;

    AllRegime& operator=(const AllRegime& rhs) = default;

    AllRegime& operator=(AllRegime&& rhs) noexcept = default;

    template <int idir, int iside>
    KOKKOS_FORCEINLINE_FUNCTION ConsState operator()(
            const PrimState& q_L,
            const PrimState& q_R,
            std::integral_constant<int, idir> idir_tag,
            [[maybe_unused]] std::integral_constant<int, iside> iside_tag) const
    {
        using namespace constants;

        const Real un_L = q_L.v(idir_tag);
        const Real un_R = q_R.v(idir_tag);

        // Find the largest eigenvalues in the normal direction to the interface
        const Real c_L = Euler::computeSpeedOfSound(q_L, m_eos);
        const Real c_R = Euler::computeSpeedOfSound(q_R, m_eos);

        const Real a = m_K * std::fmax(q_R.d * c_R, q_L.d * c_L);

        const Real S_L = un_L - a / q_L.d;
        const Real S_R = un_R + a / q_R.d;

        const Real ustar = half * (un_R + un_L) - half * (q_R.p - q_L.p) / a;
        const Real Ma = std::fmax(std::fabs(un_L) / c_L, std::fabs(un_R) / c_R);
        const Real theta = std::fmin(Ma, one);
        const Real pistar = half * (q_R.p + q_L.p) - half * theta * a * (un_R - un_L);

        Real sign = 0;
        PrimState q;
        if (ustar > 0)
        {
            sign = -one;
            q = q_L;
        }
        else
        {
            sign = +one;
            q = q_R;
        }
        const Real un = q.v(idir_tag);
        const Real eint = Euler::computeInternalEnergy(q, m_eos);
        const Real ekin = Euler::computeKineticEnergy(q);
        const Real etot = (eint + ekin) / q.d;

        Real un_o = 0;
        Real ptot_o = 0;
        if (S_L * S_R > 0)
        {
            un_o = un;
            ptot_o = q.p;
        }
        else
        {
            un_o = ustar;
            ptot_o = pistar;
        }
        const Real d_o = q.d / (one - sign * q.d * (un_o - un) / a);
        const Real etot_o = etot - sign * (q.p * un - ptot_o * un_o) / a;

        ConsState flux;
        flux.d = d_o * un_o;
        flux.e = (d_o * etot_o + ptot_o) * un_o;
        flux.m = (d_o * un_o) * q.v;
        flux.m(idir_tag) = d_o * un_o * un_o + ptot_o;

        return flux;
    }

private:
    EquationOfState m_eos;
    Real m_K;
};

} // namespace ark::riemann
