#pragma once

#include <cmath>

#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark::riemann
{

template <dim_t dim>
class AllRegimeDev
{
public:
    using Euler = EulerSystem<dim>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;

    AllRegimeDev(const Params<dim>& params, const EquationOfState& eos)
        : m_eos(eos)
        , m_K(params.hydro.K)
    {
    }

    AllRegimeDev(const AllRegimeDev& x) = default;

    AllRegimeDev(AllRegimeDev&& x) noexcept = default;

    ~AllRegimeDev() = default;

    AllRegimeDev& operator=(const AllRegimeDev& x) = default;

    AllRegimeDev& operator=(AllRegimeDev&& x) noexcept = default;

    template <int idir, int iside>
    KOKKOS_FORCEINLINE_FUNCTION ConsState operator()(
            const PrimState& q_L,
            const PrimState& q_R,
            std::integral_constant<int, idir> idir_tag,
            [[maybe_unused]] std::integral_constant<int, iside> iside_tag) const
    {
        using namespace constants;

        const Real un_L = q_L.v(idir_tag);
        const Real un_R = q_R.v(idir_tag);

        // Find the largest eigenvalues in the normal direction to the interface
        const Real c_L = Euler::computeSpeedOfSound(q_L, m_eos);
        const Real c_R = Euler::computeSpeedOfSound(q_R, m_eos);

        const Real a = m_K * std::fmax(q_R.d * c_R, q_L.d * c_L);

        const Real ustar = half * (un_R + un_L) - half * (q_R.p - q_L.p) / a;
        const Real Ma = std::fmax(std::fabs(un_L) / c_L, std::fabs(un_R) / c_R);
        const Real theta = std::fmin(Ma, one);
        const Real pistar = half * (q_R.p + q_L.p) - half * theta * a * (un_R - un_L);

        PrimState q;
        if (ustar > zero)
        {
            q = q_L;
        }
        else
        {
            q = q_R;
        }
        const Real eint = Euler::computeInternalEnergy(q, m_eos);
        const Real ekin = Euler::computeKineticEnergy(q);
        const Real rhoE = eint + ekin;

        ConsState flux;
        flux.d = q.d * ustar;
        flux.e = (rhoE + pistar) * ustar;
        flux.m = (q.d * ustar) * q.v;
        flux.m(idir_tag) += pistar;

        return flux;
    }

private:
    EquationOfState m_eos;
    Real m_K;
};

} // namespace ark::riemann
