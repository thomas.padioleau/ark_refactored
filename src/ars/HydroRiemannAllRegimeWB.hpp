#pragma once

#include <cmath>

#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark::riemann
{

template <dim_t dim>
class ARWB
{
public:
    using Euler = EulerSystem<dim>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;

    ARWB(const Params<dim>& params, const EquationOfState& eos, const UniformGrid<dim>& grid)
        : m_eos(eos)
        , m_K(params.hydro.K)
        , m_gdl()
    {
        for (Int idir = 0; idir < dim; ++idir)
        {
            m_gdl[idir] = params.hydro.g[idir] * grid.m_dl[idir];
        }
    }

    ARWB(const ARWB& x) = default;

    ARWB(ARWB&& x) noexcept = default;

    ~ARWB() = default;

    ARWB& operator=(const ARWB& x) = default;

    ARWB& operator=(ARWB&& x) noexcept = default;

    template <int idir, int iside>
    KOKKOS_FORCEINLINE_FUNCTION ConsState operator()(
            const PrimState& q_L,
            const PrimState& q_R,
            std::integral_constant<int, idir> idir_tag,
            [[maybe_unused]] std::integral_constant<int, iside> iside_tag) const
    {
        using namespace constants;

        const Real un_L = q_L.v(idir_tag);
        const Real un_R = q_R.v(idir_tag);

        // Find the largest eigenvalues in the normal direction to the interface
        const Real c_L = Euler::computeSpeedOfSound(q_L, m_eos);
        const Real c_R = Euler::computeSpeedOfSound(q_R, m_eos);

        const Real a = m_K * std::fmax(q_R.d * c_R, q_L.d * c_L);

        const Real M = half * (q_L.d + q_R.d) * m_gdl[idir];

        const Real ustar = half * (un_R + un_L) - half * (q_R.p - q_L.p) / a - M / a;

        const Real Ma = std::fmax(std::fabs(un_L) / c_L, std::fabs(un_R) / c_R);
        const Real theta = std::fmin(Ma, one);
        const Real pistar = half * (q_R.p + q_L.p) - half * theta * a * (un_R - un_L);

        PrimState q;
        if (ustar > zero)
        {
            q = q_L;
        }
        else
        {
            q = q_R;
        }
        const Real eint = Euler::computeInternalEnergy(q, m_eos);
        const Real ekin = Euler::computeKineticEnergy(q);
        const Real rhoE = eint + ekin;

        const Real side = iside == 0 ? -one : +one;
        ConsState flux;
        flux.d = q.d * ustar;
        flux.e = (rhoE + pistar) * ustar - side * M * ustar * half;
        flux.m = (q.d * ustar) * q.v;
        flux.m(idir_tag) += pistar - side * M * half;

        return flux;
    }

private:
    EquationOfState m_eos;
    Real m_K;
    RealVector<dim> m_gdl;
};

} // namespace ark::riemann
