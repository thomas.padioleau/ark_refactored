#pragma once

#include <cmath>

#include "Constants.hpp"
#include "EulerSystem.hpp"
#include "Params.hpp"
#include "Types.hpp"
#include "UniformGrid.hpp"

namespace ark::riemann
{

template <dim_t dim>
class Rusanov
{
public:
    using Euler = EulerSystem<dim>;
    using EquationOfState = typename Euler::EquationOfState;
    using ConsState = typename Euler::ConsState;
    using PrimState = typename Euler::PrimState;

    explicit Rusanov(const EquationOfState& eos) : m_eos(eos) {}

    Rusanov(const Rusanov& x) = default;

    Rusanov(Rusanov&& x) noexcept = default;

    ~Rusanov() = default;

    Rusanov& operator=(const Rusanov& x) = default;

    Rusanov& operator=(Rusanov&& x) noexcept = default;

    template <int idir, int iside>
    KOKKOS_FORCEINLINE_FUNCTION ConsState operator()(
            const PrimState& q_L,
            const PrimState& q_R,
            std::integral_constant<int, idir> idir_tag,
            [[maybe_unused]] std::integral_constant<int, iside> iside_tag) const
    {
        constexpr Real half = constants::half;

        const ConsState flux_L = Euler::Flux(q_L, m_eos, idir_tag);
        const ConsState flux_R = Euler::Flux(q_R, m_eos, idir_tag);

        const Real c_L = Euler::computeSpeedOfSound(q_L, m_eos);
        const Real c_R = Euler::computeSpeedOfSound(q_R, m_eos);

        const Real un_L = q_L.v(idir_tag);
        const Real un_R = q_R.v(idir_tag);

        const Real S = std::fmax(std::fabs(un_L) + c_L, std::fabs(un_R) + c_R);

        const ConsState u_L = Euler::primitiveToConservative(q_L, m_eos);
        const ConsState u_R = Euler::primitiveToConservative(q_R, m_eos);

        // Compute the Godunov flux
        ConsState flux;
        flux.d = half * (flux_L.d + flux_R.d) - half * S * (u_R.d - u_L.d);
        flux.m = half * (flux_L.m + flux_R.m) - half * S * (u_R.m - u_L.m);
        flux.e = half * (flux_L.e + flux_R.e) - half * S * (u_R.e - u_L.e);

        return flux;
    }

private:
    EquationOfState m_eos;
};

} // namespace ark::riemann
