#!/bin/bash
#Test of the code with several test cases
#Compile the code with HDF5_C on and execture this script from this folder
#You need wget, python3, hdf5 for python, numpy and matplotlib

#import the reference solutions

read -p "Do you want to download the reference solutions (necessary in order to generate comparison plots) ?" yn
case $yn in
    [Nn]* ) echo"";;
    [Yy]* ) mkdir reference_solutions; cd reference_solutions;
            wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1xW5v4-30TzrNauEjRdqCqJWETNOc797P' -O reference_sod.h5;
            wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1rLTIHeEoQANH972d1Jn0s8ESpNVVxGvE' -O reference_RT_hllc.h5;
            wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1bonurApJ9shlBZG6dFf3Mw_EcycQnaaz' -O reference_RT_allregime.h5;
            wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1ty2NtXr_0cOy7TxgrkuHTC6Fx8Ojj4-R' -O reference_raref.h5;
            wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1rCBVApw902OkHhHJEjq7cAhcjCZP_B36' -O reference_dshock.h5;
            wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1CHXd3ebVTTpQkRob_4gcTR6GBIvBplmo' -O reference_2DRP.h5;
            cd .. ;;
    * ) echo "Please answer yes or no.";;
esac


#Create the output file
mkdir outputs
mkdir outputs/test_sod_allregime
mkdir outputs/test_sod_hllc
mkdir outputs/test_raref_allregime
mkdir outputs/test_raref_hllc
mkdir outputs/test_dshock_allregime
mkdir outputs/test_dshock_hllc
mkdir outputs/test_RT_allregime
mkdir outputs/test_RT_hllc
mkdir outputs/test_2DRP_allregime
mkdir outputs/test_2DRP_hllc
mkdir outputs/test_gresho_allregime_32
mkdir outputs/test_gresho_allregime_64
mkdir outputs/test_ATMO_allregime



#Exectute the simulations
cd ./../build/
echo " "
echo "=================================== 1D Problem runs ==================================="
echo "--Test: Sod Shock tube--"
echo "Riemann solver: All Regime"
./main ../validation_via_test_cases/settings/test_sod_allregime.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_sod_allregime/output_sod_allregime_time_000000002.h5
echo " "
echo "Riemann solver: HLLC"
./main ../validation_via_test_cases/settings/test_sod_hllc.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_sod_hllc/output_sod_hllc_time_000000002.h5
echo " "
echo "--Test: double rarefaction--"
echo "Riemann solver: All Regime"
./main ../validation_via_test_cases/settings/test_raref_allregime.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_raref_allregime/output_raref_allregime_time_000000002.h5
echo "Riemann solver: HLLC"
./main ../validation_via_test_cases/settings/test_raref_hllc.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_raref_hllc/output_raref_hllc_time_000000002.h5
echo " "
echo "--Test: double shock--"
echo "Riemann solver: All Regime"
./main ../validation_via_test_cases/settings/test_dshock_allregime.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_dshock_allregime/output_dshock_allregime_time_000000002.h5
echo "Riemann solver: HLLC"
./main ../validation_via_test_cases/settings/test_dshock_hllc.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_dshock_hllc/output_dshock_hllc_time_000000002.h5
echo " "
echo "--Test: Atmo at rest--"
echo "Riemann solver: All Regime"
./main ../validation_via_test_cases/settings/test_ATMO_allregime.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_ATMO_allregime/output_ATMO_allregime_time_000000002.h5
echo "================================== 2D Problems runs ==================================="
echo "--Test: Rayleigh Taylor--"
echo "Riemann solver: All Regime"
./main ../validation_via_test_cases/settings/test_RT_allregime.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_RT_allregime/output_RT_allregime_time_000000002.h5
echo "Riemann solver: HLLC"
./main ../validation_via_test_cases/settings/test_RT_hllc.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_RT_hllc/output_RT_hllc_time_000000002.h5
echo " "
echo "--Test: 2D Riemann problem--"
echo "Riemann solver: All Regime"
./main ../validation_via_test_cases/settings/test_2DRP_allregime.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_2DRP_allregime/output_2DRP_allregime_time_000000002.h5
echo "Riemann solver: HLLC"
./main ../validation_via_test_cases/settings/test_2DRP_hllc.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_2DRP_hllc/output_2DRP_hllc_time_000000002.h5
echo " "
echo "--Test: Gresho vortex--"
echo "Riemann solver: All Regime, n=32"
./main ../validation_via_test_cases/settings/test_gresho_allregime_32.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_gresho_allregime_32/output_gresho_allregime_32_time_000000002.h5
echo " "
echo "Riemann solver: All Regime, n=64"
./main ../validation_via_test_cases/settings/test_gresho_allregime_64.ini>>'out.txt'
python3 ../validation_via_test_cases/scripts/check_script/check_script.py  ../validation_via_test_cases/outputs/test_gresho_allregime_64/output_gresho_allregime_64_time_000000002.h5
echo " "
rm out.txt
echo "=============================== Generating output plots: =============================="
echo " "
#Treat the ouputs and produce plots
cd ../validation_via_test_cases/scripts/plot_scripts
python3 plot_sod.py
echo "Sod                plot generated"
python3 plot_raref.py
echo "Double rarefaction plot generated"
python3 plot_dshock.py
echo "Double shock       plot generated"
python3 plot_RT.py
echo "Rayleigh Taylor    plot generated"
python3 plot_2DRP.py
echo "2D Riemann problem plot generated"

cd ../../
echo " "
echo "=========================== Test for   all regime  behavior ==========================="
echo " "
python3 ../validation_via_test_cases/scripts/error_script/error_script.py  ../validation_via_test_cases/outputs/test_gresho_allregime_32/output_gresho_allregime_32_time_000000000.h5 ../validation_via_test_cases/outputs/test_gresho_allregime_32/output_gresho_allregime_32_time_000000002.h5
python3 ../validation_via_test_cases/scripts/error_script/error_script.py  ../validation_via_test_cases/outputs/test_gresho_allregime_64/output_gresho_allregime_64_time_000000000.h5 ../validation_via_test_cases/outputs/test_gresho_allregime_64/output_gresho_allregime_64_time_000000002.h5
echo " "
echo "The two errors should be of the same order of magnitude (E-3)"
echo " "
echo "=========================== Test for well balanced behavior ==========================="
echo " "
python3 ../validation_via_test_cases/scripts/WB_script/WB_script.py  ../validation_via_test_cases/outputs/test_ATMO_allregime/output_ATMO_allregime_time_000000002.h5
echo " "


#echo "Press enter when you are done"

#Delete the outputs
rm -r outputs
#rm *.pdf *.png

echo "========================================= END ========================================="
