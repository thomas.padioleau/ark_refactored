import numpy as np
import matplotlib.pyplot as plt
import h5py
import math
import sys

mystringBeg = sys.argv[1]

f_Beg = h5py.File(mystringBeg)

f_Beg_mx = f_Beg['mx'][:]

(nx, ny, nz)=f_Beg_mx.shape

speed = 0.0

for ix in range(nx):
    for iy in range(ny):
        for iz in range ( nz):
            speed = speed + abs(f_Beg_mx[ix][iy][iz])

speed = speed/(nx*ny*nz)

if (speed>10**-12):
    text = '\033[0;31m Failed \033[0;0m'
else:
    text = '\033[0;32m Passed \033[0;0m'

print("Average final speed = ",speed)
print("Acceptable values are of magnitude 10^-15 -> 10^-13. Thus, the well balanced test is ",text)
