#include <DistributedMemorySession.hpp>
#include <Types.hpp>
#include <UniformGrid.hpp>

int main(int argc, char** argv)
{
    Session::initialize(argc, argv);

    {
        std::cout << "Dimension 1:" << std::endl;
        constexpr auto dim = ark::one_d;
        ark::UniformGrid<dim> grid {{-0.5}, {0.5}, {10}, {1}, 0};
        std::cout << grid.lo(0) << std::endl;
        std::cout << grid.hi(0) << std::endl;
        std::cout << grid.dl(0) << std::endl;
        std::cout << grid.nbCells() << std::endl;
        std::cout << grid.coordToIndex({7}) << std::endl;
        std::cout << (grid.coordToIndex(grid.indexToCoord(7)) == 7 ? "true" : "false") << std::endl;
    }
    {
        std::cout << "Dimension 2:" << std::endl;
        constexpr auto dim = ark::two_d;
        ark::UniformGrid<dim> grid {{-0.5, -0.5}, {0.5, 0.5}, {10, 10}, {1, 1}, 0};
        std::cout << grid.lo(0) << " " << grid.lo(1) << std::endl;
        std::cout << grid.hi(0) << " " << grid.hi(1) << std::endl;
        std::cout << grid.dl(0) << " " << grid.dl(1) << std::endl;
        std::cout << grid.nbCells() << std::endl;
        std::cout << grid.coordToIndex({0, 7}) << std::endl;
        std::cout << (grid.coordToIndex(grid.indexToCoord(7)) == 7 ? "true" : "false") << std::endl;
    }
    {
        std::cout << "Dimension 3:" << std::endl;
        constexpr auto dim = ark::three_d;
        ark::UniformGrid<dim>
                grid {{-0.5, -0.5, -0.5}, {0.5, 0.5, 0.5}, {10, 10, 10}, {1, 1, 1}, 0};
        std::cout << grid.lo(0) << " " << grid.lo(1) << " " << grid.lo(2) << std::endl;
        std::cout << grid.hi(0) << " " << grid.hi(1) << " " << grid.hi(2) << std::endl;
        std::cout << grid.dl(0) << " " << grid.dl(1) << " " << grid.dl(2) << std::endl;
        std::cout << grid.nbCells() << std::endl;
        std::cout << grid.coordToIndex({0, 0, 7}) << std::endl;
        std::cout << (grid.coordToIndex(grid.indexToCoord(7)) == 7 ? "true" : "false") << std::endl;
    }

    Session::finalize();

    return 0;
}
