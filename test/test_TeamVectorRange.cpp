#include <cstdlib>
#include <string>
#include <vector>

#include "BaseKernel.hpp"


struct VectorLoopBoundaries
{
    int start;
    int end;
};

template <class iType, class Team>
VectorLoopBoundaries computeVectorLoopBoundaries(
        const Team& team,
        const iType start,
        const iType end) noexcept
{
    const iType chunk {(end - start) / team.team_size()};
    VectorLoopBoundaries bounds;
    bounds.start = start + team.team_rank() * chunk;
    bounds.end = team.team_rank() == team.team_size() - 1 ? end : bounds.start + chunk;
    return bounds;
}

void test(int start, int end, int nthread, const std::vector<VectorLoopBoundaries>& solution);

int main()
{
    // In this test we suppose that nthread > 0
    // (Kokkos should never return 0 thread in a team)

    try
    {
        // Only one thread in the team
        {
            std::cout << "* Test 1: Single thread, simple bounds" << std::endl;

            constexpr int start = 0;
            constexpr int end = 10;
            constexpr int nthread = 1;

            std::vector<VectorLoopBoundaries> solution {{{0, 10}}};
            test(start, end, nthread, solution);
        }

        // Only one thread in the team
        {
            std::cout << "* Test 2: Single thread" << std::endl;

            constexpr int start = 2;
            constexpr int end = 10;
            constexpr int nthread = 1;

            std::vector<VectorLoopBoundaries> solution {{{2, 10}}};
            test(start, end, nthread, solution);
        }

        // Multiple threads in the team
        {
            std::cout << "* Test 3: Multiple threads" << std::endl;

            constexpr int start = 2;
            constexpr int end = 10;
            constexpr int nthread = 3;

            std::vector<VectorLoopBoundaries> solution({{2, 4}, {4, 6}, {6, 10}});
            test(start, end, nthread, solution);
        }

        // Small loop
        {
            std::cout << "* Test 4: Small loop" << std::endl;

            constexpr int start = 2;
            constexpr int end = 4;
            constexpr int nthread = 3;

            std::vector<VectorLoopBoundaries> solution({{2, 2}, {2, 2}, {2, 4}});
            test(start, end, nthread, solution);
        }

        // No loop
        {
            std::cout << "* Test 5: No loop" << std::endl;

            constexpr int start = 2;
            constexpr int end = 2;
            constexpr int nthread = 3;

            std::vector<VectorLoopBoundaries> solution({{2, 2}, {2, 2}, {2, 2}});
            test(start, end, nthread, solution);
        }
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

// Mimic TeamPolicy<>::member_type
class Team
{
public:
    Team(int team_rank, int team_size) : m_team_rank {team_rank}, m_team_size {team_size} {}

    int team_rank() const
    {
        return m_team_rank;
    }

    int team_size() const
    {
        return m_team_size;
    }

private:
    int m_team_rank;
    int m_team_size;
};

void test(int start, int end, int nthread, const std::vector<VectorLoopBoundaries>& solution)
{
    std::vector<Team> threads;
    std::vector<VectorLoopBoundaries> bounds;
    for (int ithread = 0; ithread < nthread; ++ithread)
    {
        threads.emplace_back(ithread, nthread);
        bounds.emplace_back(computeVectorLoopBoundaries(threads[ithread], start, end));
    }

    for (int ithread = 0; ithread < nthread; ++ithread)
    {
        std::cout << "thread " << threads[ithread].team_rank() << "/"
                  << threads[ithread].team_size();
        std::cout << " computed bounds: [" << bounds[ithread].start << ", " << bounds[ithread].end
                  << "[";
        std::cout << " solution bounds: [" << solution[ithread].start << ", "
                  << solution[ithread].end << "[" << std::endl;
        if (bounds[ithread].start != solution[ithread].start)
        {
            throw std::runtime_error("Wrong start bound");
        }
        if (bounds[ithread].end != solution[ithread].end)
        {
            throw std::runtime_error("Wrong end bound");
        }
    }
}
