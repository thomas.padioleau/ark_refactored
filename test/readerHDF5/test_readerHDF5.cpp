#include <array>
#include <sstream>

#include <Ark.hpp>
#include <Params.hpp>
#include <ReaderHDF5_C.hpp>
#include <Types.hpp>
#include <UniformGrid.hpp>

int main(int argc, char** argv)
{
    ark::initialize(argc, argv);

    {
        constexpr auto dim = ark::three_d;

        ark::Params<dim> params;

        enum : int
        {
            IX = 0,
            IY = 1,
            IZ = 2
        };

        auto nbCells = params.mesh.nbCells;
        auto dom = params.mesh.dom;
#if defined(MPI_SESSION)
        nbCells[0] = nbCells[1] = nbCells[2] = 2;
        dom[0] = dom[1] = dom[2] = 2;
#else
        nbCells[0] = nbCells[1] = nbCells[2] = 4;
        dom[0] = dom[1] = dom[2] = 1;
#endif

        std::cout << "Dimension 3:" << std::endl;
        ark::UniformGrid<dim> grid {params.mesh.low, params.mesh.up, nbCells, dom, 0};
        std::cout << grid.lo(IX) << " " << grid.lo(IY) << " " << grid.lo(IZ) << std::endl;
        std::cout << grid.hi(IX) << " " << grid.hi(IY) << " " << grid.hi(IZ) << std::endl;
        std::cout << grid.dl(IX) << " " << grid.dl(IY) << " " << grid.dl(IZ) << std::endl;
        std::cout << grid.nbCells() << std::endl;

#if defined(MPI_SESSION)
        const auto coords = grid.comm.getCoords(grid.comm.rank());
#else
        std::array<int, dim> coords = {0, 0, 0};
#endif

        ark::HostArrayNd<dim> array("Test", grid.nbCells());

        std::vector<std::string> var_names {"rho", "energy", "mx", "my", "mz"};
        std::vector<std::pair<int, std::string>> variables_to_read;
        for (int ivar = 0; ivar < nbvar; ++ivar)
        {
            variables_to_read.push_back(std::make_pair(ivar, var_names[ivar]));
        }

        ark::Real t = 0.0;
        ark::Int istep = 0;
        ark::io::ReaderHDF5_C<dim> reader(grid, params, variables_to_read);
        reader.read(array, grid, istep, t);
        for (int k = 0; k < grid.m_nbCells[IZ]; ++k)
        {
            for (int j = 0; j < grid.m_nbCells[IY]; ++j)
            {
                for (int i = 0; i < grid.m_nbCells[IX]; ++i)
                {
                    auto index_loc = grid.coordToIndex({i, j, k});
                    const int index
                            = ((i + coords[IX] * grid.m_nbCells[IX])
                               + (j + coords[IY] * grid.m_nbCells[IY]) * dom[IX]
                                         * grid.m_nbCells[IX]
                               + (k + coords[IZ] * grid.m_nbCells[IZ]) * dom[IX]
                                         * grid.m_nbCells[IX] * dom[IY] * grid.m_nbCells[IY]);
                    for (int ivar = 0; ivar < dim + 2; ++ivar)
                    {
                        if (array(index_loc, ivar) != index)
                        {
                            throw;
                        }
                    }
                }
            }
        }
    }

    ark::finalize();

    return 0;
}
