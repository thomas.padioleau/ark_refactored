#include <iostream>

#include "linalg/Vector.hpp"
#include "linalg/linalg_ostream.hpp"

int main()
{
    using namespace ark;

    std::cout << "Dimension 1:\n";
    {
        Vector1d a(1.0);
        Vector1d b(2.0);

        a *= 10.0;
        a += b;

        std::cout << "a = " << a << std::endl;
        std::cout << "b = " << b << std::endl;

        std::cout << "2.0*a+b = " << 2.0 * a + b << " ";
        std::cout << (2.0 * a + b)(0) << "\n";

        std::cout << "a.a = " << dot(a, a) << std::endl;
        std::cout << sizeof(a) << " Bytes" << std::endl;
    }

    std::cout << "Dimension 2:\n";
    {
        Vector2d a(1.0, 2.0);
        Vector2d b(2.0, 3.0);

        a *= 10.0;
        a += b;

        std::cout << "a = " << a << std::endl;
        std::cout << "b = " << b << std::endl;

        std::cout << "2.0*a+b = " << 2.0 * a + b << " ";
        std::cout << (2.0 * a + b)(0) << " ";
        std::cout << (2.0 * a + b)(1) << "\n";

        std::cout << "a.a = " << dot(a, a) << std::endl;
        std::cout << sizeof(a) << " Bytes" << std::endl;
    }

    std::cout << "Dimension 3:\n";
    {
        Vector3d a(1.0, 2.0, 3.0);
        Vector3d b(2.0, 3.0, 4.0);

        a *= 10.0;
        a += b;

        std::cout << "a = " << a << std::endl;
        std::cout << "b = " << b << std::endl;

        std::cout << "2.0*a+b = " << 2.0 * a + b << " ";
        std::cout << (2.0 * a + b)(0) << " ";
        std::cout << (2.0 * a + b)(1) << " ";
        std::cout << (2.0 * a + b)(2) << "\n";

        std::cout << "a.a = " << dot(a, a) << std::endl;
        std::cout << sizeof(a) << " Bytes" << std::endl;
    }

    return 0;
}
