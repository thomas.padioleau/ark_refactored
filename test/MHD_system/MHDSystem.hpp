#pragma once

#include <vector>

#include <Kokkos_Core.hpp>

#include "linalg/Vector.hpp"
#include "thermodynamics/PerfectGas.hpp"

#include "Constants.hpp"
#include "Types.hpp"
#include "Units.hpp"
#include "Utils.hpp"

namespace ark
{

namespace experimental
{

template <dim_t Dimension>
struct MHDVarPrimMD;

template <>
struct MHDVarPrimMD<one_d>
{
    enum : int
    {
        ID = 0,
        IP = 1,
        IUx = 2,
        IBx = 3
    };
};

template <>
struct MHDVarPrimMD<two_d>
{
    enum : int
    {
        ID = 0,
        IP = 1,
        IUx = 2,
        IUy = 3,
        IBx = 4,
        IBy = 5
    };
};

template <>
struct MHDVarPrimMD<three_d>
{
    enum : int
    {
        ID = 0,
        IP = 1,
        IUx = 2,
        IUy = 3,
        IUz = 4,
        IBx = 5,
        IBy = 6,
        IBz = 7
    };
};

template <dim_t Dimension>
struct MHDVarConsMD;

template <>
struct MHDVarConsMD<one_d>
{
    enum : int
    {
        ID = 0,
        IE = 1,
        IMx = 2,
        IBx = 3
    };
};

template <>
struct MHDVarConsMD<two_d>
{
    enum : int
    {
        ID = 0,
        IE = 1,
        IMx = 2,
        IMy = 3,
        IBx = 4,
        IBy = 5
    };
};

template <>
struct MHDVarConsMD<three_d>
{
    enum : int
    {
        ID = 0,
        IE = 1,
        IMx = 2,
        IMy = 3,
        IMz = 4,
        IBx = 5,
        IBy = 6,
        IBz = 7
    };
};

template <dim_t Dimension_>
struct MHDSystem
{
    static constexpr dim_t Dimension {Dimension_};
    static constexpr int dim {Dimension};
    static constexpr int nbvar {2 + 2 * dim};

    using EquationOfState = thermodynamics::PerfectGas;

    using VarPrim = MHDVarPrimMD<Dimension>;
    using VarCons = MHDVarConsMD<Dimension>;

    struct ConsState
    {
        Real d;
        Real e; // change to E ?
        Vector<dim, Real> m;
        Vector<dim, Real> B;
    };

    struct PrimState
    {
        Real d;
        Real p;
        Vector<dim, Real> v;
        Vector<dim, Real> B;
    };

    MHDSystem() = delete;
    MHDSystem(const MHDSystem& x) = delete;
    MHDSystem(MHDSystem&& x) = delete;
    ~MHDSystem() = delete;
    MHDSystem& operator=(const MHDSystem& x) = delete;
    MHDSystem& operator=(MHDSystem&& x) = delete;

    static std::vector<std::string> cons_names()
    {
        std::vector<std::string> names(nbvar);
        names.at(VarCons::ID) = "d";
        names.at(VarCons::IE) = "E";
        names.at(VarCons::IMx + 0) = "mx";
        names.at(VarCons::IBx + 0) = "Bx";

        if (Dimension >= two_d)
        {
            names.at(VarCons::IMx + 1) = "my";
            names.at(VarCons::IBx + 1) = "By";
        }
        if (Dimension >= three_d)
        {
            names.at(VarCons::IMx + 2) = "mz";
            names.at(VarCons::IBx + 2) = "Bz";
        }
        return names;
    }

    static std::vector<std::string> prim_names()
    {
        std::vector<std::string> names(nbvar);
        names.at(VarPrim::ID) = "d";
        names.at(VarPrim::IP) = "p";
        names.at(VarPrim::IUx + 0) = "ux";
        names.at(VarPrim::IBx + 0) = "Bx";

        if (Dimension >= two_d)
        {
            names.at(VarPrim::IUx + 1) = "uy";
            names.at(VarPrim::IBx + 1) = "By";
        }
        if (Dimension >= three_d)
        {
            names.at(VarPrim::IUx + 2) = "uz";
            names.at(VarPrim::IBx + 2) = "Bz";
        }
        return names;
    }

    KOKKOS_INLINE_FUNCTION
    static Real computeInternalEnergy(const PrimState& q, const EquationOfState& eos) noexcept
    {
        return eos.computeInternalEnergy(q.d, q.p);
    }

    KOKKOS_INLINE_FUNCTION
    static Real computeTemperature(const PrimState& q, const EquationOfState& eos) noexcept
    {
        return eos.computeTemperature(q.d, q.p);
    }

    KOKKOS_INLINE_FUNCTION
    static Real computeSpeedOfSound(const PrimState& q, const EquationOfState& eos) noexcept
    {
        return eos.computeSpeedOfSound(q.d, q.p);
    }

    template <int dir>
    KOKKOS_INLINE_FUNCTION static Real computeAlfvenSpeed(const PrimState& q) noexcept
    {
        const Real ca = q.B(dir) / std::sqrt(q.d);
        return ca;
    }

    template <int dir>
    KOKKOS_INLINE_FUNCTION static Real computeSlowMagnetoAcousticSpeed(
            const PrimState& q,
            const EquationOfState& eos) noexcept
    {
        const Real B_norm22 = dot(q.B, q.B);
        const Real c = eos.computeSpeedOfSound(q.d, q.p);
        const Real ci
                = c * c + B_norm22 / q.d; // faire avec c de EOS pour pasavoir de pb avec gamma

        Real cs = ci - std::sqrt(ci * ci - 4 * c * c * q.B(dir) * q.B(dir) / q.d);

        cs = constants::half * std::sqrt(cs);

        return cs;
    }

    template <int dir>
    KOKKOS_INLINE_FUNCTION static Real computeFastMagnetoAcousticSpeed(
            const PrimState& q,
            const EquationOfState& eos) noexcept
    {
        const Real B_norm22 = dot(q.B, q.B);
        const Real c = eos.computeSpeedOfSound(q.d, q.p);
        const Real ci
                = c * c + B_norm22 / q.d; // faire avec c de EOS pour pasavoir de pb avec gamma

        Real cf = cf - std::sqrt(ci * ci - 4 * c * c * q.B(dir) * q.B(dir) / q.d);

        cf = constants::half * std::sqrt(cf);

        return cf;
    }

    KOKKOS_INLINE_FUNCTION
    static Real computeKineticEnergy(const PrimState& q) noexcept
    {
        const Real v_norm22 = dot(q.v, q.v);
        const Real ekin = constants::half * q.d * v_norm22;
        return ekin;
    }

    KOKKOS_INLINE_FUNCTION
    static Real computeKineticEnergy(const ConsState& u) noexcept
    {
        const Real m_norm22 = dot(u.m, u.m);
        const Real ekin = constants::half * m_norm22 / u.d;
        return ekin;
    }

    KOKKOS_INLINE_FUNCTION
    static Real computeMagneticEnergy(const PrimState& q) noexcept // New for MHD
    {
        const Real B_norm22 = dot(q.B, q.B);
        const Real emag = constants::half * B_norm22;
        return emag;
    }

    KOKKOS_INLINE_FUNCTION
    static Real computeMagneticEnergy(const ConsState& u) noexcept // New for MHD
    {
        const Real B_norm22 = dot(u.B, u.B);
        const Real emag = constants::half * B_norm22;
        return emag;
    }

    KOKKOS_INLINE_FUNCTION
    static PrimState conservativeToPrimitive(
            const ConsState& u,
            const EquationOfState& eos) noexcept
    {
        const Real ekin = computeKineticEnergy(u);
        const Real emag = computeMagneticEnergy(u);

        PrimState q;
        q.d = u.d;
        q.p = eos.computePressure(u.d, u.e - ekin - emag);
        q.v = u.m;
        q.v *= constants::one / u.d;
        q.B = u.B;
        return q;
    }

    KOKKOS_INLINE_FUNCTION
    static ConsState primitiveToConservative(
            const PrimState& q,
            const EquationOfState& eos) noexcept
    {
        const Real eint = eos.computeInternalEnergy(q.d, q.p);
        const Real ekin = computeKineticEnergy(q);
        const Real emag = computeMagneticEnergy(q);

        ConsState u;
        u.d = q.d;
        u.e = eint + ekin + emag;
        u.m = q.v;
        u.m *= q.d;
        u.B = q.B;
        return u;
    }

    template <int dir>
    KOKKOS_INLINE_FUNCTION static ConsState Flux(
            const PrimState& q,
            const EquationOfState& eos) noexcept
    {
        const Real eint = eos.computeInternalEnergy(q.d, q.p);
        const Real ekin = computeKineticEnergy(q);
        const Real emag = computeMagneticEnergy(q);

        const Real vn = q.v(dir);
        const Real Bn = q.B(dir);

        const Real TotalEnergy = eint + ekin + emag;

        const Real p_total = q.p + emag;


        ConsState flux;
        flux.d = q.d * vn;
        flux.e = (TotalEnergy + p_total) * vn - Bn * dot(q.B, q.v);
        flux.m = q.v;
        flux.m *= q.d * vn;
        flux.m -= q.B * Bn;
        flux.m(dir) += p_total;

        flux.B = vn * q.B - Bn * q.v;

        return flux;
    }

    template <int dir>
    KOKKOS_INLINE_FUNCTION static ConsState Flux(
            const ConsState& u,
            const EquationOfState& eos) noexcept
    {
        const Real ekin = computeKineticEnergy(u);
        const Real emag = computeMagneticEnergy(u);
        const Real eint = u.e - ekin - emag;
        const Real p = eos.computePressure(u.d, eint);
        const Real vn = u.m(dir) / u.d;
        const Real Bn = u.B(dir);
        const Real p_total = p + emag;

        ConsState flux;
        flux.d = u.d * vn;
        flux.e = (u.e + p_total) * vn - Bn * dot(u.B, u.m) / u.d;
        flux.m = u.m;
        flux.m *= vn;
        flux.m -= u.B * Bn;
        flux.m(dir) += p_total;

        flux.B = vn * u.B - Bn * u.m / u.d;
        return flux;
    }
};

} // namespace experimental

} // namespace ark
