#include <iostream>

#include "thermodynamics/ThermoParams.hpp"

#include "MHDSystem.hpp"

int main()
{
    ark::thermodynamics::ThermoParams thermo;
    thermo.gamma = 1.4;
    thermo.mmw = 1.0;
    thermo.p0 = 0.0;

    {
        std::cout << "Dimension 1:" << std::endl;
        using MHD1D = ark::experimental::MHDSystem<ark::one_d>;
        using EoS = MHD1D::EquationOfState;
        using PrimState = MHD1D::PrimState;
        using ConsState = MHD1D::ConsState;

        EoS eos(thermo);

        PrimState q1;
        q1.d = 2.0;
        q1.p = 0.0;
        q1.v(0) = 3.0;
        q1.B(0) = 1.0;

        ConsState u1 = {MHD1D::primitiveToConservative(q1, eos)};
        PrimState q2 {MHD1D::conservativeToPrimitive(u1, eos)};

        std::cout << q1.d << " " << q2.d << " " << u1.d << std::endl;
        //std::cout << q1.p << " " << q2.p << " " << " " << std::endl;
        std::cout << q1.v(0) << " " << q2.v(0) << " " << u1.m(0) / u1.d << std::endl;
        std::cout << q1.B(0) << " " << q2.B(0) << " " << u1.B(0) << std::endl;

        //  std::cout << q1.v.size() << " " << q1.v(0) << std::endl;
        //  std::cout << MHD1D::computeKineticEnergy(q1) << " ";
        //  std::cout << MHD1D::computeKineticEnergy(u1) << std::endl;
    }
    {
        std::cout << "Dimension 2:" << std::endl;
        using MHD2D = ark::experimental::MHDSystem<ark::two_d>;
        using EoS = MHD2D::EquationOfState;
        using PrimState = MHD2D::PrimState;
        using ConsState = MHD2D::ConsState;

        EoS eos(thermo);

        PrimState q1;
        q1.d = 2.0;
        q1.p = 0.0;
        q1.v(0) = 3.0;
        q1.v(1) = 4.0;
        q1.B(0) = 1.0;
        q1.B(1) = 2.0;
        ConsState u1 {MHD2D::primitiveToConservative(q1, eos)};
        PrimState q2 {MHD2D::conservativeToPrimitive(u1, eos)};

        std::cout << q1.d << " " << q2.d << " " << u1.d << std::endl;
        //  std::cout << q1.p << " " << q2.p << " " << " " << std::endl;
        std::cout << q1.v(0) << " " << q2.v(0) << " " << u1.m(0) / u1.d << std::endl;
        std::cout << q1.v(1) << " " << q2.v(1) << " " << u1.m(1) / u1.d << std::endl;
        std::cout << q1.B(0) << " " << q2.B(0) << " " << u1.B(0) << std::endl;
        std::cout << q1.B(1) << " " << q2.B(1) << " " << u1.B(1) << std::endl;

        //std::cout << q1.v.size() << " " << q1.v(0) << " " << q1.v(1) << std::endl;
        //std::cout << MHD2D::computeKineticEnergy(q1) << " ";
        //std::cout << MHD2D::computeKineticEnergy(u1) << std::endl;
    }
    {
        std::cout << "Dimension 3:" << std::endl;
        using MHD3D = ark::experimental::MHDSystem<ark::three_d>;
        using EoS = MHD3D::EquationOfState;
        using PrimState = MHD3D::PrimState;
        using ConsState = MHD3D::ConsState;

        EoS eos(thermo);

        PrimState q1;
        q1.d = 2.0;
        q1.p = 0.0;
        q1.v(0) = 3.0;
        q1.v(1) = 4.0;
        q1.v(2) = 5.0;

        q1.B(0) = 1.0;
        q1.B(1) = 2.0;
        q1.B(2) = 3.0;
        ConsState u1 {MHD3D::primitiveToConservative(q1, eos)};
        PrimState q2 {MHD3D::conservativeToPrimitive(u1, eos)};

        std::cout << q1.d << " " << q2.d << " " << u1.d << std::endl;
        //std::cout << q1.p << " " << q2.p << " " << " " << std::endl;
        std::cout << q1.v(0) << " " << q2.v(0) << " " << u1.m(0) / u1.d << std::endl;
        std::cout << q1.v(1) << " " << q2.v(1) << " " << u1.m(1) / u1.d << std::endl;
        std::cout << q1.v(2) << " " << q2.v(2) << " " << u1.m(2) / u1.d << std::endl;

        std::cout << q1.B(0) << " " << q2.B(0) << " " << u1.B(0) << std::endl;
        std::cout << q1.B(1) << " " << q2.B(1) << " " << u1.B(1) << std::endl;
        std::cout << q1.B(2) << " " << q2.B(2) << " " << u1.B(2) << std::endl;

        //std::cout << q1.v.size() << " " << q1.v(0) << " " << q1.v(1) << " " << q1.v(2) << std::endl;
        //std::cout << MHD3D::computeKineticEnergy(q1) << " ";
        //std::cout << MHD3D::computeKineticEnergy(u1) << std::endl;

        //std::cout << MHD3D::computeMagneticEnergy(q1) << " ";
        //std::cout << MHD3D::computeMagneticEnergy(u1) << std::endl;
    }

    return 0;
}
