if(${PROJECT_NAME}_ENABLE_HDF5_C)
  add_executable(test_ioHDF5_C test_ioHDF5_C.cpp)
  target_link_libraries(test_ioHDF5_C PUBLIC ${PROJECT_NAME})
  euler_add_test(test_ioHDF5_C 8)

  add_test(NAME xml_validation COMMAND xmllint --noout output_000000000.xmf)
  set_tests_properties(xml_validation PROPERTIES DEPENDS test_ioHDF5_C)

  configure_file(Xdmf.dtd Xdmf.dtd)
  add_test(NAME xml_schema_validation COMMAND xmllint --noout --dtdvalid Xdmf.dtd output_000000000.xmf)
  set_tests_properties(xml_schema_validation PROPERTIES DEPENDS xml_validation)
endif(${PROJECT_NAME}_ENABLE_HDF5_C)
