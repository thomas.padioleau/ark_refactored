#include <array>

#include <io/ReaderHDF5_C.hpp>
#include <io/WriterHDF5_C.hpp>

#include <Ark.hpp>
#include <Params.hpp>
#include <Types.hpp>
#include <UniformGrid.hpp>
#include <UnorderedMpiOstream.hpp>

int main(int argc, char** argv)
{
    ark::initialize(argc, argv);

    {
        constexpr auto dim = ark::three_d;
        constexpr auto nbvar = dim + 2;

        std::vector<std::string> var_names {"rho", "energy", "mx", "my", "mz"};
        std::vector<std::pair<int, std::string>> variables_to_save;
        for (int ivar = 0; ivar < nbvar; ++ivar)
        {
            variables_to_save.push_back(std::make_pair(ivar, var_names[ivar]));
        }

        ark::Params<dim> params;
        params.output.prefix = "output";
        params.run.restart_filename = "output_time_000000000.h5";

        enum : int
        {
            IX = 0,
            IY = 1,
            IZ = 2
        };

        auto nbCells = params.mesh.nbCells;
        auto dom = params.mesh.dom;
#if defined(MPI_SESSION)
        nbCells[0] = nbCells[1] = nbCells[2] = 2;
        dom[0] = dom[1] = dom[2] = 2;
#else
        nbCells[0] = nbCells[1] = nbCells[2] = 4;
        dom[0] = dom[1] = dom[2] = 1;
#endif

        std::ostringstream oss;

        oss << "Dimension 3:" << std::endl;
        ark::UniformGrid<dim> grid {params.mesh.low, params.mesh.up, nbCells, dom, 0};
        oss << grid.lo(IX) << " " << grid.lo(IY) << " " << grid.lo(IZ) << std::endl;
        oss << grid.hi(IX) << " " << grid.hi(IY) << " " << grid.hi(IZ) << std::endl;
        oss << grid.dl(IX) << " " << grid.dl(IY) << " " << grid.dl(IZ) << std::endl;
        oss << grid.nbCells() << std::endl;

        umpicout << oss.str();
#if defined(MPI_SESSION)
        const auto coords = grid.comm.getCoords(grid.comm.rank());
#else
        std::array<int, dim> coords = {0, 0, 0};
#endif

        {
            ark::HostRealArray<dim> array("Write", grid.nbCells());
            for (int k = 0; k < grid.m_nbCells[IZ]; ++k)
            {
                for (int j = 0; j < grid.m_nbCells[IY]; ++j)
                {
                    for (int i = 0; i < grid.m_nbCells[IX]; ++i)
                    {
                        auto index_loc = grid.coordToIndex({i, j, k});
                        const int index
                                = ((i + coords[IX] * grid.m_nbCells[IX])
                                   + (j + coords[IY] * grid.m_nbCells[IY]) * dom[IX]
                                             * grid.m_nbCells[IX]
                                   + (k + coords[IZ] * grid.m_nbCells[IZ]) * dom[IX]
                                             * grid.m_nbCells[IX] * dom[IY] * grid.m_nbCells[IY]);
                        for (int ivar = 0; ivar < nbvar; ++ivar)
                        {
                            array(index_loc, ivar) = static_cast<ark::Real>(index);
                        }
                    }
                }
            }

            ark::io::WriterHDF5_C<dim>
                    writer(grid, params, params.output.prefix, variables_to_save);
            writer.write(array, grid, 0, 0.0, 1.0, 1.0);
        }

        {
            ark::HostRealArray<dim> array("Read", grid.nbCells());
            ark::Real time = -1.0;
            ark::Int iStep = -1;
            ark::Int outputId = -1;
            ark::Int restartId = -1;
            ark::io::ReaderHDF5_C<dim> reader(grid, params, variables_to_save);
            reader.read(array, grid, iStep, time, outputId, restartId);

            if (time != 0.0 || iStep != 0 || outputId != 0 || restartId != 0)
            {
                std::cerr << "t=" << time << "iStep=" << iStep << "outputId=" << outputId
                          << "restartId=" << restartId << std::endl;
                throw std::runtime_error("Metadata do not match");
            }
            for (int k = 0; k < grid.m_nbCells[IZ]; ++k)
            {
                for (int j = 0; j < grid.m_nbCells[IY]; ++j)
                {
                    for (int i = 0; i < grid.m_nbCells[IX]; ++i)
                    {
                        auto index_loc = grid.coordToIndex({i, j, k});
                        const int index
                                = ((i + coords[IX] * grid.m_nbCells[IX])
                                   + (j + coords[IY] * grid.m_nbCells[IY]) * dom[IX]
                                             * grid.m_nbCells[IX]
                                   + (k + coords[IZ] * grid.m_nbCells[IZ]) * dom[IX]
                                             * grid.m_nbCells[IX] * dom[IY] * grid.m_nbCells[IY]);
                        for (int ivar = 0; ivar < nbvar; ++ivar)
                        {
                            if (array(index_loc, ivar) != static_cast<ark::Real>(index))
                            {
                                std::cerr << "1: " << array(index_loc, ivar)
                                          << "2: " << static_cast<ark::Real>(index) << std::endl;
                                throw std::runtime_error("Raw data do not match");
                            }
                        }
                    }
                }
            }
        }
    }

    ark::finalize();

    return 0;
}
