#include <iostream>

#include "linalg/Tensor.hpp"
#include "linalg/Vector.hpp"
#include "linalg/linalg_ostream.hpp"

int main()
{
    using namespace ark;

    std::cout << "Dimension 1:\n";
    {
        Tensor_1d a(1.0);
        Tensor_1d b(2.0);

        std::cout << "a = " << a << std::endl;
        std::cout << "b = " << b << std::endl;

        Tensor_1d c = a;
        c *= 2.0;
        c += b;

        std::cout << "c = 2.0*a+b = " << c << std::endl;

        std::cout << "2.0*a+b = " << 2.0 * a + b << "\n";

        std::cout << sizeof(a) << " Bytes" << std::endl;
    }

    std::cout << "Dimension 2:\n";
    {
        Tensor_2d a(1.0, 2.0, 3.0, 4.0);
        Tensor_2d b(2.0, 3.0, 4.0, 5.0);

        std::cout << "a = " << a << std::endl;
        std::cout << "b = " << b << std::endl;

        Tensor_2d c = a;
        c *= 2.0;
        c += b;

        std::cout << "c = 2.0*a+b = " << c << std::endl;

        std::cout << "2.0*a+b = " << 2.0 * a + b << "\n";

        std::cout << sizeof(a) << " Bytes" << std::endl;
    }

    std::cout << "Dimension 3:\n";
    {
        Tensor_3d a(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0);
        Tensor_3d b(4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0);

        std::cout << "a = " << a << std::endl;
        std::cout << "b = " << b << std::endl;

        Tensor_3d c = a;
        c *= 2.0;
        c += b;

        std::cout << "c = 2.0*a+b = " << c << std::endl;

        std::cout << "2.0*a+b = " << 2.0 * a + b << "\n";

        std::cout << sizeof(a) << " Bytes" << std::endl;
    }

    return 0;
}
