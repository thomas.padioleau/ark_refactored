#include <iostream>

#include "thermodynamics/ThermoParams.hpp"

#include "EulerSystem.hpp"

int main()
{
    ark::thermodynamics::ThermoParams thermo;
    thermo.gamma = 1.4;
    thermo.mmw = 1.0;
    thermo.p0 = 0.0;

    {
        std::cout << "Dimension 1:" << std::endl;
        using Euler1d = ark::EulerSystem<ark::one_d>;
        using EoS = Euler1d::EquationOfState;
        using PrimState = Euler1d::PrimState;
        using ConsState = Euler1d::ConsState;

        EoS eos(thermo);

        PrimState q1;
        q1.d = 2.0;
        q1.p = 0.0;
        q1.v(0) = 3.0;
        ConsState u1 = {Euler1d::primitiveToConservative(q1, eos)};
        PrimState q2 {Euler1d::conservativeToPrimitive(u1, eos)};

        std::cout << q1.d << " " << q2.d << " " << u1.d << std::endl;
        std::cout << q1.p << " " << q2.p << " " << u1.e << std::endl;
        std::cout << q1.v(0) << " " << q2.v(0) << " " << u1.m(0) << std::endl;

        std::cout << q1.v.size() << " " << q1.v(0) << std::endl;
        std::cout << Euler1d::computeKineticEnergy(q1) << " ";
        std::cout << Euler1d::computeKineticEnergy(u1) << std::endl;
    }
    {
        std::cout << "Dimension 2:" << std::endl;
        using Euler2d = ark::EulerSystem<ark::two_d>;
        using EoS = Euler2d::EquationOfState;
        using PrimState = Euler2d::PrimState;
        using ConsState = Euler2d::ConsState;

        EoS eos(thermo);

        PrimState q1;
        q1.d = 2.0;
        q1.p = 0.0;
        q1.v(0) = 3.0;
        q1.v(1) = 4.0;
        ConsState u1 {Euler2d::primitiveToConservative(q1, eos)};
        PrimState q2 {Euler2d::conservativeToPrimitive(u1, eos)};

        std::cout << q1.d << " " << q2.d << " " << u1.d << std::endl;
        std::cout << q1.p << " " << q2.p << " " << u1.e << std::endl;
        std::cout << q1.v(0) << " " << q2.v(0) << " " << u1.m(0) << std::endl;
        std::cout << q1.v(1) << " " << q2.v(1) << " " << u1.m(1) << std::endl;

        std::cout << q1.v.size() << " " << q1.v(0) << " " << q1.v(1) << std::endl;
        std::cout << Euler2d::computeKineticEnergy(q1) << " ";
        std::cout << Euler2d::computeKineticEnergy(u1) << std::endl;
    }
    {
        std::cout << "Dimension 3:" << std::endl;
        using Euler3d = ark::EulerSystem<ark::three_d>;
        using EoS = Euler3d::EquationOfState;
        using PrimState = Euler3d::PrimState;
        using ConsState = Euler3d::ConsState;

        EoS eos(thermo);

        PrimState q1;
        q1.d = 2.0;
        q1.p = 0.0;
        q1.v(0) = 3.0;
        q1.v(1) = 4.0;
        q1.v(2) = 5.0;
        ConsState u1 {Euler3d::primitiveToConservative(q1, eos)};
        PrimState q2 {Euler3d::conservativeToPrimitive(u1, eos)};

        std::cout << q1.d << " " << q2.d << " " << u1.d << std::endl;
        std::cout << q1.p << " " << q2.p << " " << u1.e << std::endl;
        std::cout << q1.v(0) << " " << q2.v(0) << " " << u1.m(0) << std::endl;
        std::cout << q1.v(1) << " " << q2.v(1) << " " << u1.m(1) << std::endl;
        std::cout << q1.v(2) << " " << q2.v(2) << " " << u1.m(2) << std::endl;

        std::cout << q1.v.size() << " " << q1.v(0) << " " << q1.v(1) << " " << q1.v(2) << std::endl;
        std::cout << Euler3d::computeKineticEnergy(q1) << " ";
        std::cout << Euler3d::computeKineticEnergy(u1) << std::endl;
    }

    return 0;
}
