#include <cmath>
#include <iostream>

#include <Kokkos_Core.hpp>
#include <simd.hpp>

using Real = double;

#ifdef KOKKOS_ENABLE_CUDA
using RealSimd = simd::simd<Real, simd::simd_abi::cuda_warp<32>>;
#else
using RealSimd = simd::simd<Real, simd::simd_abi::native>;
#endif

using Layout = Kokkos::LayoutLeft;
using VectorView = Kokkos::View<RealSimd::storage_type***, Layout>;
using ScalarView = Kokkos::View<Real***, Layout>;

class PerfectGas
{
public:
    PerfectGas(Real gamma, Real mmw) noexcept;

    ~PerfectGas() = default;

    PerfectGas(const PerfectGas& x) = default;

    PerfectGas(PerfectGas&& x) = default;

    PerfectGas& operator=(const PerfectGas& x) = default;

    PerfectGas& operator=(PerfectGas&& x) = default;

    KOKKOS_INLINE_FUNCTION
    Real computeAdiabaticIndex() const noexcept;

    template <class ScalarOrSimd>
    KOKKOS_INLINE_FUNCTION ScalarOrSimd
    computeInternalEnergy(const ScalarOrSimd& d, const ScalarOrSimd& p) const noexcept;

    KOKKOS_INLINE_FUNCTION
    Real computeMeanMolecularWeight() const noexcept;

    template <class ScalarOrSimd>
    KOKKOS_INLINE_FUNCTION ScalarOrSimd
    computePressure(const ScalarOrSimd& d, const ScalarOrSimd& e) const noexcept;

    template <class ScalarOrSimd>
    KOKKOS_INLINE_FUNCTION ScalarOrSimd
    computeSpeedOfSound(const ScalarOrSimd& d, const ScalarOrSimd& p) const noexcept;

private:
    Real m_gamma;
    Real m_gamma_m1;
    Real m_inv_gamma_m1;
    Real m_mmw;
    Real m_Rstar;
};

PerfectGas::PerfectGas(Real gamma, Real mmw) noexcept
    : m_gamma {gamma}
    , m_gamma_m1 {gamma - 1.0}
    , m_inv_gamma_m1 {1.0 / (gamma - 1.0)}
    , m_mmw {mmw}
    , m_Rstar {1.0 / mmw}
{
}

KOKKOS_INLINE_FUNCTION
Real PerfectGas::computeAdiabaticIndex() const noexcept
{
    return m_gamma;
}

template <class ScalarOrSimd>
KOKKOS_INLINE_FUNCTION ScalarOrSimd
PerfectGas::computeInternalEnergy(const ScalarOrSimd& d, const ScalarOrSimd& p) const noexcept
{
    return m_inv_gamma_m1 * p;
}

KOKKOS_INLINE_FUNCTION
Real PerfectGas::computeMeanMolecularWeight() const noexcept
{
    return m_mmw;
}

template <class ScalarOrSimd>
KOKKOS_INLINE_FUNCTION ScalarOrSimd
PerfectGas::computePressure(const ScalarOrSimd& d, const ScalarOrSimd& e) const noexcept
{
    return m_gamma_m1 * e;
}

template <class ScalarOrSimd>
KOKKOS_INLINE_FUNCTION ScalarOrSimd
PerfectGas::computeSpeedOfSound(const ScalarOrSimd& d, const ScalarOrSimd& p) const noexcept
{
    return sqrt(m_gamma * p / d);
}

enum : int
{
    ID = 0,
    IP = 1,
    IC = 2
};

struct ComputeSpeedOfSoundVector
{
    ComputeSpeedOfSoundVector(const PerfectGas& eos, const VectorView& view_vector)
        : m_eos(eos)
        , m_view_vector(view_vector)
    {
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const Kokkos::TeamPolicy<>::member_type& team) const
    {
        const int jk = team.league_rank();
        Kokkos::parallel_for(
                Kokkos::TeamThreadRange(team, m_view_vector.extent(0)),
                [&](const int i) {
                    RealSimd d = m_view_vector(i, jk, ID);
                    RealSimd p = m_view_vector(i, jk, IP);
                    m_view_vector(i, jk, IC) = m_eos.computeSpeedOfSound(d, p);
                });
    }

    PerfectGas m_eos;
    VectorView m_view_vector;
};

struct ComputeSpeedOfSoundScalar
{
    ComputeSpeedOfSoundScalar(const PerfectGas& eos, const ScalarView& view_scalar)
        : m_eos(eos)
        , m_view_scalar(view_scalar)
    {
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const Kokkos::TeamPolicy<>::member_type& team) const
    {
        const int jk = team.league_rank();
        Kokkos::parallel_for(
                Kokkos::TeamVectorRange(team, m_view_scalar.extent(0)),
                [&](const int i) {
                    Real d = m_view_scalar(i, jk, ID);
                    Real p = m_view_scalar(i, jk, IP);
                    m_view_scalar(i, jk, IC) = m_eos.computeSpeedOfSound(d, p);
                });
    }

    PerfectGas m_eos;
    ScalarView m_view_scalar;
};

int main(int argc, char** argv)
{
    Kokkos::ScopeGuard scope(argc, argv);

    Kokkos::print_configuration(std::clog, true);

    std::clog << "Vector size " << RealSimd::size() << std::endl;

    const int nx = (65 / RealSimd::size()) * RealSimd::size();
    const int nx_vector = nx / RealSimd::size();
    const int nynz = 2;
    constexpr int nbvar = 3;

    VectorView view_vector("Vector", nx_vector, nynz, nbvar);

    ScalarView view_scalar(
            (RealSimd::value_type*)view_vector.data(),
            view_vector.extent(0) * RealSimd::storage_type::size(),
            nynz,
            nbvar);

    auto view_host = Kokkos::create_mirror_view(view_scalar);

    for (auto jk = 0; jk < view_host.extent(1); ++jk)
    {
        for (auto i = 0; i < view_host.extent(0); ++i)
        {
            view_host(i, jk, ID) = 2.0 * static_cast<Real>(i + 1);
            view_host(i, jk, IP) = 100.0 * static_cast<Real>(i + 1);
            view_host(i, jk, IC) = 0.0;
        }
    }

    Kokkos::deep_copy(view_scalar, view_host);

    PerfectGas pg(2.0, 1.0);
    {
        Kokkos::TeamPolicy<> team_policy(view_vector.extent(1), 1, RealSimd::size());
        Kokkos::parallel_for(
                "Compute Speed of Sound",
                team_policy,
                ComputeSpeedOfSoundVector(pg, view_vector));
    }

    {
        Kokkos::TeamPolicy<> team_policy(view_scalar.extent(1), 1, RealSimd::size());
        Kokkos::parallel_for(
                "Compute Speed of Sound",
                team_policy,
                ComputeSpeedOfSoundScalar(pg, view_scalar));
    }

    Kokkos::deep_copy(view_host, view_scalar);

    for (auto jk = 0; jk < view_host.extent(1); ++jk)
    {
        for (auto i = 0; i < view_host.extent(0); ++i)
        {
            std::clog << view_host(i, jk, IC) << ' ';
        }
    }
    std::clog << std::endl;

    return 0;
}
