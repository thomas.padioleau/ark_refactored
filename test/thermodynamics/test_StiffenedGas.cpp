#include <iostream>

#include <thermodynamics/StiffenedGas.hpp>

int main()
{
    double gamma = {4.4};
    double mu = {17.87};
    double p0 = {6.0e8};
    double e0 = {0.0};
    ark::thermodynamics::StiffenedGas water = {gamma, mu, p0, e0};
    double d = {1000.0};
    double p = {101325.0};
    std::cout << "Gamma = " << water.computeAdiabaticIndex() << std::endl;
    std::cout << "Mu    = " << water.computeMeanMolecularWeight() << std::endl;
    std::cout << "Eint  = " << water.computeInternalEnergy(d, p) << std::endl;
    std::cout << "Cs    = " << water.computeSpeedOfSound(d, p) << " m/s" << std::endl;
    std::cout << "T     = " << water.computeTemperature(d, p) << " K" << std::endl;
    return 0;
}
