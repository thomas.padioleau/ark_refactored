#include <iostream>
#include <string>

#include <Kokkos_Core.hpp>

struct LinearIndex_t
{
};

struct MultiIndex_t
{
};

constexpr int nx = 4;
constexpr int ny = 3;
constexpr int nz = 2;

struct TeamLinearIndex_t
{
};
struct TeamMultiIndex_t
{
};

template <class T>
class BaseKernel
{
public:
    using TeamPolicy = typename Kokkos::TeamPolicy<>;
    using Team = typename TeamPolicy::member_type;

    KOKKOS_INLINE_FUNCTION
    void operator()(TeamLinearIndex_t, const Team& team) const
    {
        int j0z = team.league_rank() / ny;
        int j0y = team.league_rank() - j0z * ny;
        int j0x = 0;
        int j0 = j0x * 1 + j0y * nx + j0z * nx * ny;
        Kokkos::parallel_for(Kokkos::TeamVectorRange(team, j0, j0 + nx), [&](const int& j) {
            std::printf("jz=%d jy=%d jx=%d", j0z, j0y, j - j0);
            static_cast<const T&> (*this)(LinearIndex_t(), j);
        });
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(TeamMultiIndex_t, const Team& team) const
    {
        int j0z = team.league_rank();
        // int j0y = 0;
        // int j0x = 0;
        Kokkos::parallel_for(
                Kokkos::TeamThreadRange(team, 0, ny),
                KOKKOS_LAMBDA(const int& jy) {
                    Kokkos::parallel_for(
                            Kokkos::ThreadVectorRange(team, 0, nx),
                            [&](const int& jx) { static_cast<const T&> (*this)(jx, jy, j0z); });
                });
    }
};


template <class Kernel>
void ark_parallel_for_lin(const std::string& name, const Kernel& kernel)
{
    using TeamPolicy = Kokkos::TeamPolicy<Kokkos::IndexType<int>, TeamLinearIndex_t>;

    TeamPolicy policy(ny * nz, Kokkos::AUTO);
    Kokkos::parallel_for(name, policy, kernel);
}

template <class Kernel>
void ark_parallel_for_unlin(const std::string& name, const Kernel& kernel)
{
    using TeamPolicy = Kokkos::TeamPolicy<Kokkos::IndexType<int>, TeamMultiIndex_t>;

    TeamPolicy policy(nz, Kokkos::AUTO);
    Kokkos::parallel_for(name, policy, kernel);
}

template <class Kernel>
void ark_parallel_for_mdrange(const std::string& name, const Kernel& kernel)
{
    Kokkos::MDRangePolicy<Kokkos::Rank<3, Kokkos::Iterate::Left>>
            policy({0, 0, 0}, {nx, ny, nz}, {1, 1, 1});
    Kokkos::parallel_for(name, policy, kernel);
}

class Print : public BaseKernel<Print>
{
public:
    using Super = BaseKernel<Print>;
    using Super::operator(); // thanks to David Hollman

    KOKKOS_INLINE_FUNCTION
    void operator()(LinearIndex_t, const int& j) const
    {
        std::printf(" j=%d\n", j);
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const int& jx, const int& jy, const int& jz) const
    {
        std::printf("jz=%d jy=%d jx=%d j=%d\n", jz, jy, jx, jx * 1 + jy * nx + jz * nx * ny);
    }
};


int main(int argc, char** argv)
{
    Kokkos::ScopeGuard scope;
    ark_parallel_for_lin("Test", Print());
    ark_parallel_for_unlin("Test", Print());
    ark_parallel_for_mdrange("Test", Print());
    return 0;
}
