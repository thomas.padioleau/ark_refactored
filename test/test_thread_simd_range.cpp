#include <cmath>
#include <iostream>

#include <Kokkos_Core.hpp>
#include <simd.hpp>

using Real = double;

#ifdef KOKKOS_ENABLE_CUDA
using RealSimd = simd::simd<Real, simd::simd_abi::cuda_warp<32>>;
#else
using RealSimd = simd::simd<Real, simd::simd_abi::native>;
#endif

using Layout = Kokkos::LayoutLeft;
using ScalarView = Kokkos::View<Real***, Layout>;

class SimdRange
{
public:
    using simd_type = RealSimd;

    KOKKOS_INLINE_FUNCTION
    SimdRange(int begin) noexcept : m_begin(begin) {}

    KOKKOS_INLINE_FUNCTION
    static constexpr auto size() noexcept
    {
        return simd_type::size();
    }

    KOKKOS_INLINE_FUNCTION
    int begin() const noexcept
    {
        return m_begin;
    }

    KOKKOS_INLINE_FUNCTION
    int end() const noexcept
    {
        return m_begin + this->size();
    }

private:
    int m_begin;
};

class ScalarRange
{
public:
    KOKKOS_INLINE_FUNCTION
    ScalarRange(int begin) noexcept : m_begin(begin) {}

    KOKKOS_INLINE_FUNCTION
    static constexpr auto size() noexcept
    {
        return 1;
    }

    KOKKOS_INLINE_FUNCTION
    int begin() const noexcept
    {
        return m_begin;
    }

    KOKKOS_INLINE_FUNCTION
    int end() const noexcept
    {
        return m_begin + this->size();
    }

private:
    int m_begin;
};

KOKKOS_INLINE_FUNCTION
SimdRange shift(const SimdRange& x, int s) noexcept
{
    return SimdRange(x.begin() + s);
}

KOKKOS_INLINE_FUNCTION
ScalarRange shift(const ScalarRange& x, int s) noexcept
{
    return ScalarRange(x.begin() + s);
}

KOKKOS_INLINE_FUNCTION
SimdRange::simd_type load(const Real* ptr, const SimdRange& range)
{
    return SimdRange::simd_type(ptr + range.begin(), simd::element_aligned_tag());
}

KOKKOS_INLINE_FUNCTION
Real load(const Real* ptr, const ScalarRange& range) noexcept
{
    return *(ptr + range.begin());
}

KOKKOS_INLINE_FUNCTION
void store(Real* ptr, const SimdRange& range, const SimdRange::simd_type& simd)
{
    simd.copy_to(ptr + range.begin(), simd::element_aligned_tag());
}

KOKKOS_INLINE_FUNCTION
void store(Real* ptr, const ScalarRange& range, const Real& scalar) noexcept
{
    *(ptr + range.begin()) = scalar;
}

class ThreadSimdRange
{
public:
    using scalar_range = ScalarRange;
    using simd_range = SimdRange;

    KOKKOS_INLINE_FUNCTION
    ThreadSimdRange(int begin, int end) noexcept : m_begin(begin), m_end(end) {}

    KOKKOS_INLINE_FUNCTION
    ThreadSimdRange(int count) noexcept : ThreadSimdRange(0, count) {}

    KOKKOS_INLINE_FUNCTION
    int begin() const noexcept
    {
        return m_begin;
    }

    KOKKOS_INLINE_FUNCTION
    int end() const noexcept
    {
        return m_end;
    }

private:
    int m_begin;
    int m_end;
};

template <class Functor>
KOKKOS_INLINE_FUNCTION void parallel_for(const ThreadSimdRange& range, Functor&& f)
{
    using simd_range = ThreadSimdRange::simd_range;

    const auto count = range.end() - range.begin();
    const auto end_vector = range.end() - count % simd_range::size();

    // Simd loop
    for (auto i = range.begin(); i < end_vector; i += simd_range::size())
    {
        f(SimdRange(i));
    }

    // Remainder loop
#if defined(__CUDACC__)
    for (auto i = end_vector + threadIdx.x; i < range.end(); i += blockDim.x)
    {
        f(ScalarRange(i));
    }
#else
    for (auto i = end_vector; i < range.end(); ++i)
    {
        f(ScalarRange(i));
    }
#endif
}

struct HeatKernel
{
    HeatKernel(const ScalarView& scalar_view, const ScalarView& scalar_view2)
        : m_scalar_view(scalar_view)
        , m_scalar_view2(scalar_view2)
    {
    }

    KOKKOS_INLINE_FUNCTION
    void operator()(const Kokkos::TeamPolicy<>::member_type& team) const
    {
        const int k = team.league_rank() + 1;
        Kokkos::parallel_for(
                Kokkos::TeamThreadRange(team, 1, m_scalar_view.extent(1) - 1),
                [&](const int j) {
                    parallel_for(
                            ThreadSimdRange(1, m_scalar_view.extent(0) - 1),
                            [&](const auto& irange) {
                                const auto T_ijk = load(&m_scalar_view(0, j, k), irange);
                                const auto T_imjk
                                        = load(&m_scalar_view(0, j, k), shift(irange, -1));
                                const auto T_ipjk
                                        = load(&m_scalar_view(0, j, k), shift(irange, +1));
                                const auto T_ijmk = load(&m_scalar_view(0, j - 1, k), irange);
                                const auto T_ijpk = load(&m_scalar_view(0, j + 1, k), irange);
                                const auto T_ijkm = load(&m_scalar_view(0, j, k - 1), irange);
                                const auto T_ijkp = load(&m_scalar_view(0, j, k + 1), irange);
                                const auto T2_ijk = 2.0
                                                    * (T_ipjk + T_ijpk + T_ijkp + T_ijk - T_imjk
                                                       - T_ijmk - T_ijkm);
                                store(&m_scalar_view2(0, j, k), irange, T2_ijk);
                            });
                });
    }

    ScalarView m_scalar_view;
    ScalarView m_scalar_view2;
};

int main(int argc, char** argv)
{
    Kokkos::ScopeGuard scope(argc, argv);

    Kokkos::print_configuration(std::cout, true);

    const int nx = argc > 1 ? std::stoi(argv[1]) : 100;
    const int ny = argc > 2 ? std::stoi(argv[2]) : nx;
    const int nz = argc > 3 ? std::stoi(argv[3]) : nx;

    const int ng = 1;
    const int nxg = nx + 2 * ng;
    const int nyg = ny + 2 * ng;
    const int nzg = nz + 2 * ng;

    ScalarView view_scalar("Scalar", nxg, nyg, nzg);
    ScalarView view_scalar2("Scalar 2", nxg, nyg, nzg);

    Kokkos::View<double**, Kokkos::LayoutLeft> a(view_scalar.data(), nx, ny);
    Kokkos::view_alloc();
    auto view_host = Kokkos::create_mirror_view(view_scalar);

    for (auto k = 0; k < view_host.extent(2); ++k)
    {
        for (auto j = 0; j < view_host.extent(1); ++j)
        {
            for (auto i = 0; i < view_host.extent(0); ++i)
            {
                view_host(i, j, k) = 2.0;
            }
        }
    }

    Kokkos::deep_copy(view_scalar, view_host);

    auto team_size = argc > 4 ? std::stoi(argv[4])
                              : Kokkos::TeamPolicy<>(
                                        view_scalar.extent(2) - 2,
                                        Kokkos::AUTO,
                                        RealSimd::size())
                                        .team_size_recommended(
                                                HeatKernel(view_scalar, view_scalar2),
                                                Kokkos::ParallelForTag());

    std::cout << "Vector size " << RealSimd::size() << std::endl;
    std::cout << "Allocation 2x" << 1e-9 * view_scalar.size() * 8 << "GB\n";
    std::cout << "Team size " << team_size << std::endl;

    Kokkos::Timer t;
    int nrepeat = 200;
    for (int repeat = 0; repeat < nrepeat; ++repeat)
    {
        Kokkos::TeamPolicy<> team_policy(view_scalar.extent(2) - 2, team_size, RealSimd::size());
        Kokkos::parallel_for("Heat kernel", team_policy, HeatKernel(view_scalar, view_scalar2));
    }
    Kokkos::fence();
    std::cout << ((double)nrepeat * nx * ny * nz) / t.seconds() * 1e-6 << std::endl;

    Kokkos::deep_copy(view_host, view_scalar2);

    if (nxg * nyg * nzg < 1000)
    {
        for (auto k = 0; k < view_host.extent(2); ++k)
        {
            for (auto j = 0; j < view_host.extent(1); ++j)
            {
                for (auto i = 0; i < view_host.extent(0); ++i)
                {
                    std::cout << view_host(i, j, k) << ' ';
                }
            }
        }
        std::cout << std::endl;
    }

    return 0;
}
